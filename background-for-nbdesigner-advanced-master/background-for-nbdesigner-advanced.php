<?php
/**
 * Plugin Name:       Background for NBDesigner Advanced
 * Plugin URI:        https://cmsmart.net
 * Description:       Background color and image for NBDesigner Advanced
 * Version:           1.1.0
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            thanhbinh87 & Hoang & The
 * Author URI:        https://cmsmart.net
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       background-for-nbdesigner-advanced
 * Domain Path:       /languages
 */

add_action('nbd_js_config','bagr_background_js_config');
function bagr_background_js_config(){
    defined('NBDESIGNER_BACKGROUND_URL') or bagr_define();
    echo "var bagr_background = true;";
    echo 'NBDESIGNCONFIG.background_url = "'.NBDESIGNER_BACKGROUND_URL.'";';
    echo 'NBDESIGNCONFIG.nbdlangs.backgrounds = "'.__('Backgrounds', 'web-to-print-online-designer').'";';
}

add_action('nbod_define','bagr_define');
function bagr_define(){
    nbd_define('NBDESIGNER_BACKGROUND_DIR', NBDESIGNER_DATA_DIR.'/backgrounds');
    nbd_define('NBDESIGNER_BACKGROUND_URL', NBDESIGNER_DATA_URL.'/backgrounds');
}

add_filter('nbod_extra_active_tabs','bagr_add_background_active_tab',10,9);
function bagr_add_background_active_tab($nbav_active_tabs, $active_product, $active_design ,$active_typos ,$active_cliparts ,$active_photos ,$active_elements ,$active_template ,$active_layers){
    $nbav_active_tabs['active_background'] = (!$active_product && !$active_design && !$active_typos && !$active_cliparts && !$active_photos && !$active_elements && !$active_template && !$active_layers) ? true : false;
    return $nbav_active_tabs;
}

add_filter('nbdesigner_general_settings','addon_show_custom_background');
function addon_show_custom_background($option){
            $bleed_arr = array(
                        'title'         => esc_html__( 'Show upload image and background', 'web-to-print-online-designer'),
                        'description'   => esc_html__( 'Show upload image and background.', 'web-to-print-online-designer'),
                        'id'            => 'nbdesigner_attachment_show_upload_color',
                        'default'       => 'no',
                        'type'          => 'radio',
                        'options'       => array(
                            'yes'   => esc_html__('Yes', 'web-to-print-online-designer'),
                            'no'    => esc_html__('No', 'web-to-print-online-designer')
                        )  
                    );
    array_push($option['customization'],$bleed_arr);
    return $option; 
}

add_action('nbod_after_tab_typography','bagr_add_background_tab');
function bagr_add_background_tab($nbav_active_tabs){
    ?>

    <div class="<?php if( $nbav_active_tabs['active_background'] ) echo 'active'; ?> tab nbd-onload" id="tab-background" data-container="#tab-background" nbd-scroll="scrollLoadMore(container, type)" data-type="background" data-offset="20">
        <div class="tab-main tab-scroll">
            <?php if(nbdesigner_get_option( 'nbdesigner_show_type_background' , 'no') == 'yes') : ?>
            <div class="inner-tab-layer">
                <spectrum-colorpicker
                    ng-model="colorBackground" 
                    ng-change="changeBackgroundColor(colorBackground)" 
                    nb-dragstart="changeBackgroundColor(colorBackground)"
                    options="{
                        allowEmpty: true,
                        showPaletteOnly: false, 
                        flat: true,
                        clickoutFiresChange: true,
                        togglePaletteOnly: false, 
                        showInitial: false,
                        showPalette: true, 
                        showSelectionPalette: false,
                        showButtons: false,
                        showAlpha: true,
                        preferredFormat: 'hex3',
                        palette: [
                            ['#000','#444','#666','#999','#ccc','#eee','#f3f3f3','#fff'],
                            ['#f00','#f90','#ff0','#0f0','#0ff','#00f','#90f','#f0f'],
                            ['#f4cccc','#fce5cd','#fff2cc','#d9ead3','#d0e0e3','#cfe2f3','#d9d2e9','#ead1dc'],
                            ['#ea9999','#f9cb9c','#ffe599','#b6d7a8','#a2c4c9','#9fc5e8','#b4a7d6','#d5a6bd'],
                            ['#e06666','#f6b26b','#ffd966','#93c47d','#76a5af','#6fa8dc','#8e7cc3','#c27ba0'],
                            ['#c00','#e69138','#f1c232','#6aa84f','#45818e','#3d85c6','#674ea7','#a64d79'],
                            ['#900','#b45f06','#bf9000','#38761d','#134f5c','#0b5394','#351c75','#741b47'],
                            ['#600','#783f04','#7f6000','#274e13','#0c343d','#073763','#20124d','#4c1130']
                            ],
                        showInput: true}">
                </spectrum-colorpicker>
            </div>
            <?php endif; ?>
            <?php if(nbdesigner_get_option( 'nbdesigner_attachment_show_upload_color' , 'no') == 'yes') : ?>
            <div class="nbd-search">
                    <input type="text" name="search" placeholder="<?php _e('Search Background', 'web-to-print-online-designer'); ?>" ng-model="resource.background.filter.search"/>
                    <i class="icon-nbd icon-nbd-fomat-search"></i>
            </div>
            <div class="backgrounds-category" ng-class="resource.background.data.cat.length > 0 ? '' : 'nbd-hiden'">           
                    <div class="nbd-button nbd-dropdown">
                        <span>{{resource.background.filter.currentCat.name}}</span>
                        <i class="icon-nbd icon-nbd-chevron-right rotate90"></i>
                        <div class="nbd-sub-dropdown" data-pos="center">
                            <ul class="nbd-perfect-scroll">
                                <li ng-click="changeCat('background', cat)" ng-repeat="cat in resource.background.data.cat"><span>{{cat.name}}</span><span>{{cat.amount}}</span></li>
                            </ul>
                        </div>
                    </div>
            </div>
            <div class="nbd-items">
                <div class="nbd-title-item">Current background</div>
                <img class="current_background" ng-src="{{stages[currentStage].canvas.backgroundImage.getSrc()}}" alt="">
                <label nbd-upload-bg=uploadBackgroundImage(files) class="nbd-button upload-background">upload background image<input type="file" style="display:none"></label>
            </div>
            
            <div class="nbd-items colors-items">
                <ul class="main-color-palette nbd-perfect-scroll" >
                    <li class="color-palette-add" ng-init="showbgPalette = false" ng-click="showbgPalette = !showbgPalette;" ng-style="{'background-color': backgroundColor}"></li>
                    <li ng-repeat="color in listBackgroundColor track by $index" class="color-palette-item" ng-click="changeBackgroundColor(color)" data-color="{{color}}" title="{{color}}" ng-style="{'background-color': color}"></li>
                </ul>
                <div class="nbd-text-color-picker" id="nbd-bg-color-palette" ng-class="showbgPalette ? 'active' : ''" >
                    <spectrum-colorpicker
                        ng-model="backgroundColor"
                        options="{
                                preferredFormat: 'hex',
                                color: '#3e4653',
                                flat: true,
                                showButtons: false,
                                showInput: true,
                                containerClassName: 'nbd-sp'
                        }">
                    </spectrum-colorpicker>
                    <div style="text-align: <?php echo (is_rtl()) ? 'right' : 'left'?>">
                        <button class="nbd-button" ng-click="addColorBackground();changeBackgroundColor(backgroundColor);showbgPalette = false;"><?php esc_html_e('Choose','web-to-print-online-designer'); ?></button>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            
            <div class="nbd-items-dropdown" style="padding:10px;">
                <div>
                <div class="background-wrap">
                        <div class="background-item" nbd-drag="background.url" extenal="false" type="svg"  ng-repeat="background in resource.background.filteredBackgrounds | limitTo: resource.background.filter.currentPage * resource.background.filter.perPage" repeat-end="onEndRepeat('background')" >
                                <img  ng-src="{{background.url}}" ng-click="addBackgrounds(background, true, true)" alt="{{background.name}}" >
                        </div>
                </div>
                <div class="loading-photo" style="width: 40px; height: 40px;">
                        <svg class="circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                        </svg>
                </div>
                <div class="tab-load-more" style="display: none;" ng-show="!resource.background.onload && resource.background.filteredBackgrounds.length && resource.background.filter.currentPage * resource.background.filter.perPage < resource.background.filter.total">
                        <a class="nbd-button" ng-click="scrollLoadMore('#tab-background', 'background')"><?php _e('Load more','web-to-print-online-designer');?></a>
                </div>
                </div>
            </div>
        </div>         
    </div>
    <style type="text/css">
        
        .sp-container{background-color: unset;border: none; width: auto;}
        .sp-picker-container{border:none;clear: both;}
        #tab-background .sp-container .sp-palette-container{width: 90%;border: none;}
        #nav-background #background-icon{fill:#fff;}
        #nav-background.active #background-icon{fill:#404762;}
        .nbd-workspace .tabs-content.mobile{height: 190px !important}
        .nbd-workspace .main.mobile{z-index: 111111}
        .nbd-workspace .tabs-content.mobile #tab-background .sp-container .sp-palette-container{display: none;}
        .nbd-sidebar #tab-background .tab-main{height:calc(100% - 20px);}
        .nbd-sidebar #tab-background .nbd-items-dropdown span{font-size:12px;color:#404762}
        .nbd-sidebar #tab-background .nbd-items-dropdown .info-support span{font-size:16px}
        .nbd-sidebar #tab-background .nbd-items-dropdown .main-items{position:relative}
        .nbd-sidebar #tab-background .nbd-items-dropdown .main-items .items .item{width:33.33%}
        .nbd-sidebar #tab-background .nbd-items-dropdown .main-items .items .item .main-item{border-radius:2px;cursor:pointer;-webkit-transition:-webkit-box-shadow .3s;transition:-webkit-box-shadow .3s;transition:box-shadow .3s;transition:box-shadow .3s,-webkit-box-shadow .3s;border:none}
        .nbd-sidebar #tab-background .nbd-items-dropdown .main-items .items .item .main-item:hover .item-svg{-webkit-box-shadow:1px 0 10px rgba(0,0,0,.1);box-shadow:1px 0 10px rgba(0,0,0,.1)}
        .nbd-sidebar #tab-background .nbd-items-dropdown .main-items .items .item .main-item .item-svg{background:#fff;padding:20px;border-radius:2px}
        .nbd-sidebar #tab-background .nbd-items-dropdown .main-items .items .item .main-item .item-svg svg{width:40px}
        .nbd-sidebar #tab-background .nbd-items-dropdown .main-items .items .item .main-item .item-info{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;padding:5px}
        .nbd-sidebar #tab-background .nbd-items-dropdown .result-loaded{margin-top:0}
        .nbd-sidebar #tab-background .nbd-items-dropdown .result-loaded .nbdesigner-gallery .nbdesigner-item{width:33.33%}
        .nbd-sidebar #tab-background .backgrounds-category{padding:0 10px;display: flex;}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button{width:100%;margin:0;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;overflow:unset;font-size:12px;text-transform:capitalize;}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button .nbd-sub-dropdown{width:100%;top:calc(100% + 5px)}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button .nbd-sub-dropdown:after,.nbd-sidebar #tab-background .backgrounds-category .nbd-button .nbd-sub-dropdown:before{display:none}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button ul{min-width:220px;max-height:250px;margin:10px 0}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button ul li{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding:0 20px}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button ul li span{color:#404762;text-transform:capitalize}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button ul li:hover{background-color:hsla(0,0%,62%,.2)}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button i,.nbd-sidebar #tab-background .backgrounds-category .nbd-button span{color:#fff}
        .nbd-sidebar #tab-background .backgrounds-category .nbd-button i{font-size:24px}
        .nbd-sidebar #tab-background .nbd-search {position: relative;left:0;}
        /*style.min.css*/
        <?php include_once(__DIR__.'/include/style.min.css'); ?>
        <?php if(is_rtl()):?>
            /*nbdesigner-rtl.css*/
            .nbdesigner_background_modal_header > span {
                float: right;
            }
        <?php endif; ?>
        /*modern-additional.css*/
        .background-wrap .background-item {
            visibility: visible !important; 
            width: 33.33%;
            padding: 2px;
            opacity: 0;
            z-index: 3;
            cursor: pointer;
        }
        .background-wrap .background-item.in-view {
            opacity: 1;
        }
        .mansory-wrap .mansory-item
        .nbd-sidebar #tab-background .backgrounds-category {
            margin-top: 70px;
            padding: 0px 10px 10px;        
        }

    </style>
    <?php
}

add_action( 'wp_enqueue_scripts', 'bagr_enqueue_scripts' );
function bagr_enqueue_scripts(){
    $dokanStyle = file_get_contents(__DIR__.'/include/dokan.css');
    wp_add_inline_style('nbd-dokan-product',$dokanStyle);
}

add_action('nbod_after_nav_typos','bagr_add_background_nav');
function bagr_add_background_nav($nbav_active_tabs){
    ?>
    <li id="nav-background" data-tour="backgrounds" data-tour-priority="7" class="<?php if( $nbav_active_tabs['active_background'] ) echo 'active' ;?> tab animated <?php echo $animation_dir; ?> animate900 tab-background" ng-click="disableDrawMode();getResource('background', '#tab-background', true)">
            <svg id="background-icon" height="25" viewBox="0 0 64 64" width="28" xmlns="http://www.w3.org/2000/svg"><path d="m21 8a1 1 0 0 0 -1 1v9h2v-9a1 1 0 0 0 -1-1z"/><path d="m12 39a3 3 0 0 0 6 0v-30a3 3 0 0 1 6 0v12a3 3 0 0 0 6 0v-17h-18z"/><path d="m47 4h-15v14h15a1 1 0 0 0 1-1v-12a1 1 0 0 0 -1-1z"/><path d="m4 9h2v4h-2z"/><path d="m8 5v12a1 1 0 0 0 1 1h1v-14h-1a1 1 0 0 0 -1 1z"/><path d="m54 12h3a1 1 0 0 1 1 1v9.429a1 1 0 0 1 -.664.942l-23.336 8.334v10.295h2v-8.429a1 1 0 0 1 .664-.942l23.336-8.329v-14.3h-6z"/><path d="m50 9h2v4h-2z"/><path d="m31 44h8v16h-8z"/></svg>
            <span><?php _e('Backgrounds','web-to-print-online-designer'); ?></span></li>     
    <?php
}

add_action('nbod_before_local_font_setting','bagr_add_background_local_setting',10,3);
function bagr_add_background_local_setting($nbes_enable_settings,$nbes_settings){
    $select_all_background_cat = false;
    if( !$nbes_settings ){
        $select_all_background_cat=true;
    }
    $background_cat_path    = NBDESIGNER_DATA_DIR . '/background_cat.json';
    $background_cats        = file_exists( $background_cat_path ) ? (array)json_decode( file_get_contents( $background_cat_path ) ) : array();
    ?>
    <div class="nbo-form-field">
        <label><b><?php _e( 'Background settings', 'web-to-print-online-designer' ); ?></b></label>
        <div class="nbo-option-val">
            <input type="hidden" value="0" name="_nbes_enable_settings[background]"/>
            <input type="checkbox" value="1" name="_nbes_enable_settings[background]" id="_nbes_background_category_enable" data-ls-toggle="nbes_background_cats" <?php checked( $nbes_enable_settings['background'] ); ?> class="short" />
            <label for="_nbes_background_category_enable"><?php _e('Enable', 'web-to-print-online-designer'); ?></label>
        </div>
    </div>
    <div class="nbo-form-field nbes-depend" id="nbes_bkground_cats">
        <label><b><?php _e( 'Select background category', 'web-to-print-online-designer' ); ?></b></label>
        <div class="nbo-option-val">
            <select name="_nbes_settings[background_cats][]" multiple class="nbes-slect-woo">
                <?php 
                    foreach( $background_cats as $background_cat ): 
                        $selected = ( $select_all_background_cat || in_array( $background_cat->id, $nbes_settings['background_cats'] ) ) ? ' selected="selected" ' : '';
                ?>
                <option value="<?php echo $background_cat->id; ?>" <?php echo $selected; ?>><?php echo $background_cat->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <hr />
    <?php
}

add_filter('nbdesigner_default_frontend_settings','bagr_background_default_frontend_settings');
function bagr_background_default_frontend_settings($default_settings){
    $default_settings['nbdesigner_enable_background'] = 'yes';
    $default_settings['nbdesigner_background_change_path_color'] = 1;           
    $default_settings['nbdesigner_background_rotate'] = 1;     
    $default_settings['nbdesigner_background_opacity'] = 1;
    return $default_settings;
}

add_filter('nbd_admin_pages','bagr_background_admin_page');
function bagr_background_admin_page($admin_pages){
    $admin_pages[] = 'nbdesigner_page_nbdesigner_manager_backgrounds';
    return $admin_pages;
}

add_action('nbd_init_files_and_folders','bagr_background_create_files_folders');
function bagr_background_create_files_folders(){
    Nbdesigner_IO::mkdir(NBDESIGNER_BACKGROUND_DIR);
}

add_filter('nbod_get_resource_before_send','bagr_get_background_resource',10,2);
function bagr_get_background_resource($return,$rq_type){
    if ($rq_type=='background'){
        $path_cat = NBDESIGNER_DATA_DIR . '/background_cat.json';
        $path_background = NBDESIGNER_DATA_DIR . '/backgrounds.json';
        $return['data']['cat'] = $return['data']['backgrounds'] =array();
        if( file_exists($path_cat) ){
            $_cat = file_get_contents($path_cat);
            $return['data']['cat'] = $_cat == '' ? array() : json_decode($_cat);
        }
        if( file_exists($path_background) ){
            $_background = file_get_contents($path_background);
            $return['data']['backgrounds'] = $_background == '' ? array() : json_decode($_background);
        } 
    }
    return $return;
}

add_action( 'wp_ajax_nbdesigner_add_background_cat', 'nbdesigner_add_background_cat' );
add_action( 'wp_ajax_nbdesigner_delete_background_cat', 'nbdesigner_delete_background_cat' );
add_action( 'wp_ajax_nbdesigner_delete_background', 'nbdesigner_delete_background' );
add_action( 'wp_ajax_nopriv_nbdesigner_get_background', 'nbdesigner_get_background' );

add_filter('nbod_ajax_events','bagr_background_ajax_events');
function bagr_background_ajax_events($ajax_events){
    $ajax_events['nbdesigner_add_background_cat'] = false;
    $ajax_events['nbdesigner_delete_background_cat'] = false;
    $ajax_events['nbdesigner_delete_background'] = false;
    $ajax_events['nbdesigner_get_background'] = true;
    return $ajax_events;
}

add_filter('nbd_admin_hooks_need_asset','bagr_add_background_assets');
function bagr_add_background_assets($hooks){
    $hooks[] = 'nbdesigner_page_nbdesigner_manager_backgrounds';
    return $hooks;
}

add_action( 'admin_enqueue_scripts', 'bagr_admin_enqueue_scripts' , 31, 1 );
function bagr_admin_enqueue_scripts($hook){
    if ($hook == 'nbdesigner_page_nbdesigner_manager_backgrounds'){
        wp_enqueue_style( 'nbdesigner_sweetalert_css', NBDESIGNER_CSS_URL . 'sweetalert.css' );
        wp_enqueue_script( 'nbdesigner_sweetalert_js', NBDESIGNER_JS_URL . 'sweetalert.min.js' , array( 'jquery' ) );
    }
    wp_add_inline_script('admin_nbdesigner',"var bagr_background = true;");
    wp_add_inline_style('nbd-rtl','
        #nbdesigner_cancel_add_background_cat {
            margin-right: 15px;
        }
    ');
    wp_add_inline_style('admin_nbdesigner',file_get_contents(__DIR__.'/include/admin-nbdesigner.css'));
}

add_action('nbd_menu','bagr_background_menu');
function bagr_background_menu(){
    if(current_user_can('manage_nbd_background')){ 
        add_submenu_page(
                'nbdesigner', __('Manager Backgrounds', 'web-to-print-online-designer'), __('Backgrounds', 'web-to-print-online-designer'), 'manage_nbd_background', 'nbdesigner_manager_backgrounds', 'bagr_nbdesigner_manager_backgrounds'
        );
    }
}

function bagr_nbdesigner_manager_backgrounds() {
    defined('NBDESIGNER_BACKGROUND_URL') or bagr_define();
    global $nb_designer;
    $notice = '';
    $current_background_cat_id = 0;
    $background_id = 0;
    $update = false;
    $cats = array("0");
    $list = $nb_designer->nbdesigner_read_json_setting(NBDESIGNER_DATA_DIR . '/backgrounds.json');
    $cat = $nb_designer->nbdesigner_read_json_setting(NBDESIGNER_DATA_DIR . '/background_cat.json');
    $total = sizeof($list);
    $limit = 40;
    if (is_array($cat))
        $current_background_cat_id = sizeof($cat);
    if (isset($_GET['id'])) {
        $background_id = $_GET['id'];
        $update = true;
        if (isset($list[$background_id])) {
            $background_data = $list[$background_id];
            $cats = $background_data->cat;
        }
    }
    $page = filter_input(INPUT_GET, "p", FILTER_VALIDATE_INT);
    $current_cat = filter_input(INPUT_GET, "cat_id", FILTER_VALIDATE_INT);

    if (isset($_POST[$nb_designer->plugin_id . '_hidden']) && wp_verify_nonce($_POST[$nb_designer->plugin_id . '_hidden'], $nb_designer->plugin_id) && current_user_can('edit_nbd_background')) {
        $background = array();
        $background['id'] = $_POST['nbdesigner_background_id'];
        $background['cat'] = $cats;
        if (isset($_POST['nbdesigner_background_cat'])) $background['cat'] = $_POST['nbdesigner_background_cat'];
        if (isset($_FILES['svg'])) {
            $files = $_FILES['svg'];
            foreach ($files['name'] as $key => $value) {
                $file = array(
                  'name'     => $files['name'][$key],
                  'type'     => $files['type'][$key],
                  'tmp_name' => $files['tmp_name'][$key],
                  'error'    => $files['error'][$key],
                  'size'     => $files['size'][$key]
                );                    
                $uploaded_file_name = basename($file['name']);
                $allowed_file_types = array('svg', 'png', 'jpg', 'jpeg');
                if (Nbdesigner_IO::checkFileType($uploaded_file_name, $allowed_file_types)) {
                    $upload_overrides = array('test_form' => false);
                    $uploaded_file = wp_handle_upload($file, $upload_overrides);
                    if (isset($uploaded_file['url'])) {
                        $new_path_background = Nbdesigner_IO::create_file_path(NBDESIGNER_BACKGROUND_DIR, $uploaded_file_name);
                        $background['file'] = $uploaded_file['file'];
                        $background['url'] = $uploaded_file['url'];
                        $background['name'] = $_POST['bg_name'][$key];
                        if (!copy($background['file'], $new_path_background['full_path'])) {
                            $notice = apply_filters('nbdesigner_notices', nbd_custom_notices('error', __('Failed to copy.', 'web-to-print-online-designer')));
                        }else{
                            $background['file'] = $new_path_background['date_path'];
                            $background['url'] = $new_path_background['date_path'];
                            
                        }                                               
                        if ($update) {
                            nbdesigner_update_list_backgrounds($background, $background_id);
                        } else {
                            nbdesigner_update_list_backgrounds($background);
                        }
                        $notice = apply_filters('nbdesigner_notices', nbd_custom_notices('success', __('Your background has been saved.', 'web-to-print-online-designer')));

                    } else {
                        $notice = apply_filters('nbdesigner_notices', nbd_custom_notices('error', sprintf(__( 'Error while upload file, please try again! <a target="_blank" href="%s">Force upload SVG</a>', 'web-to-print-online-designer'), esc_url(admin_url('admin.php?page=nbdesigner&tab=general#nbdesigner_option_download_type')))));
                    }
                } else {
                    $notice = apply_filters('nbdesigner_notices', nbd_custom_notices('error', __('Incorrect file extensions.', 'web-to-print-online-designer')));
                }
            }
        }
        $list = $nb_designer->nbdesigner_read_json_setting(NBDESIGNER_DATA_DIR . '/backgrounds.json');
        $cats = $background['cat'];
        $total = sizeof($list);
        
    }
    $current_cat_id = 0;
    $name_current_cat = 'uploaded';
    if($total){
        if(isset($current_cat)){
            $current_cat_id = $current_cat;
            $new_list = array();
            foreach($list as $background){  
                if(in_array((string)$current_cat, $background->cat)) $new_list[] = $background;
                if(($current_cat == 0) && sizeof($background->cat) == 0) $new_list[] = $background;
            }
            foreach($cat as $c){
                if($c->id == $current_cat){
                    $name_current_cat = $c->name;
                    break;
                } 
                $name_current_cat = 'uploaded';
            }
            $list = $new_list;
            $total = sizeof($list);               
        }else{
            $name_current_cat = 'uploaded';
        }
        if(isset($page)){
            $_tp = ceil($total / $limit);
            if($page > $_tp) $page = $_tp;
            $_list = array_slice($list, ($page-1)*$limit, $limit);
        }else{
            $_list = $list;
            if($total > $limit) $_list = array_slice($list, 0, $limit); 
        }
    } else{
        $_list = array();
    }        
    if(isset($current_cat)){
        $url = add_query_arg(array('cat_id' => $current_cat), admin_url('admin.php?page=nbdesigner_manager_backgrounds'));
    }else{
        $url = admin_url('admin.php?page=nbdesigner_manager_backgrounds');   
    }
    require_once NBDESIGNER_PLUGIN_DIR . 'includes/class.nbdesigner.pagination.php';
    $paging = new Nbdesigner_Pagination();
    $config = array(
        'current_page'  => isset($page) ? $page : 1, 
        'total_record'  => $total,
        'limit'         => $limit,
        'link_full'     => $url.'&p={p}',
        'link_first'    => $url              
    );          
    $paging->init($config);
    include_once(__DIR__ . '/views/nbdesigner-manager-backgrounds.php');
}

function nbdesigner_add_background_cat() {  
    global $nb_designer;  
    $data = array(
            'mes'   =>  __('You do not have permission to add/edit background category!', 'web-to-print-online-designer'),
            'flag'  => 0
        );          
    if (!wp_verify_nonce($_POST['nonce'], 'nbdesigner_add_cat') || !current_user_can('edit_nbd_background')) {
        echo json_encode($data);
        wp_die();
    }
    $path = NBDESIGNER_DATA_DIR . '/background_cat.json';
    $cat = array(
        'name' => sanitize_text_field($_POST['name']),
        'id' => $_POST['id']
    );
    $nb_designer->nbdesigner_update_json_setting($path, $cat, $cat['id']);
    $data['mes'] = __('Category has been added/edited successfully!', 'web-to-print-online-designer');
    $data['flag'] = 1;        
    echo json_encode($data);
    wp_die();
}

function nbdesigner_delete_background_cat() {
    global $nb_designer;
    $data = array(
            'mes'   =>  __('You do not have permission to delete bakground category!', 'web-to-print-online-designer'),
            'flag'  => 0
        );          
    if (!wp_verify_nonce($_POST['nonce'], 'nbdesigner_add_cat') || !current_user_can('delete_nbd_background')) {
        echo json_encode($data);
        wp_die();
    }
    $path = NBDESIGNER_DATA_DIR . '/background_cat.json';
    $id = $_POST['id'];
    $nb_designer->nbdesigner_delete_json_setting($path, $id, true);
    $background_path = NBDESIGNER_DATA_DIR . '/backgrounds.json';
    $nb_designer->nbdesigner_update_json_setting_depend($background_path, $id);
    $data['mes'] = __('Category has been delete successfully!', 'web-to-print-online-designer');
    $data['flag'] = 1;        
    echo json_encode($data);
    wp_die();
}

function nbdesigner_update_list_backgrounds($background, $id = null) {
    global $nb_designer;
    $path = NBDESIGNER_DATA_DIR . '/backgrounds.json';
    if (isset($id)) {
        $nb_designer->nbdesigner_update_json_setting($path, $background, $id);
        return;
    }
    $list_background = array();
    $list = $nb_designer->nbdesigner_read_json_setting($path);
    if (is_array($list)) {
        $list_background = $list;
        $id = sizeOf($list_background);
        $background['id'] = (string) $id;
    }
    $list_background[] = $background;
    $res = json_encode($list_background);
    file_put_contents($path, $res);
}

function nbdesigner_delete_background() {
    global $nb_designer;
    $data = array(
            'mes'   =>  __('You do not have permission to delete background!', 'web-to-print-online-designer'),
            'flag'  => 0
        );
    if (!wp_verify_nonce($_POST['nonce'], 'nbdesigner_add_cat') || 
        !current_user_can('delete_nbd_background')) {
        echo json_encode($data);
        wp_die();
    }
    $id = $_POST['id'];
    $path = NBDESIGNER_DATA_DIR . '/backgrounds.json';
    $list = $nb_designer->nbdesigner_read_json_setting($path);
    $file_background = $list[$id]->file;
    if(file_exists($file_background)){
        unlink($file_background);
    }else{
        $file_background = NBDESIGNER_BACKGROUND_DIR . $list[$id]->file;
        unlink($file_background);
    }        
    $nb_designer->nbdesigner_delete_json_setting($path, $id);
    $data['mes'] = __('Background has been deleted successfully!', 'web-to-print-online-designer');
    $data['flag'] = 1;
    echo json_encode($data);
    wp_die();
}

add_filter('nbod_admin_role','bagr_admin_role');
function bagr_admin_role($admin_role){
    $admin_role->add_cap('manage_nbd_background');
    $admin_role->add_cap('export_nbd_background');
    $admin_role->add_cap('import_nbd_background');
    $admin_role->add_cap('edit_nbd_background');
    $admin_role->add_cap('delete_nbd_background');
    return $admin_role;
}

function _nbdesigner_get_background(){
    if (!wp_verify_nonce($_REQUEST['nonce'], 'nbdesigner-get-data') && NBDESIGNER_ENABLE_NONCE) {
        die('Security error');
    }   
    $result = array();
    $path = NBDESIGNER_DATA_DIR . '/backgrounds';
    $cats = Nbdesigner_IO::get_list_folder($path, 1);
    foreach ($cats as $key => $cat){
        $result['cat'][] = array(
            'name'  => basename($cat),
            'id'    => $key                
        );
        $list = Nbdesigner_IO::get_list_files($path . '/' . basename($cat), 1);
        $backgrounds = preg_grep('/\.(svg)(?:[\?\#].*)?$/i', $list);
        foreach($backgrounds as $k => $background) {
            $result['backgrounds'][] = array(
                'name'  => basename($background),
                'id'    => $k,
                'cat'   => array($key),
                'file'  => '',
                'url'   => Nbdesigner_IO::wp_convert_path_to_url($background)
            );
        }               
    }       
    $result['flag'] = 1;

    echo json_encode($result);
    wp_die();         
}

function nbdesigner_get_background(){
    global $nb_designer;
    if (!wp_verify_nonce($_REQUEST['nonce'], 'nbdesigner-get-data') && NBDESIGNER_ENABLE_NONCE) {
        die('Security error');
    }       
    $result = array();
    $path_cat = NBDESIGNER_DATA_DIR . '/background_cat.json';
    $path_background = NBDESIGNER_DATA_DIR . '/backgrounds.json';
    $result['flag'] = 1;
    $result['cat'] = $nb_designer->nbdesigner_read_json_setting($path_cat);
    $result['backgrounds'] = $nb_designer->nbdesigner_read_json_setting($path_background);   
    echo json_encode($result);
    wp_die();        
}

add_filter('nbdesigner_appearance_settings','bagr_nbdesigner_appearance_settings',15);
function bagr_nbdesigner_appearance_settings($settings){
    array_unshift($settings['modern'],array(
        'title'         => esc_html__('Show nbdesigner colorpallet', 'web-to-print-online-designer'),
        'id'            => 'nbdesigner_show_type_background',
        'description'   => esc_html__( 'Show type colorpallet in design editor.', 'web-to-print-online-designer'),
        'default'       => 'no',
        'type'          => 'radio',
        'options'       => array(
            'yes'   => esc_html__('Yes', 'web-to-print-online-designer'),
            'no'    => esc_html__('No', 'web-to-print-online-designer')
        ) 
    ));
    return $settings;
}


