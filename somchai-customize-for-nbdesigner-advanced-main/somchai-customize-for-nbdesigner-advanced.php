

<?php
/**
 * Plugin Name:       Somchai for NBDesigner Advanced
 * Plugin URI:        https://cmsmart.net
 * Description:       Somchai for NBDesigner Advanced
 * Version:           1.0.1
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            Hieu
 * Author URI:        https://cmsmart.net
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       somchai-customize-for-nbdesigner-advanced
 * Domain Path:       /languages
 */

add_action('nbd_js_config','bagr_somchai_js_config');
function bagr_somchai_js_config(){
    ?>
    NBDESIGNCONFIG['is_prenium'] = <?php echo check_is_prenium_user() ? 1 : 0 ?>;
    var link_redirect = '<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')) ?>';
    <?php
    if (!is_user_logged_in()) {
    ?>
        var get_u_login = 1;
    <?php
    }
    echo "var bagr_somchai_js_config = true;";

}
add_filter('nbod_frontend_products_design','somchai_frontend_products_design',10,2);
function somchai_frontend_products_design($result , $product_id){
    $k = count($result) - 1;
    $result[$k]['link_design'] = add_query_arg( array(
            'product_id'    => $product_id,
            'view'          => 'm'
        ),  getUrlPageNBD( 'create' ) );
    $_designer_setting = unserialize(get_post_meta($product_id, '_designer_setting',true));
    $option            = unserialize( get_post_meta( $product_id, '_nbdesigner_option', true ) );
    $product_size = '';
    if(isset($_designer_setting[0])){
        $product_size = $_designer_setting[0]['product_height'] . ' x ' . $_designer_setting[0]['product_width'] . ' ' . $option['unit'];
    }
    $result[$k]['product_size'] = $product_size;
    return $result;
}
add_action('nbd_custom_resize_main','add_nbd_custom_resize_main');
function add_nbd_custom_resize_main(){
    if(!isset($_GET['task'])){
        ?>
            <li class="menu-item item-view item-resize">
            <svg style="color: #FBBE28;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                <span><?php esc_html_e('Resize','web-to-print-online-designer'); ?></span>
                <div class="sub-menu sub-change-size" data-pos="left">
                    <div class="sub-size-wrap">
                        <div class="search-product">
                            <input class="inp-search" type="" ng-model="productName" placeholder="Search..." ng-change="searchSize()">
                            <span class="icon-search">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M15.2 16.34a7.5 7.5 0 1 1 1.38-1.45l4.2 4.2a1 1 0 1 1-1.42 1.41l-4.16-4.16zm-4.7.16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z"></path></svg>
                            </span>
                        </div>
                        <div class="sub-content">
                            <div class="custom-dimension" ng-hide="productName != ''">
                                <div class="title-custom"><?php esc_html_e('Custom dimension','web-to-print-online-designer'); ?></div>
                                <div class="dimension-box">
                                    <div class="dimension">
                                        <label><?php esc_html_e('Height','web-to-print-online-designer'); ?></label>
                                        <input ng-model="customWidth" max="200" step="1" min="0" type="number">
                                    </div>
                                    <div class="dimension">
                                        <label><?php esc_html_e('Width','web-to-print-online-designer'); ?></label>
                                        <input ng-model="customHeight" max="200" step="1" min="0" type="number">
                                    </div>
                                    <div class="unit">
                                        <div class="nbd-button nbd-dropdown">
                                            <span class="ng-binding">{{settings.nbdesigner_dimensions_unit}}</span>
                                            <i class="icon-nbd icon-nbd-chevron-right rotate90"></i>
                                            <div class="nbd-sub-dropdown" data-pos="center">
                                                <ul class="nbd-perfect-scroll">
                                                    <li ng-click="settings.nbdesigner_dimensions_unit = 'cm'"><span>cm</span></li>
                                                    <li ng-click="settings.nbdesigner_dimensions_unit = 'in'"><span>in</span></li>
                                                    <li ng-click="settings.nbdesigner_dimensions_unit = 'mm'"><span>mm</span></li>
                                                    <li ng-click="settings.nbdesigner_dimensions_unit = 'ft'"><span>ft</span></li>
                                                    <li ng-click="settings.nbdesigner_dimensions_unit = 'px'"><span>px</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="unit">
                                        <span class="nbd-button btn-check" ng-click="changeDimension()"><img src="<?php echo SOMCHAI_URL . 'images/check.png'; ?>"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="product-list">
                                <div class="title-custom"><?php esc_html_e('Product','web-to-print-online-designer'); ?></div>
                                <ul>
                                    <li ng-repeat="product in filterProducts"><a ng-href="{{product.link_design}}">{{product.name}}<span class="size">{{product.product_size}}</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="nbd-overlay"></div>
            </li>
            </li>
        <?php
    }
}

add_filter('nbod_variable_label_design','cusotm_nbod_variable_label_design');
function cusotm_nbod_variable_label_design($html){
    if( nbdesigner_get_option('nbdesigner_attachment_show_resize_product', 'no') == 'yes' ){
        $product_id = get_the_ID();
        $link_edit_design = add_query_arg(
        array(
            'product_id'    => $product_id,
            'view'          => 'm'),
        getUrlPageNBD('create'));
        ?>
            <a class="button alt nbdesign-button " terget="_blank" href="<?php echo($link_edit_design);?>" >
                <img class="nbdesigner-img-loading hide" src="<?php echo NBDESIGNER_PLUGIN_URL . 'assets/images/loading.gif'; ?>"/>
                 Start Design
            </a>
        <?php
    }else{
        return $html;
    }
}

add_filter('nbdesigner_general_settings','addon_show_custom_resize');
function addon_show_custom_resize($option){
            $bleed_arr = array(
                        'title'         => esc_html__( 'Resize products', 'web-to-print-online-designer'),
                        'description'   => esc_html__( 'Resize products.', 'web-to-print-online-designer'),
                        'id'            => 'nbdesigner_attachment_show_resize_product',
                        'default'       => 'no',
                        'type'          => 'radio',
                        'options'       => array(
                            'yes'   => esc_html__('Yes', 'web-to-print-online-designer'),
                            'no'    => esc_html__('No', 'web-to-print-online-designer')
                        )  
                    );
    array_push($option['customization'],$bleed_arr);
    return $option; 
}


add_action('nbd_js_config','bagr_background_color_js_config');
function bagr_background_color_js_config(){
    defined('NBDESIGNER_BACKGROUND_URL') or bagr_define();
    echo "var bagr_background_color = true;";
    echo 'NBDESIGNCONFIG.background_url = "'.NBDESIGNER_BACKGROUND_URL.'";';
    echo 'NBDESIGNCONFIG.nbdlangs.backgrounds = "'.__('Backgrounds', 'web-to-print-online-designer').'";';
}

add_action('nbod_before_tab_typography','remove_nbod_after_tab_typography');
function remove_nbod_after_tab_typography(){
    remove_action('nbod_after_tab_typography','bagr_add_background_tab');
}
add_action('nbod_extra_content_tab_design','bagr_add_background_tabs');
function bagr_add_background_tabs($nbav_active_tabs){
    ?>
    <div class="<?php if( $nbav_active_tabs['active_background'] ) echo 'active'; ?> tab" id="tab-background_color" data-container="#tab-background_color" nbd-scroll="scrollLoadMore(container, type)" data-type="background" data-offset="20">
        <div class="tab-main tab-scroll">
            <?php if(nbdesigner_get_option( 'nbdesigner_show_type_background' , 'no') == 'yes') : ?>
            <div class="inner-tab-layer">
                <spectrum-colorpicker
                    ng-model="colorBackground" 
                    ng-change="changeBackgroundColor(colorBackground)" 
                    nb-dragstart="changeBackgroundColor(colorBackground)"
                    options="{
                        allowEmpty: true,
                        showPaletteOnly: false, 
                        flat: true,
                        clickoutFiresChange: true,
                        togglePaletteOnly: false, 
                        showInitial: false,
                        showPalette: true, 
                        showSelectionPalette: false,
                        showButtons: false,
                        showAlpha: true,
                        preferredFormat: 'hex3',
                        palette: [
                            ['#000','#444','#666','#999','#ccc','#eee','#f3f3f3','#fff'],
                            ['#f00','#f90','#ff0','#0f0','#0ff','#00f','#90f','#f0f'],
                            ['#f4cccc','#fce5cd','#fff2cc','#d9ead3','#d0e0e3','#cfe2f3','#d9d2e9','#ead1dc'],
                            ['#ea9999','#f9cb9c','#ffe599','#b6d7a8','#a2c4c9','#9fc5e8','#b4a7d6','#d5a6bd'],
                            ['#e06666','#f6b26b','#ffd966','#93c47d','#76a5af','#6fa8dc','#8e7cc3','#c27ba0'],
                            ['#c00','#e69138','#f1c232','#6aa84f','#45818e','#3d85c6','#674ea7','#a64d79'],
                            ['#900','#b45f06','#bf9000','#38761d','#134f5c','#0b5394','#351c75','#741b47'],
                            ['#600','#783f04','#7f6000','#274e13','#0c343d','#073763','#20124d','#4c1130']
                            ],
                        showInput: true}">
                </spectrum-colorpicker>
            </div>
            <?php endif; ?>
            <?php if(nbdesigner_get_option( 'nbdesigner_attachment_show_upload_color' , 'no') == 'yes') : ?>
            <div class="backgrounds-category" ng-class="resource.background.data.cat.length > 0 ? '' : 'nbd-hiden'" >           
                <button class="cts tc_show_tab_color" ng-click="tabChangeBackgroundColor('color')" ><span>Colors</span></button>
                <button class="cts tc_show_tab_cat" ng-click="tabChangeBackgroundColor('gradient')" ><span>Colors Gradient</span></button>
                <button class="cts tc_show_tab_cat" ng-click="changeCat('background', cat);tabChangCat()" ng-repeat="cat in resource.background.data.cat"><span>{{cat.name}}</span></button>
            </div>
            <style type="text/css">
                .cts span {
                    color : #fff;
                }
                .doc-container{
                    display: inline-flex;
                }
                .fcolorpicker-curbox {
                    width: 40px !important; 
                    height: 40px !important;
                }
                .gradient-angle,.color-preview{
                    display: none !important;
                }
                .fcolorpicker-curbox{
                    background : none !important;
                }
                .color-gradient{
                    display : none ;
 
                }
            </style>
            
            <div class="nbd-items colors-items" id="tc_background_color">
                <ul class="main-color-palette nbd-perfect-scroll" >
                    <li class=" color-gradient to-right" id="colorpickerleft-tab" ></li>
                    <li class=" color-gradient to-left" id="colorpickerright-tab" ></li>
                    <li class=" color-gradient to-radian" id="colorpickerradial-tab" ></li>
                    <li class="color-palette-add" ng-init="showbgPalette = false" ng-click="showbgPalette = !showbgPalette;" ng-style="{'background-color': backgroundColor}"></li>
                    <li ng-repeat="color in listBackgroundColor track by $index" class="color-palette-item" ng-click="changeBackgroundColor(color)" data-color="{{color}}" title="{{color}}" ng-style="{'background-color': color}"></li>
                </ul>
                <div class="nbd-text-color-picker ps--active-y" id="nbd-bg-color-palette" ng-class="showbgPalette ? 'active' : ''" >
                    <spectrum-colorpicker
                        ng-model="backgroundColor"
                        options="{
                                preferredFormat: 'hex',
                                color: '#3e4653',
                                flat: true,
                                showButtons: false,
                                showInput: true,
                                containerClassName: 'nbd-sp'
                        }">
                    </spectrum-colorpicker>
                    <div style="text-align: <?php echo (is_rtl()) ? 'right' : 'left'?>">
                        <button class="nbd-button" ng-click="addColorBackground();changeBackgroundColor(backgroundColor);showbgPalette = false;"><?php esc_html_e('Choose','web-to-print-online-designer'); ?></button>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="nbd-items-dropdown" style="padding:10px; display: none;" id="tab-show-images">
                <div class="background_color-wrap">
                    <div class="background-item" nbd-drag="background.url" extenal="false" type="svg"  ng-repeat="background in resource.background.filteredBackgrounds | limitTo: resource.background.filter.currentPage * resource.background.filter.perPage" repeat-end="onEndRepeat('background')" >
                        <img  ng-src="{{background.url}}" ng-click="addBackgrounds(background, true, true)" alt="{{background.name}}" >
                    </div>
                </div>
                <div class="loading-photo" style="width: 40px; height: 40px;">
                    <svg class="circular" viewBox="25 25 50 50">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                    </svg>
                </div>
                <div class="tab-load-more" style="display: none;" ng-show="!resource.background.onload && resource.background.filteredBackgrounds.length && resource.background.filter.currentPage * resource.background.filter.perPage < resource.background.filter.total">
                        <a class="nbd-button" ng-click="scrollLoadMore('#tab-background_color', 'background')"><?php _e('Load more','web-to-print-online-designer');?></a>
                </div>
            </div>
        </div>         
    </div>
    <script>
                var xncolorpickerlefttab = new XNColorPicker({
                    color: "linear-gradient(90deg,rgba(168, 80, 215, 1) 0.0,rgba(35, 174, 234, 1) 100.0%)",
                    selector: "#colorpickerleft-tab",
                    showprecolor: true,
                    prevcolors: null,
                    showhistorycolor: false,
                    historycolornum: 16,
                    format: 'hex',
                    showPalette:true,
                    show:false, 
                    lang:'en',
                    // colorTypeOption:'single,linear-gradient,radial-gradient',
                    colorTypeOption:'linear-gradient',
                    canMove:false,
                    alwaysShow:false,
                    autoConfirm:true,
                    onError: function (e) {
    
                    },
                    onCancel:function(color){
                        console.log("cancel",color)
                    },
                    onChange:function(color){
                        var scope = angular.element(document.getElementById("designer-controller")).scope();
                        scope.setCanvasGradientBG(color,'left');

                    },
                    onConfirm:function(color){
                        console.log("confirm",color)
                    }
                });
                var xncolorpickerrighttab = new XNColorPicker({
                    color: "linear-gradient(270.0deg,rgba(168, 80, 215, 1) 0.0,rgba(35, 174, 234, 1) 100.0%)",
                    selector: "#colorpickerright-tab",
                    showprecolor: true,
                    prevcolors: null,
                    showhistorycolor: false,
                    historycolornum: 16,
                    format: 'hex',
                    showPalette:true,
                    show:false, 
                    lang:'en',
                    // colorTypeOption:'single,linear-gradient,radial-gradient',
                    colorTypeOption:'linear-gradient',
                    canMove:false,
                    alwaysShow:false,
                    autoConfirm:true,
                    onError: function (e) {
    
                    },
                    onCancel:function(color){
                        console.log("cancel",color)
                    },
                    onChange:function(color){
                        var scope = angular.element(document.getElementById("designer-controller")).scope();
                        scope.setCanvasGradientBG(color,'right');

                    },
                    onConfirm:function(color){
                        console.log("confirm",color)
                    }
                })
                var xncolorpickerradialtab = new XNColorPicker({
                    color: "linear-gradient(270.0deg,rgba(168, 80, 215, 1) 0.0,rgba(35, 174, 234, 1) 100.0%)",
                    selector: "#colorpickerradial-tab",
                    showprecolor: true,
                    prevcolors: null,
                    showhistorycolor: false,
                    historycolornum: 16,
                    format: 'hex',
                    showPalette:true,
                    show:false, 
                    lang:'en',
                    // colorTypeOption:'single,linear-gradient,radial-gradient',
                    colorTypeOption:'linear-gradient',
                    canMove:false,
                    alwaysShow:false,
                    autoConfirm:true,
                    onError: function (e) {
    
                    },
                    onCancel:function(color){
                        console.log("cancel",color)
                    },
                    onChange:function(color){
                        var scope = angular.element(document.getElementById("designer-controller")).scope();
                        scope.setCanvasGradientBG(color,'radial');

                    },
                    onConfirm:function(color){
                        console.log("confirm",color)
                    }
                })
            </script>
    <style type="text/css">
        .color-palette-add:after {
            content: "";
            background-image: url("<?php echo plugin_dir_url(__FILE__)."assets/color-picker_00000.jpg"?>") !important;
        }
        /*style.min.css*/
        <?php include_once (__DIR__.'/assets/include/style.min.css'); ?>
        <?php if(is_rtl()):?>
            /*nbdesigner-rtl.css*/
            .nbdesigner_background_modal_header > span {
                float: right;
            }
        <?php endif; ?>
        /*modern-additional.css*/
        .background_color-wrap{
            margin-top: 18px;
        }
       .background_color-wrap>.background-item>img {
            visibility: visible !important; 
            width: 33.33% !important;
            padding: 0 3px 3px;
            opacity: 1;
            z-index: 3;
            cursor: pointer;
            border-radius: 6px;
            float: left;
        }
        .background_color-wrap .background-item.in-view {
            opacity: 1;
        }
        .mansory-wrap .mansory-item
        .nbd-sidebar #tab-background_color .backgrounds-category {
            margin-top: 70px;
            padding: 0px 10px 10px;        
        }

        </style>
    <?php
}

add_action( 'nbod_inside_main_tabs', 'remove_my_action');
add_action('nbd_editor_extra_tab_nav','bagr_add_background_navaa');
add_action( 'nbod_inside_main_tabs', 'change_icon_template');

function remove_my_action() { 
    remove_action( 'nbod_after_nav_typos', 'bagr_add_background_nav');
    remove_action('nbod_after_nav_typos','bagr_add_background_nav');
}

function bagr_add_background_navaa($nbav_active_tabs){
    ?>
    <li id="nav-background" data-tour="backgrounds" data-tour-priority="7" class="<?php if( $nbav_active_tabs['active_background'] ) echo 'active' ;?> tab animated <?php echo $animation_dir; ?> animate900 tab-background" ng-click="disableDrawMode();getResource('background', '#tab-background_color', true)">
    <i id="background-icon" class="icon-nbd icon-nbd-fill-color"></i>
        <span><?php _e('Background','web-to-print-online-designer'); ?></span></li>     
    <?php
}


function change_icon_template(){
    ?>
    <g transform="matrix(-22.6473401516 0 0 22.6473401516 342 342)" id="Capa_1">
        <path style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" translate(-14.023500375, -14.024)" d="M 25.818 0.433 H 2.231 C 0.998 0.433 0 1.451 0 2.699 v 22.654 c 0 1.25 0.998 2.262 2.231 2.262 h 23.587 c 1.23 0 2.229 -1.012 2.229 -2.262 V 2.699 C 28.048 1.451 27.049 0.433 25.818 0.433 z M 3.644 2.06 h 20.763 c 1.083 0 1.963 0.895 1.963 1.994 V 8.54 H 1.68 V 4.054 C 1.68 2.954 2.556 2.06 3.644 2.06 z M 9.668 10.796 H 26.37 v 5.982 H 9.668 V 10.796 z M 1.68 23.995 V 10.796 h 5.812 v 15.193 H 3.644 C 2.556 25.99 1.68 25.097 1.68 23.995 z M 24.407 25.99 H 9.668 v -6.957 H 26.37 v 4.963 C 26.37 25.097 25.49 25.99 24.407 25.99 z" stroke-linecap="round"></path>
    </g>
    <?php
}

add_filter('tc_edit_templates_icon_tab','change_icon_template_tab');
    function change_icon_template_tab(){
    ?>
    <li id="nav-templates" data-tour="templates" data-tour-priority="1" ng-click="disableDrawMode();disablePreventClickMode();getProduct()" class="tab tab-first animated active <?php echo( $animation_dir ); ?> animate400 <?php if( $active_template ) echo 'active'; ?>">
        <svg id="template-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="28px" viewBox="0 0 684 684" xml:space="preserve">
            <g transform="matrix(-22.6473401516 0 0 22.6473401516 342 342)" id="Capa_1">
                <path style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1;" transform=" translate(-14.023500375, -14.024)" d="M 25.818 0.433 H 2.231 C 0.998 0.433 0 1.451 0 2.699 v 22.654 c 0 1.25 0.998 2.262 2.231 2.262 h 23.587 c 1.23 0 2.229 -1.012 2.229 -2.262 V 2.699 C 28.048 1.451 27.049 0.433 25.818 0.433 z M 3.644 2.06 h 20.763 c 1.083 0 1.963 0.895 1.963 1.994 V 8.54 H 1.68 V 4.054 C 1.68 2.954 2.556 2.06 3.644 2.06 z M 9.668 10.796 H 26.37 v 5.982 H 9.668 V 10.796 z M 1.68 23.995 V 10.796 h 5.812 v 15.193 H 3.644 C 2.556 25.99 1.68 25.097 1.68 23.995 z M 24.407 25.99 H 9.668 v -6.957 H 26.37 v 4.963 C 26.37 25.097 25.49 25.99 24.407 25.99 z" stroke-linecap="round"></path>
            </g>
        </svg>
            <span><?php esc_html_e('Templates','web-to-print-online-designer'); ?></span>
        </li>
    <?php
}
add_filter('nbd_change_icon_text_fields','tc_change_icon_text_fields');
function tc_change_icon_text_fields(){
    ?>
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="28px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;margin-top:5px;" xml:space="preserve">
        <g>
            <polygon class="st0" points="192,96 256,96 256,128 320,128 320,32 0,32 0,128 64,128 64,96 128,96 128,416 64,416 64,480 256,480 
                256,416 192,416 	"></polygon>
            <polygon class="st0" points="512,288 512,192 256,192 256,192 256,288 320,288 320,256 352,256 352,416 320,416 320,480 448,480 
                448,416 416,416 416,256 448,256 448,288 	"></polygon>
        </g>
        </svg>
        <span><?php esc_html_e('Text','web-to-print-online-designer'); ?></span>
    <?php
}

add_filter('nbod_popup_share','remove_nbod_popup_share');
function remove_nbod_popup_share(){
    return ;
}

add_filter('nbod_input_title','remove_nbod_input_title');
function remove_nbod_input_title(){
    return;
}

add_filter('nbod_update_customize_process','change_process_as_download',10,1);
function change_process_as_download($process_action){
    $task           = (isset($_GET['task']) &&  $_GET['task'] != '') ? $_GET['task'] : 'new';
    if( $task == 'create' || ( $task == 'edit' && ( isset( $_GET['design_type'] ) && $_GET['design_type'] == 'template' ) ) ){
        return $process_action;
    }
    ?>
    <li ng-click="ebookPreview()" class="menu-item item-edit">
        <span><?php esc_html_e('Peview','web-to-print-online-designer'); ?></span>
    </li>
    <li class="menu-item item-edit">
        <!-- <span class="title-menu txt-clr">Download</span>
        <div class="sub-menu abcd" data-pos="left">
            <ul class="abc">
                <li ng-click="saveDesign('png') "><span class="title-menu">PNG</span></li>
                <li ng-click="saveDesign('svg') "><span class="title-menu">SVG</span></li>
                <li ng-click="saveData('download-pdf') "><span class="title-menu">PDF</span></li>
            </ul>
        </div> -->
        <span><?php esc_html_e('Download','web-to-print-online-designer'); ?></span>
        <div class="sub-menu" data-pos="right">
            <div class="download-wrap">
                <div class="nbd-button type-download nbd-dropdown">
                    <span class="ng-binding">{{labelDownload}}</span>
                    <i class="icon-nbd icon-nbd-chevron-right rotate90"></i>
                    <div class="nbd-sub-dropdown" data-pos="center">
                        <ul class="nbd-perfect-scroll">
                            <li ng-click="typeDownload = 'png';labelDownload = 'PNG';"><span><?php esc_html_e('PNG','web-to-print-online-designer'); ?></span></li>
                            <li ng-click="typeDownload = 'download-jpg';labelDownload = 'CMYK JPG';"><span><?php esc_html_e('CMYK JPG','web-to-print-online-designer'); ?></span></li>
                            <li ng-click="typeDownload = 'svg';labelDownload = 'SVG';" class="transpdf">
                                <span><?php esc_html_e('SVG','web-to-print-online-designer'); ?></span>
                                <svg style="color: #FBBE28" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.75 17.5h-9a.75.75 0 0 0 0 1.5h9a.75.75 0 0 0 0-1.5Z" fill="currentColor"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M12.98 6.81a1.5 1.5 0 1 0-1.46.001c-.762 2.086-1.588 3.109-2.353 3.194-.862.095-1.613-.28-2.32-1.21a.807.807 0 0 0-.145-.147 1.5 1.5 0 1 0-1.204.602c0 .058.009.12.024.182l1.216 4.864A2.25 2.25 0 0 0 8.921 16h6.658a2.25 2.25 0 0 0 2.183-1.704l1.216-4.864a.81.81 0 0 0 .024-.182 1.5 1.5 0 1 0-1.203-.602.806.806 0 0 0-.136.135c-.743.935-1.5 1.314-2.33 1.222-.73-.082-1.56-1.108-2.353-3.194Z" fill="currentColor"></path></svg>
                            </li>
                            <li ng-click="typeDownload = 'rmbg';labelDownload = 'Transparent PNG';" class="transpdf">
                                <span><?php esc_html_e('Transparent PNG','web-to-print-online-designer'); ?></span>
                                <svg style="color: #FBBE28" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.75 17.5h-9a.75.75 0 0 0 0 1.5h9a.75.75 0 0 0 0-1.5Z" fill="currentColor"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M12.98 6.81a1.5 1.5 0 1 0-1.46.001c-.762 2.086-1.588 3.109-2.353 3.194-.862.095-1.613-.28-2.32-1.21a.807.807 0 0 0-.145-.147 1.5 1.5 0 1 0-1.204.602c0 .058.009.12.024.182l1.216 4.864A2.25 2.25 0 0 0 8.921 16h6.658a2.25 2.25 0 0 0 2.183-1.704l1.216-4.864a.81.81 0 0 0 .024-.182 1.5 1.5 0 1 0-1.203-.602.806.806 0 0 0-.136.135c-.743.935-1.5 1.314-2.33 1.222-.73-.082-1.56-1.108-2.353-3.194Z" fill="currentColor"></path></svg>
                            </li>
                            <li ng-click="typeDownload = 'download-pdf';labelDownload = 'PDF';"><span><?php esc_html_e('PDF','web-to-print-online-designer'); ?></span></li>
                        </ul>
                    </div>
                </div>
                <div class="stages-download" ng-repeat="stage in stages">
                    <label><input type="checkbox" ng-model="stage.config.download_flag"> {{stage.config.name}}</label>
                </div>
                <div class="btn-download">
                    <button ng-click="downloadDesignPage();" class="nbd-button">
                        <span><?php esc_html_e('Download','web-to-print-online-designer'); ?></span>
                    </button>
                </div>
            </div>
            
        </div>
    </li>
    <?php
}
add_action('nbd_extra_css','templates_style');
 function templates_style($path){
    $path = plugin_dir_url(__FILE__) . 'assets/style.css';
    ?>
    <link rel="stylesheet" href="<?= $path; ?>">
    <link type="text/css" rel="stylesheet" media="all" href = "<?php echo plugin_dir_url( __FILE__ ) .'assets/css/sofm.css'; ?>">
    <?php
 }

function add_my_stylesheet() 
{
    wp_enqueue_style( 'myCSS', plugins_url( '/assets/admin.css', __FILE__ ) );
}
add_action('admin_print_styles', 'add_my_stylesheet');


add_filter('nbd_template_ai_all','tppl_template_all',10,2);
function tppl_template_all($content,$html){
    $id = get_current_user_id ();
    $uow = get_user_meta($id, 'staff_somchai');
  $rd = wc_clean($_GET['rd']);
  if($rd == 'admin_templates' || ($rd == 'my_store_design' && $uow[0] == 'Yes')){
       $content .= '
           <label for="check_pro" style="font-weight: 100;cursor: pointer;">Enable Pro</label>
           <input type="checkbox" ng-model="chech_pro" name="check_pro" value="" id="check_pro" style="width: 30px; height: 18px;cursor: pointer;">
           <div class="price_tl">
           <label class="template-lable">Price ($)</label>
           <input type="number" class="add_price" name="" ng-model="price_template">
           </div>
       ';
  } else if ($rd == 'my_store_design') {
      $content .= '
           <label for="check_sale_design" style="font-weight: 100;cursor: pointer;">Designer</label>
           <input type="checkbox" ng-model="chech_design" name="check_sale_design" value="" id="check_sale_design" style="width: 30px; height: 18px;cursor: pointer;">
           <div class="price_tl">
           <label class="template-lable">Price ($)</label>
           <input type="number" class="add_price" name="" ng-model="price_template_design">
           </div>
       ';
  }
 return $content;
}

function custom_user_profile_fields() {
    $uid = $_GET['user_id'];
    if( $uid != '') {
        $uow = get_user_meta($uid, 'staff_somchai');
?>
    <table class="form-table">
        <tr>
            <th>
                <label for="user_location"><?php _e( 'Staff' ); ?></label>
            </th>
            <td>
                <input type="checkbox" <?php if($uow[0] == 'Yes') echo 'checked'; ?> value="Yes" name="staff_somchai">
            </td>
        </tr>
    </table>
<?php
    }
}
add_action( 'show_user_profile', 'custom_user_profile_fields' );
add_action( 'edit_user_profile', 'custom_user_profile_fields' );

function update_profile_user_wp($user_id)
{
    if($_POST['staff_somchai'] == ''){
        update_user_meta($user_id, 'staff_somchai', 'No');
    } else if (current_user_can('edit_user', $user_id)) {
        update_user_meta($user_id, 'staff_somchai', $_POST['staff_somchai']);
    }
}
add_action('edit_user_profile_update', 'update_profile_user_wp');

add_filter('manage_users_columns', 'custom_add_user_id_column');
add_filter('manage_users_custom_column',  'custom_show_user_id_column_content', 10, 3);

function custom_add_user_id_column($columns)
{
    $columns['staff'] = 'Staff';
    return $columns;
}

function custom_show_user_id_column_content($value, $column_name, $user_id)
{
    $user = get_userdata($user_id);
    if ('staff' == $column_name) {
        $staff = get_user_meta($user_id, 'staff_somchai');
        $default_staff = '';
        if ($staff[0] == 'Yes') {
            $default_staff = 'Yes';
        }
        else if ($staff[0] == 'No'){
            $default_staff = 'No';
        }
        else {
            $default_staff = '';
        }
        return $default_staff;
    }
    return $value;
}
 
 add_action('nbd_js_config','tppl_add_js');
 function tppl_add_js(){
    ?>
      var tppl_add_js = 1;
    <?php 
 }

add_filter('nbod_dislay_kind_of_templates','tppl_dislay_kind_of_templates',10,2);
function tppl_dislay_kind_of_templates($html, $valid_license)
{
?>
    <div class="modalCheckpr">
        <div class="box-check_price">
            <div class="ct-price">Price</div>
            <form name="myForm" ng-submit="login()">
                <div class="c_all_price">
                    <label for="cpr_free">
                        <div class="pr-5">
                            <input type="checkbox" id="cpr_free" name="priceTempalte" ng-model="prfree" ng-click="checkerPcfree($event)">
                        </div>
                        <div>
                            <p>Free</p>
                        </div>
                    </label>
                    <label for="cpr_pro">
                        <div class="pr-5">
                            <input type="checkbox" id="cpr_pro" name="priceTempalte" ng-model="prpro" ng-click="checkerPcpro($event)">
                        </div>
                        <div class="cpr_pro-title">
                            <p>Pro</p>
                            <svg viewbox="0 0 140 140">
                                <path fill="gold">
                                    <animate attributeName="d" dur="1440ms" repeatCount="indefinite" values="M 10,110 L 10,10 L 40,50 L 70,10 L 100,50 L 130,10 L 130,110 z;M 30,110 L 0,0 L 50,50 L 70,0 L 90,50 L 140,0 L 110,110 z;M 10,110 L 10,10 L 40,50 L 70,10 L 100,50 L 130,10 L 130,110 z;" />
                                </path>
                            </svg>
                        </div>
                    </label>
                     <label for="cpr_design">
                <div class="pr-5">
                    <input type="checkbox" id="cpr_design" name="priceTempalte" ng-model="prdesign" ng-click="checkerPcdesign($event)">
                </div>
                <div>
                    <p>Designer</p>
                </div>
            </label>
                </div>
                <div class="btn-filter">
                    <button type="submit">Filter</button>
                </div>
            </form>
        </div>
    </div>

    <div ng-style="{'display': settings.task == 'create_template' ? 'none' : 'inline-block' }" temp="{{temp}}" class="item" ng-repeat="temp in resource.templates | limitTo: resource.templateLimit" ng-class="(temp.pro || temp.price > 0) ? 'd_pro' : 'd_design'" ng-click="insertTemplate(false, temp);addWatermark(temp);" id="tpl-filter">
        <div class="main-item">
            <div class="item-img" ng-class="{'c_pro': temp.price > 0,'c_design': temp.priceDesign > 0,'tfree': temp.price == undefined || temp.priceDesign == undefined }" nbd-template-hover="{{temp.id}}">
                <img ng-src="{{temp.thumbnail}}" alt="<?php esc_html_e('Template', 'web-to-print-online-designer'); ?>">
            </div>
        </div>
    </div>
    <hr ng-show="resource.templates.length > 0 && resource.globalTemplate.data.length > 0" class="seperate2" />
    <div class="item" ng-repeat="temp in resource.globalTemplate.data" ng-click="insertGlobalTemplate(temp.id, $index)">
        <div class="main-item" image-on-load="temp.thumbnail">
            <div class="item-img item-img-global-tem">
                <img ng-src="{{temp.thumbnail}}" alt="{{temp.name}}">
                <?php if (!$valid_license) : ?>
                    <span class="nbd-pro-mark-wrap" ng-if="$index > 4">
                        <svg class="nbd-pro-mark" fill="#F3B600" xmlns="http://www.w3.org/2000/svg" viewBox="-505 380 12 10">
                            <path d="M-503 388h8v1h-8zM-494 382.2c-.4 0-.8.3-.8.8 0 .1 0 .2.1.3l-2.3.7-1.5-2.2c.3-.2.5-.5.5-.8 0-.6-.4-1-1-1s-1 .4-1 1c0 .3.2.6.5.8l-1.5 2.2-2.3-.8c0-.1.1-.2.1-.3 0-.4-.3-.8-.8-.8s-.8.4-.8.8.3.8.8.8h.2l.8 3.3h8l.8-3.3h.2c.4 0 .8-.3.8-.8 0-.4-.4-.7-.8-.7z"></path>
                        </svg>
                        <?php esc_html_e('Pro', 'web-to-print-online-designer'); ?>
                    </span>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php
    return '';
}

add_filter('add_color','tppl_add_field_template',10,2);
function tppl_add_field_template($_temp,$temp){
 $path_config    = NBDESIGNER_CUSTOMER_DIR . '/' . $temp['folder'] . '/config.json';
 $listConfig     = Nbdesigner_IO::read_json_setting($path_config);
 $content = $listConfig->pro;
 $price = $listConfig->price;
 $proDesign = $listConfig->proDesign;
 $priceDesign = $listConfig->priceDesign;
 $_temp['pro'] = $content;
 $_temp['price'] = $price;
 $_temp['proDesign'] = $proDesign;
 $_temp['priceDesign'] = $priceDesign;
 return $_temp;
}


add_filter('nbd_add_seach_design','add_nbd_add_seach_design',10,4);
function add_nbd_add_seach_design($html,$designs,$total,$item_per_page){
    $abc = new Nbdesigner_Plugin();
    global $wpdb;
    $result         = array();
    $nbd_products   = get_transient( 'nbd_frontend_products' );
    if( false === $nbd_products ){
        $products = $abc->get_all_product_has_design();
        foreach ( $products as $pro ){
            $product    = wc_get_product( $pro->ID );
            $type       = $product->get_type();
            $image      = get_the_post_thumbnail_url( $pro->ID, 'post-thumbnail' );
            if( !$image ) $image = wc_placeholder_img_src();
            $result[] = array(
                'product_id'    => $pro->ID,
                'name'          => $pro->post_title,
                'src'           => $image,
                'type'          => $type,
                'url'           => get_permalink( $pro->ID )
            );
        }
        set_transient( 'nbd_frontend_products' , $result );
    }else{
        $result = $nbd_products;
    }
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REDIRECT_URL]";
    if(!isset($_GET['search2']) && !isset($_GET['product_id'])){
        $designs = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}nbdesigner_mydesigns" );
        setcookie("id_pro", "");
    }
    $select = $_GET['product_id'] ? $_GET['product_id'] : $_COOKIE["id_pro"];
    if(isset($_GET['product_id']) || count($select) > 0){
        $designs = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}nbdesigner_mydesigns WHERE product_id = {$select}" );
       setcookie("id_pro", $_GET['product_id']);
    }
    if(isset($_GET['search2']) && $_GET['search2'] != '' ){
        $designs = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}nbdesigner_mydesigns WHERE myname_designs LIKE '%{$_GET['search2']}%'" ); 
    }
    if((isset($_GET['search2']) && $_GET['search2'] != '' ) && (isset($_GET['product_id']) || count($select) > 0)){
        $designs = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}nbdesigner_mydesigns WHERE product_id = {$select} AND myname_designs LIKE '%{$_GET['search2']}%'" ); 
    }
    ?>
    <div class="container-design">
        <div class="seach-design" style="display: flex;">
            <span><?php echo(count($designs)); ?> designs found</span>
            <select onchange="if (this.value) window.location.href=this.value" style="width: 23%; margin-left: 10%;">
                <option value="<?php echo($actual_link)?>"><?php esc_html_e('All Design Type', 'web-to-print-online-designer'); ?></option>
                <?php foreach($result as $cat_index => $val): ?>
                <option value="<?php echo(add_query_arg(array('product_id' =>$val['product_id'])))?>" <?php selected( $val['product_id'], $select ); ?>><?php echo( $val['name'] ); ?></option>
                <?php endforeach; ?>
            </select>
            <form class="example" action style="margin:auto;max-width:300px">
                <input type="text" placeholder="Type here..." name="search2" value="<?php echo($_GET['search2']); ?>">
                <button type="submit" ><i class="fa fa-search"></i></button>
            </form>
        </div>
        <table class="shop_table shop_table_responsive my_account_orders seach_tabse">
            <thead>
                <tr>
                    <th><?php esc_html_e('Preview', 'web-to-print-online-designer'); ?></th>
                    <th><?php esc_html_e('Created', 'web-to-print-online-designer'); ?></th>
                    <th><?php esc_html_e('Action ', 'web-to-print-online-designer'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($designs as $design):
                    $pathDesign = NBDESIGNER_CUSTOMER_DIR . '/' . $design->folder .'/preview';
                    $listThumb = Nbdesigner_IO::get_list_images($pathDesign);
                    asort( $listThumb );
                    $link_edit_design = add_query_arg(array(
                            'product_id'        => $design->product_id,
                            'variation_id'      => $design->variation_id,
                            'task'              => 'edit',
                            'design_type'       => 'art',
                            'nbd_item_key'      => $design->folder,
                            'current_page'      => $current_page,
                            'rd'                => 'my_design'
                        ), getUrlPageNBD('create'));
                    $link_product = add_query_arg(array(
                        'nbds-ref'  => $design->folder
                    ), get_permalink($design->product_id));
                    $link_start_design = add_query_arg(array(
                        'product_id' => $design->product_id,
                        'reference'  => $design->folder
                    ), getUrlPageNBD('create'));
                ?>
                <tr class="order">
                    <td data-title="<?php esc_html_e('Preview', 'web-to-print-online-designer'); ?>">
                        <p><?php echo get_the_title( $design->product_id ); ?></p>
                        <?php foreach ( $listThumb as $image ): ?>
                        <img class="nbd-preview" src="<?php echo Nbdesigner_IO::convert_path_to_url($image); ?>" />
                        <?php endforeach; ?>
                    </td>
                    <!--<td data-title="<?php esc_html_e('Price', 'web-to-print-online-designer'); ?>"><?php echo wc_price($design->price) ?></td>-->
                    <td data-title="<?php esc_html_e('Date', 'web-to-print-online-designer'); ?>"><?php esc_html_e( $design->created_date ); ?></td>
                    <td data-title="<?php esc_html_e('Action ', 'web-to-print-online-designer'); ?>">
                        <!-- <a href="<?php echo wc_get_endpoint_url( 'view-design', $design->id, wc_get_page_permalink( 'myaccount' ) ); ?>"><?php esc_html_e('Edit', 'web-to-print-online-designer'); ?></a> -->
                        <a href="<?php echo esc_url( $link_edit_design ); ?>"><?php esc_html_e('Edit', 'web-to-print-online-designer'); ?></a><br />
                        <a href="javascript:void(0)" data-design="<?php esc_html_e( $design->id );  ?>" onclick="NBDESIGNERPRODUCT.delete_my_design( this )"><?php esc_html_e('Delete', 'web-to-print-online-designer'); ?></a><br />
                        <!--  <a href="javascript:void(0)" data-design="<?php esc_html_e( $design->id );  ?>" onclick="NBDESIGNERPRODUCT.add_design_to_cart( this )"><?php esc_html_e('Add to cart', 'web-to-print-online-designer'); ?></a>-->                   
                        <?php /* NBDesigner Advanced */ob_start(); ?>
                        <a href="<?php echo esc_url( $link_start_design ); ?>"><?php esc_html_e('Use this design', 'web-to-print-online-designer'); ?></a>
                        <?= apply_filters('button_use_design',ob_get_clean(),$design->folder,$design->product_id); ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php 
            $max_page = ceil( $total / $item_per_page );
            if( $max_page > 1 ): 
        ?>
        <div class="nbd-nav">
            <?php if ( 1 !== $current_page ) : ?>
            <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo wc_get_endpoint_url( 'my-designs', $current_page - 1, wc_get_page_permalink( 'myaccount' ) ); ?>" ><?php esc_html_e( 'Previous', 'web-to-print-online-designer' ); ?></a>
            <?php endif;?>
            <?php if( $max_page != $current_page ): ?>
            <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo wc_get_endpoint_url( 'my-designs', $current_page + 1, wc_get_page_permalink( 'myaccount' ) ); ?>"><?php esc_html_e( 'Next', 'web-to-print-online-designer' ); ?></a>
            <?php endif;?>
        </div>
        <?php endif;?>
    </div>
    <?php

}

add_filter('nbod_change_save_for_late','add_nbod_change_save_for_late');
function add_nbod_change_save_for_late(){
    ?>
        <div style="margin-bottom: 20px;"><span>Name design : <input type="Text" name="name_design"></span></div>
        <div class="new-design" ng-click="selectMyDesign('')">
            <i class="icon-nbd icon-nbd-add-black"></i>
            <p><?php esc_html_e('New design','web-to-print-online-designer'); ?></p>
        </div>
        <div class="nbd-user-design" ng-repeat="temp in resource.myTemplates" ng-click="selectMyDesign(temp.id)" title="<?php esc_html_e('Select','web-to-print-online-designer'); ?>">
            <div class="main-item">
                <div class="item-img">
                    <img ng-src="{{temp.src}}" alt="<?php esc_html_e('Template','web-to-print-online-designer'); ?>">
                </div>
            </div>
        </div>
    <?php
}

add_action('nbod_save_template_later','add_nbod_save_template_later',10,3);
function add_nbod_save_template_later($product_id,$design_folder,$POST){
    global $wpdb;
    if(isset($POST['name_design'])){
        $row = $wpdb->get_results(  "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = '{$wpdb->prefix}nbdesigner_mydesigns' AND column_name = 'myname_designs'"  );

        if(empty($row)){
           $wpdb->query("ALTER TABLE {$wpdb->prefix}nbdesigner_mydesigns ADD myname_designs varchar(255)");
        }
        $designs = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}nbdesigner_mydesigns WHERE folder = '{$design_folder}'" );
        foreach ($designs as $key => $value) {
           $wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}nbdesigner_mydesigns SET user_id = {$value->user_id},folder='{$value->folder}',product_id={$value->product_id},variation_id={$value->variation_id},price={$value->price},publish={$value->publish},created_date='{$value->created_date}',myname_designs = '{$POST['name_design']}' WHERE id = {$value->id} "));
        }
    }
}
add_action('nbd_js_object','somchai_js_object',30);
function somchai_js_object($object){
    $object['home_url'] = esc_url(home_url());
    $object['checkout_url'] = esc_url(wc_get_checkout_url());
    $object['somchai_js'] = true;
    return $object;
}
add_action( 'wp_enqueue_scripts',  'frontend_enqueue_scriptsss'  );
function frontend_enqueue_scriptsss(){
    ?>
    <script>var js_somchai_var = true;</script>
    <?php
    wp_enqueue_script( 'semantic', 'https://semantic-ui.com/dist/semantic.min.js', array('jquery'), NBDESIGNER_VERSION );
    wp_enqueue_style( 'semantic', 'https://semantic-ui.com/dist/semantic.min.css', array(), NBDESIGNER_VERSION );
    wp_enqueue_style( 'frontend_sc', SOMCHAI_URL . 'assets/css/frontend.css', array(), NBDESIGNER_VERSION );
    if(is_account_page()){
        wp_register_style( 'custom-nbdesigner', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css', array(), NBDESIGNER_VERSION );
        wp_enqueue_style( 'custom-nbdesigner' );
        wp_enqueue_script( 'cart_custom_nbdesigner','https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js', array('jquery'), NBDESIGNER_VERSION );
    }

}

function change_price( $cart ) { 
  $items = $cart->get_cart();
  foreach($items as $cart_item_key => $item){
    $nbd = isset($item['nbd_item_meta_ds']['nbd']) ? $item['nbd_item_meta_ds']['nbd'] : '';
    if($nbd){
      $path_config = NBDESIGNER_CUSTOMER_DIR . '/' . $nbd . '/config.json';
      $listConfig  = Nbdesigner_IO::read_json_setting($path_config);
      $priceTemplate = $listConfig->price;
      $pricePr = $item['data']->price;
      $priceArt = $listConfig->priceArt;
      if($priceArt > 0){
         $price = $priceTemplate + $pricePr + $priceArt;
      }
      else{
         $price = $priceTemplate + $pricePr;
      }
      $item['data']->set_price($price);
      $cart->set_session();
    }
  }
}
add_action('woocommerce_cart_loaded_from_session','change_price');

function addClipart(){
  ?>
    <td class="forminp-text">
        <input type="file" name="svg[]" id="addfile" value="" accept=".svg,image/*" multiple/><br />
        <div class="nbd-admin-font-tip"><?php esc_html_e('Allow extensions: svg, png, jpg, jpeg', 'web-to-print-online-designer'); ?>
            <br /><?php esc_html_e('Allow upload multiple files.', 'web-to-print-online-designer'); ?>
            <br /><?php esc_html_e('Note: if you export svg file from Illustrator, please choose Fonts type "Convert to outline"', 'web-to-print-online-designer'); ?></div>
    </td>
  <?php
  return '';
}
add_filter('nbod_add_file_clipart','addClipart');

function addPriceArt() {
  ?>
    <script>
        var filetoRead = document.getElementById("addfile");
        var imagesPreview = function(input) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    var html = '<div id="clipart"><img id="output" src="' + event.target.result + '" width="100" height="100">';
                    html += '<label>Price($)</label>'
                    html += '<input type="number" class="addprice" name="priceArt[]" value=""></div>';
                    filetoRead.insertAdjacentHTML('afterend', html);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
        };
        filetoRead.addEventListener("change",function(){
            imagesPreview(this);
        });

    </script>
  <?php 
}
add_action('nbd_update_code_key_word','addPriceArt');

function tppl_update_code_files($art,$key){
     $art['price'] = !empty($_POST['priceArt'][$key]) ? $_POST["priceArt"][$key] : 0;
  return $art;
}
add_filter('nbd_update_code_price','tppl_update_code_files',10,2);

function nbd_edit_html_art(){
  ?>
    <div class="clipart-wrap">
      <div class="clipart-item" ng-class="(art.price > 0) ? 'art_pro' : ''" nbd-drag="art.url" extenal="false" type="svg" art={{art}}  ng-repeat="art in resource.clipart.filteredArts | limitTo: resource.clipart.filter.currentPage * resource.clipart.filter.perPage" repeat-end="onEndRepeat('clipart')">
          <img ng-src="{{art.url}}" ng-click="addArt(art, true, true)" alt="{{art.name}}">
      </div>
    </div>
  <?php
  return '';
}
add_filter('adc_edit_customize_pro_clipart','nbd_edit_html_art');

// add_action('nbod_before_load_design','tppl_before_load_design');
function tppl_before_load_design(){
    if(!is_user_logged_in()){
        $link = get_permalink( get_option('woocommerce_myaccount_page_id') );

        wp_redirect( $link );
    }
}

add_filter('nbod_cutomize_update_image','nbod_cutomize_update_image_toolbar');
function nbod_cutomize_update_image_toolbar(){
    include 'views/image_filter.php';
}
add_shortcode('nbd_custom_dimesion','nbd_custom_dimesion');
function nbd_custom_dimesion($atts, $content = null){
    $product_id = (isset($atts['product_id'])) ? $atts['product_id'] : '';
    if(!$product_id){
        return '';
    }
    $link = add_query_arg( array(
        'product_id'    => $product_id,
        'view'          => 'm'
    ),  getUrlPageNBD( 'create' ) );
    ob_start();
    ?>
    <div class="button-yoursize">
        <div class="size-wrap">
        	<label for="upload-photo" style="cursor: pointer;background: #4cc180;padding: 6px 15px 10px;border-radius: 7px;color:black;">Edit you picture</label>
            <input type="file" style="opacity: 0;position: absolute;z-index:-1;visibility: hidden;" id="upload-photo" class="customedim" data-link="<?php echo esc_url($link); ?>"></input>
            <button class="customize" type="button"><span><?php esc_html_e('Custom Size' , 'web-to-print-online-designer') ?></span></button>
            <div class="custom-dimension">
                <div class="dimension-content">
                    <div class="dimension-input">
                        <label for="nbd-width"><?php esc_html_e('Width','web-to-print-online-designer') ?></label>
                        <input id="nbd-width" type="text">
                    </div>
                    <div class="dimension-input">
                        <label for="nbd-height"><?php esc_html_e('Height','web-to-print-online-designer') ?></label>
                        <input id="nbd-height" type="text">
                    </div>
                    <div class="dimension-unit">
                        <div class="ui selection dropdown">
                            <input type="hidden" name="unit" value="cm">
                            <i class="dropdown icon"></i>
                            <div class="default text">Cm</div>
                            <div class="menu">
                                <div class="item" data-value="cm">Cm</div>
                                <div class="item" data-value="in">In</div>
                                <div class="item" data-value="mm">Mm</div>
                                <div class="item" data-value="ft">Ft</div>
                                <div class="item" data-value="px">Px</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="create-design">
                    <button class="btn-create btn-disabled" data-link="<?php echo esc_url($link); ?>" id="create-dimension"><span><?php esc_html_e('Start Design' , 'web-to-print-online-designer') ?></span></button>
                </div>
            </div>
        </div>
        
    </div>
    <script type="text/javascript">
        jQuery('.ui.dropdown').dropdown();
    </script>
    <?php
    $html = ob_get_clean();
    return $html;
}
add_filter('nbod_data_product_export_pdfs_api','dlpn_data_product_export_pdfs_api');
add_filter('nbod_product_data_export_pdfs_lib','dlpn_data_product_export_pdfs_api');
function dlpn_data_product_export_pdfs_api($datas){

    if(isset($_POST['flag_download'])){
        $flags = $_POST['flag_download'];
        $data = array();
                
        foreach ($datas as $key => $side) {
            if(!$flags[$key]){
                unset($datas[$key]);
            }
        }
    }
    return $datas;
}

function update_primium_user(){
    $users = get_users( array( 'fields' => array( 'ID' ) ) );
    // update_user_meta($user_id , 'date_prenium_user_free' , date("Y-m-d H:i:s"));
    // update_user_meta($user_id , 'check_prenium_user_free' , true);
    foreach($users as $user){
        $date_prenium_user_free = get_user_meta($user->id , 'date_prenium_user_free',true) ? get_user_meta($user->id , 'date_prenium_user_free',true) : '';
        if($date_prenium_user_free){
            $today_date      = new DateTime( date( 'Y-m-d', strtotime( 'today' ) ) );
            $registered = new DateTime( date( 'Y-m-d', strtotime( $date_prenium_user_free ) ) );
            $interval_date   = $today_date->diff( $registered );
            if($interval_date->days === 31 ){
                $check_prenium_user_free = get_post_meta($user->id,'check_prenium_user_free',true) ? get_post_meta($user->id,'check_prenium_user_free',true) : '';
                $check_prenium_user_monthly = get_post_meta($user->id,'check_prenium_user_monthly',true) ? get_post_meta($user->id,'check_prenium_user_monthly',true) : '';
                if($check_prenium_user_free){
                    update_user_meta($user->id , 'check_prenium_user_free' , false);
                }elseif($check_prenium_user_monthly){
                    update_user_meta($user->id , 'is_prenium' ,0);
                    update_user_meta($user_id , 'check_prenium_user_monthly' , false);
                }
            }
            if($interval_date->days === 367 ){
                $check_prenium_user_annual = get_post_meta($user->id,'check_prenium_user_annual',true) ? get_post_meta($user->id,'check_prenium_user_annual',true) : '';
                if($check_prenium_user_annual){
                    update_user_meta($user->id , 'is_prenium' ,0);
                    update_user_meta($user->id , 'check_prenium_user_annual' , false);
                }
            }
        }
    }
}
update_primium_user();

add_shortcode('user_pricing','somchai_user_pricings');
function somchai_user_pricings($atts, $content = null){
    global $woocommerce;
    $all_ids = get_posts( array(
        'post_type' => 'product',
        'numberposts' => -1,
        'post_status' => 'Privately Published',
        'fields' => 'ids',
    ) );
    $id_product_month = 0;
    $id_product_annual = 0;
    $id_product_free = 0;
    foreach ( $all_ids as $id ) {
        $check_product_month = get_post_meta($id, '_prenium_user_monthly', true) ? get_post_meta($id, '_prenium_user_monthly', true) : 0; 
        $check_product_annual = get_post_meta($id, '_prenium_user_annual', true) ? get_post_meta($id, '_prenium_user_annual', true) : 0;
        $check_product_free = get_post_meta($id, '_prenium_user_free', true) ? get_post_meta($id, '_prenium_user_free', true) : 0;
        if($check_product_month === 'yes' ){
            $id_product_month = $id;
        }elseif($check_product_annual === 'yes'){
            $id_product_annual = $id;
        }elseif($check_product_free === 'yes'){
            $id_product_free = $id;
        }
        
    }

    $product_month =  new WC_Product($id_product_month);
    $product_annual =  new WC_Product($id_product_annual);
    $product_free =  new WC_Product($id_product_free);
    // echo "<pre>";
    // print_r($product_annual->get_price_html());
    // echo "</pre>";
    // die();
    ob_start();
    ?>
    <style type="text/css">
        #content.site-content{
            background-color: #fff;
        }
        #flexSwitchCheckChecked{
            float: unset;
            width: 3em;
            height: 2em;
            margin: 0px 10px 10px 10px;
        }
    </style>
    <script type="text/javascript">
        jQuery(document).on('change','#flexSwitchCheckChecked',function(){
            if (!document.getElementById('flexSwitchCheckChecked').checked) {
                jQuery('.change_price span').remove();
                jQuery('.change_price').append('<?= $product_month->get_price_html(); ?>');
                jQuery('#become-pro').attr('data-product','<?= $id_product_month; ?>');
                jQuery('#become-pro').html('Prenium user month');
            } else {
                jQuery('.change_price span').remove();
                jQuery('.change_price').append('<?= $product_annual->get_price_html();?>');
                jQuery('#become-pro').attr('data-product','<?= $id_product_annual; ?>');
                jQuery('#become-pro').html('Prenium user annual');
            }
        });
    </script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <h1 class="page-title"><?php esc_html_e('Thirdesign Pricing', 'web-to-print-online-designer'); ?></h1>
    <div class="price-desc">
        <p>
            <?php esc_html_e('Do more with Thirdsdesign Pro. Plus, you get a 30-day free trial.', 'web-to-print-online-designer'); ?>
        </p>
    </div>
    <div class="pricing-table-wrap">
        <table class="pricing-table">
            <thead>
                <tr>
                    <th>
                        <?php esc_html_e('Free', 'web-to-print-online-designer'); ?>
                        <p>Everything you need to start designing</p> 
                    </th>
                    <th>
                        <div class="pro-head">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.75 17.5h-9a.75.75 0 0 0 0 1.5h9a.75.75 0 0 0 0-1.5Z" fill="currentColor"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M12.98 6.81a1.5 1.5 0 1 0-1.46.001c-.762 2.086-1.588 3.109-2.353 3.194-.862.095-1.613-.28-2.32-1.21a.807.807 0 0 0-.145-.147 1.5 1.5 0 1 0-1.204.602c0 .058.009.12.024.182l1.216 4.864A2.25 2.25 0 0 0 8.921 16h6.658a2.25 2.25 0 0 0 2.183-1.704l1.216-4.864a.81.81 0 0 0 .024-.182 1.5 1.5 0 1 0-1.203-.602.806.806 0 0 0-.136.135c-.743.935-1.5 1.314-2.33 1.222-.73-.082-1.56-1.108-2.353-3.194Z" fill="currentColor"></path></svg>
                            <span><?php esc_html_e('Pro', 'web-to-print-online-designer'); ?></span>
                        </div>
                        
                        <p>Designing together is easier. Professional design made easy with unlimited access to premium content and tools.</p> 
                    </th>
                </tr>
                
            </thead>
            <tbody>
                <tr>
                    <td class="price-of-user"><?php echo wc_price(0); ?></td>
                    <td class="price-of-user change_price"><?php echo $product_month->get_price_html(); ?></td>
                </tr>
                <tr>
                    <td>
                        <a class="button" href="<?php echo home_url(); ?>"><span><?php esc_html_e('Start', 'web-to-print-online-designer'); ?></span></a>
                    </td>
                    <td>
                        <div class="form-check form-switch">
                          <label  class= "check_label" for="flexSwitchCheckChecked">Monthly payment</label>
                          <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                          <label  class= "check_label" for="flexSwitchCheckChecked">Annual payment</label>
                        </div>
                        <?php 
                            $check_free_user = get_post_meta(get_current_user_id(),'_free_user_30_days',true) ? get_post_meta(get_current_user_id(),'_free_user_30_days',true) : '';
                            if($check_free_user){
                        ?>
                        <a data-product="<?php echo $id_product_month; ?>" class="button" id="become-pro" href=""><span><?php esc_html_e('Prenium user month', 'web-to-print-online-designer'); ?></span></a>
                        <?php 
                        }else{
                            $product_id = $id_product_free;
                        ?>
                        <a data-product="<?php echo $product_id; ?>" class="button" id="become-pro" href=""><span><?php esc_html_e('Prenium user free 30 days', 'web-to-print-online-designer'); ?></span></a>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="feature">
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Login to design</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Use the free templates, images to design.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Download your design for free.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Tens of thousands of free photos and graphics.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Limited Cliparts.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Cloud Storage 200MB.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Image Upload Space 5MB.</span></div>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <ul class="feature">
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Unlimited designs.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Free to use all the pro templates, images.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div style="display:flex;">
                                    <span class="feature-text">Background removal.</span>
                                    <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                                </div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div style="display:flex;">
                                     <span class="feature-text">Unlimited design sizes "Instant resizing".</span>
                                     <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                                     
                                </div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Over 100million premium stock photos,other.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div style="display:flex;">
                                    <span class="feature-text">Unlimited cliparts.</span>
                                    <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                                </div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div style="display:flex;">
                                    <span class="feature-text">Premium Icons.</span>
                                    <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                                </div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Using the gradation function.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Making an image into a cartoon.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div style="display:flex;">
                                    <span class="feature-text">Download in Transparent PNG.</span>
                                    <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                                </div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div style="display:flex;">
                                    <span class="feature-text">Download in SVG file.</span>
                                    <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                                </div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div style="display:flex;">
                                    <span class="feature-text">Can to upload Ai, EPS file.</span>
                                    <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
                                </div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Cloud Storage 100GB.</span></div>
                            </li>
                            <li class="item-feature">
                                <span class="featue-check">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                    </span>
                                </span>
                                <div><span class="feature-text">Image Upload Space 1GB.</span></div>
                            </li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php
    $html = ob_get_clean();
    return $html;
}
add_filter('nbod_html_search_field_templates', 'ctdsSearch');
function ctdsSearch()
{
?>
    <input id="nbd-search-template" ng-model="resource.templateSearch.value" placeholder="<?php esc_attr_e('Seach templates', 'web-to-print-online-designer'); ?>" ng-mousedown="resource.templateSearch.focus = true;" ng-keyup="prepareSearchTemplate($event)" />
    <svg id="nbd_show_filter" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
        <path fill="currentColor" fill-rule="evenodd" d="M6.1 17.25a3 3 0 0 1 5.8 0h8.85a.75.75 0 1 1 0 1.5h-8.84a3 3 0 0 1-5.82 0H3.25a.75.75 0 1 1 0-1.5h2.84zm6-6a3 3 0 0 1 5.8 0h2.85a.75.75 0 1 1 0 1.5h-2.84a3 3 0 0 1-5.82 0H3.25a.75.75 0 1 1 0-1.5h8.84zm-6-6a3 3 0 0 1 5.8 0h8.85a.75.75 0 1 1 0 1.5h-8.84a3 3 0 0 1-5.82 0H3.25a.75.75 0 0 1 0-1.5h2.84zM9 7.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm6 6a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm-6 6a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"></path>
    </svg>
    <i ng-click="searchTemplate()" class="icon-nbd icon-nbd-fomat-search" ng-class="{'icon-nbd-fomat-search': !resource.templateSearch.loading, 'icon-nbd-refresh': resource.templateSearch.loading, 'nbd-template-searching': resource.templateSearch.loading}"></i>
<?php
    return '';
}
add_action('nbd_filter_template', 'after_filter_templates');
function after_filter_templates()
{
    $nbd_filter = $_GET['nbd_filter'];
?>
    <div class="nbd-category nbd-sidebar-con nbd-sidebar-filter">
        <svg id="nbd_show_filter_in_tem" width="30" height="30" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" version="1.1" title="Filter">
            <path stroke=" none" id="svg_1" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m27.85713,24.59945l-9.83264,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706s0.41112,-0.89706 0.91837,-0.89706l9.83264,0c0.50724,0 0.91837,0.40158 0.91837,0.89706s-0.41112,0.89706 -0.91837,0.89706z" />
            <path stroke=" none" id="svg_2" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m17.79,7.19497l-15.64713,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706s0.41112,-0.89706 0.91837,-0.89706l15.64713,0c0.50724,0 0.91837,0.40158 0.91837,0.89706s-0.41112,0.89706 -0.91837,0.89706z" />
            <path stroke=" none" id="svg_3" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m27.85713,15.89706l-17.11315,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706c0,-0.49548 0.41112,-0.89706 0.91837,-0.89706l17.11315,0c0.50724,0 0.91837,0.40158 0.91837,0.89706c0,0.49548 -0.41112,0.89706 -0.91837,0.89706z" />
            <path stroke=" none" id="svg_4" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m12.2097,24.59945l-10.06683,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706s0.41112,-0.89706 0.91837,-0.89706l10.06683,0c0.50724,0 0.91837,0.40158 0.91837,0.89706s-0.41112,0.89706 -0.91837,0.89706z" />
            <path stroke=" none" id="svg_5" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m5.16368,15.89706l-3.02081,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706c0,-0.49548 0.41112,-0.89706 0.91837,-0.89706l3.02081,0c0.50724,0 0.91837,0.40158 0.91837,0.89706c0,0.49548 -0.41143,0.89706 -0.91837,0.89706z" />
            <path stroke=" none" id="svg_6" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m27.85713,7.19497l-4.43234,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706s0.41112,-0.89706 0.91837,-0.89706l4.43234,0c0.50724,0 0.91837,0.40158 0.91837,0.89706s-0.41112,0.89706 -0.91837,0.89706z" />
            <path stroke=" none" id="svg_7" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m17.79,27.32472l-5.5803,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706l0,-5.45114c0,-0.49548 0.41112,-0.89706 0.91837,-0.89706l5.5803,0c0.50724,0 0.91837,0.40158 0.91837,0.89706l0,5.45084c0,0.49578 -0.41112,0.89736 -0.91837,0.89736zm-4.66193,-1.79412l3.74357,0l0,-3.65702l-3.74357,0l0,3.65702z" />
            <path stroke=" none" id="svg_8" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m10.74398,18.62233l-5.5803,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706l0,-5.45084c0,-0.49548 0.41112,-0.89706 0.91837,-0.89706l5.5803,0c0.50724,0 0.91837,0.40158 0.91837,0.89706l0,5.45084c0,0.49548 -0.41112,0.89706 -0.91837,0.89706zm-4.66193,-1.79412l3.74357,0l0,-3.65672l-3.74357,0l0,3.65672z" />
            <path stroke=" none" id="svg_9" fill-rule=" nonzero" fill=" rgb(0,0,0)" stroke-miterlimit="10" stroke-linejoin=" miter" stroke-dasharray=" none" stroke-linecap=" butt" d="m23.37061,9.92054l-5.58061,0c-0.50724,0 -0.91837,-0.40158 -0.91837,-0.89706l0,-5.45114c0,-0.49548 0.41112,-0.89706 0.91837,-0.89706l5.5803,0c0.50724,0 0.91837,0.40158 0.91837,0.89706l0,5.45084c0.00031,0.49548 -0.41082,0.89736 -0.91806,0.89736zm-4.66224,-1.79412l3.74357,0l0,-3.65702l-3.74357,0l0,3.65702z" />
        </svg>
        <section class="nbd_show-filter">
            <div class="nbd_filter">
                <form>
                    <div class="nbd_filter">
                        <div class="nbd_checkbox">
                            <input type="radio" name="nbd_filter" <?= $nbd_filter == 'free' ? 'checked=checked' : '' ?>
                                value="free" id="nbd_get_free">
                            <label for="nbd_get_free" class="nbd_free">Free</label>
                        </div>
                        <div class="nbd_checkbox">
                            <input type="radio" name="nbd_filter" <?= $nbd_filter == 'pro' ? 'checked=checked' : '' ?>
                                value="pro" id="nbd_get_pro">
                            <label for="nbd_get_pro" class="nbd_pro">Pro</label>
                            <img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/crown-4704.png"; ?>"
                                alt="" width="17px">
                        </div>
                        <div class="nbd_checkbox">
                            <input type="radio" name="nbd_filter" <?= $nbd_filter == 'designer' ? 'checked=checked' : '' ?>
                                value="designer" id="nbd_get_designer">
                            <label for="nbd_get_designer" class="nbd_designer">Designer</label>
                        </div>
                        <?php
                            if (isset($_GET['nbd_filter'])) {
                            ?>
                        <div class="nbd-checkbox">
                            <a href="
                            <?php
                                global $wp;
                                echo home_url($wp->request);
                            ?>" title="Clear Filter">X
                            </a>
                        </div>
                        <?php
                            }
                            ?>
                    </div>
                    <button class="nbd-btn-filter" type="submit" name="nbd_btn_filter">Apply Filter</button>
                </form>
            </div>
        </section>

    </div>
    <?php
}
if (isset($_GET['nbd_filter'])) {
    add_filter('nbod_template_product_category', 'list_template_filter');
}

function filter_templates($filter, $attr, $pro)
{
    if (isset($_GET['nbd_filter']) && ($_GET['nbd_filter'] == $filter)) {
        global $wpdb;
        $templates = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}nbdesigner_templates");
        $UrlPageNBD = getUrlPageNBD('create');
        foreach ($templates as $key => $temp) :
            $UrlPageNBD = apply_filters('nbod_variable_UrlPageNBD', $UrlPageNBD, $temp->product_id);
            $link_template  = add_query_arg(array(
                'product_id'    => $temp->product_id,
                'variation_id'  => $temp->variation_id,
                'reference'     => $temp->folder
            ), $UrlPageNBD);
            $new_DE = new My_Design_Endpoint();
            $favourite_templates = $new_DE->get_favourite_templates();
            $url_uploads = NBDESIGNER_DATA_DIR . '/designs/';
            $url_image = NBDESIGNER_DATA_URL . '/designs/';
            $image = $url_image . $temp->folder . "/preview/frame_0.png";
            $tem_path_json = ($url_uploads . $temp->folder . '/config.json');
            $config = json_decode(file_get_contents($tem_path_json));
            $current_user_id = wp_get_current_user()->id;
            $template = $config->$attr;
            if ($attr != '') {
                $template = $config->$attr;
            }
            if ($attr == '') {
                $template = 0;
                if ($config->proDesign) {
                    $template = 1;
                }
                if ($config->pro) {
                    $template = 1;
                }
            }
            if ($_GET['nbd_filter'] == $filter) :
                if ($template == $pro) :
            ?>
                <div class="nbdesigner-item in-view" data-id="<?php echo ($temp->product_id); ?>" data-img="<?php echo ($image); ?>"
                    data-title="<?php echo ($temp->name); ?>">
                    <div class="nbd-gallery-item">
                        <div class="nbd-gallery-item-inner">
                            <a href="<?php echo esc_url($link_template); ?>"
                                onclick="previewTempalte(event, <?php echo ($temp->product_id); ?>)">
                                <img src="<?php echo esc_url($image); ?>" class="nbdesigner-img" />
                            </a>
                            <img class="template-pro-icon" src="<?php echo plugin_dir_url(__FILE__) . '/assets/images/crown.png' ?>"
                                alt="" width="50">
                            <div class="nbd-gallery-item-more-acction-wrap">
                                <div class="nbd-gallery-item-more-acction preview"
                                    onclick="previewTempalte(event, <?php echo ($temp->product_id); ?>)">
                                    <a href="#"><?php esc_html_e('Preview', 'web-to-print-online-designer'); ?></a>
                                </div>
                                <?php if ($temp->type != 'solid') : ?>
                                <div class="nbd-gallery-item-more-acction customize">
                                    <a
                                        href="<?php echo esc_url($link_template); ?>"><?php esc_html_e('Customize', 'web-to-print-online-designer'); ?></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                
                        <div class="nbd-gallery-item-acction">
                            <span class="nbd-gallery-item-name"><?php esc_html_e($temp->name); ?></span>
                            <div class="nbd-like-icons">
                                <span class="nbd-like-icon like <?php if (in_array($temp->id, $favourite_templates)) echo 'active'; ?>"
                                    onclick="updateFavouriteTemplate(this, 'unlike', <?php echo ($temp->id); ?>)">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                        <title>Voted</title>
                                        <path fill="#db133b"
                                            d="M17.19 4.155c-1.672-1.534-4.383-1.534-6.055 0l-1.135 1.042-1.136-1.042c-1.672-1.534-4.382-1.534-6.054 0-1.881 1.727-1.881 4.52 0 6.246l7.19 6.599 7.19-6.599c1.88-1.726 1.88-4.52 0-6.246z">
                                        </path>
                                    </svg>
                                </span>
                                <span
                                    class="nbd-like-icon unlike <?php if (!in_array($temp->id, $favourite_templates)) echo 'active'; ?>"
                                    onclick="updateFavouriteTemplate(this, 'like', <?php echo ($temp->id); ?>)">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                        <title>Vote</title>
                                        <path fill="#db133b"
                                            d="M17.19 4.156c-1.672-1.535-4.383-1.535-6.055 0l-1.135 1.041-1.136-1.041c-1.672-1.535-4.382-1.535-6.054 0-1.881 1.726-1.881 4.519 0 6.245l7.19 6.599 7.19-6.599c1.88-1.726 1.88-4.52 0-6.245zM16.124 9.375l-6.124 5.715-6.125-5.715c-0.617-0.567-0.856-1.307-0.856-2.094s0.138-1.433 0.756-1.999c0.545-0.501 1.278-0.777 2.063-0.777s1.517 0.476 2.062 0.978l2.1 1.825 2.099-1.826c0.546-0.502 1.278-0.978 2.063-0.978s1.518 0.276 2.063 0.777c0.618 0.566 0.755 1.212 0.755 1.999s-0.238 1.528-0.856 2.095z">
                                        </path>
                                    </svg>
                                </span>
                                <span class="nbd-like-icon loading">
                                    <img src="<?php echo NBDESIGNER_PLUGIN_URL . 'assets/images/loading.gif' ?>" />
                                </span>
                            </div>
                        </div>
                        <?php
                            if ($temp->user_id == $current_user_id) :
                                if ($temp->type == 'solid') {
                                    $link_edit_design  = add_query_arg(array(
                                        'tab'   => 'designs',
                                        'edit'  => $temp->id
                                    ), wc_get_endpoint_url('my-store', '', wc_get_page_permalink('myaccount')));
                                } else {
                                    $link_edit_design = add_query_arg(array(
                                        'id' => $temp->user_id,
                                        'template_id' => $temp->id
                                    ), getUrlPageNBD('designer'));
                                }
                            ?>
                        <a class="nbd-edit-template" href="<?php echo esc_url($link_edit_design); ?>"
                            title="<?php esc_html_e('Edit template', 'web-to-print-online-designer'); ?>">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="12" height="14" viewBox="0 0 12 14">
                                <title>Edit template</title>
                                <path fill="#757575"
                                    d="M2.836 12l0.711-0.711-1.836-1.836-0.711 0.711v0.836h1v1h0.836zM6.922 4.75c0-0.102-0.070-0.172-0.172-0.172-0.047 0-0.094 0.016-0.133 0.055l-4.234 4.234c-0.039 0.039-0.055 0.086-0.055 0.133 0 0.102 0.070 0.172 0.172 0.172 0.047 0 0.094-0.016 0.133-0.055l4.234-4.234c0.039-0.039 0.055-0.086 0.055-0.133zM6.5 3.25l3.25 3.25-6.5 6.5h-3.25v-3.25zM11.836 4c0 0.266-0.109 0.523-0.289 0.703l-1.297 1.297-3.25-3.25 1.297-1.289c0.18-0.187 0.438-0.297 0.703-0.297s0.523 0.109 0.711 0.297l1.836 1.828c0.18 0.187 0.289 0.445 0.289 0.711z">
                                </path>
                            </svg>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php
                endif;
            endif;
        endforeach;
    }
}

function list_template_filter()
{
    if (isset($_GET['nbd_filter'])) {
        $filter_templates = $_GET['nbd_filter'];
        switch ($filter_templates) {
            case "designer":
                filter_templates('designer', 'proDesign', 1);
                break;
            case "pro";
                filter_templates('pro', 'pro', 1);
                break;
            case "free":
                filter_templates('free', '', 0);
                break;
        }
    }
}
add_action('nbd_editor_extra_preview_section','somchai_editor_extra_section');
function somchai_editor_extra_section($ui_mode){
    ?>
    <div class="preview-ebook-wrap">
        <div class="preview-ebook-wrap-inner">
            <div class="preview-ebook-wrap-loading">
                <div class="loader">
                    <svg class="circular" viewBox="25 25 50 50">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                    </svg>
                </div>
            </div>
            <i class="icon-nbd icon-nbd-clear close-preview-ebook-wrap" ng-click="closeEbookPreview()"></i>
        </div>
    </div>
    <?php
}
add_action( 'template_redirect', 'somchai_editor_html' );
function somchai_editor_html(){
    if( isset( $_GET['nbd-route'] ) && $_REQUEST['nbd-route'] == 'nbd-ebook-preview' ){
        $path = SOMCHAI_DIR . 'views/ebook.php';
        include( $path ); exit();
        return;
    }
}
add_action('wp_ajax_somchai_become_pro','somchai_become_pro');
function somchai_become_pro(){
    $product_id = $_POST['product_id'];
    global $woocommerce;
    $woocommerce->cart->empty_cart();
    $woocommerce->cart->add_to_cart($product_id,1);
}
add_filter( 'product_type_options', 'somchai_admin_toggle_option' , 10, 1  );
add_action('woocommerce_process_product_meta_variable','somchai_woocommerce_process_product_meta_variable', 10, 1);
add_action('woocommerce_process_product_meta_simple', 'somchai_woocommerce_process_product_meta_variable', 10, 1);
function somchai_admin_toggle_option(){
    $options['_prenium_user_annual'] = array(
        'id'            => '_prenium_user_annual',
        'wrapper_class' => 'show_if_simple show_if_variable',
        'label'         => __( 'Prenium User Annual', 'WooCommerce' ),
        'description'   => __( '', 'woocommerce' ),
        'default'       => 'no',
    );
    $options['prenium_user_monthly'] = array(
        'id'            => '_prenium_user_monthly',
        'wrapper_class' => 'show_if_simple show_if_variable',
        'label'         => __( 'Prenium User Monthly', 'woocommerce' ),
        'description'   => __( '', 'woocommerce' ),
        'default'       => 'no',
    );
    $options['prenium_user_free'] = array(
        'id'            => '_prenium_user_free',
        'wrapper_class' => 'show_if_simple show_if_variable',
        'label'         => __( 'Prenium User Free', 'woocommerce' ),
        'description'   => __( '', 'woocommerce' ),
        'default'       => 'no',
    );
    return $options;
}
function somchai_woocommerce_process_product_meta_variable($post_id){
    $prenium_user_annual = isset( $_POST['_prenium_user_annual'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, '_prenium_user_annual', $prenium_user_annual);
    $prenium_user_monthly = isset( $_POST['_prenium_user_monthly'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, '_prenium_user_monthly', $prenium_user_monthly);
    $prenium_user_free = isset( $_POST['_prenium_user_free'] ) ? 'yes' : 'no';
    update_post_meta( $post_id, '_prenium_user_free', $prenium_user_free);

}
add_action( 'woocommerce_order_edit_status', 'somchai_order_status_completed', 20, 2 );
function somchai_order_status_completed($order_id, $status){
    if($status == 'completed'){
        $order = wc_get_order($order_id);
        foreach ( $order->get_items() as $item_id => $item ) {
            $product_id = $item->get_product_id();
            $prenium_user_annual = get_post_meta($product_id, '_prenium_user_nnual', true);
            $prenium_user_monthly = get_post_meta($product_id, '_prenium_user_monthly', true);
            $prenium_user_free = get_post_meta($product_id, '_prenium_user_free', true);
            if($prenium_user_annual === 'yes' || $prenium_user_monthly === 'yes' || $prenium_user_free === 'yes'){
                $user_id = $order->get_user_id();
                update_user_meta($user_id , 'is_prenium' , 1);
                if($prenium_user_annual === 'yes'){
                    update_user_meta($user_id , 'date_prenium_user_annual' , date("Y-m-d H:i:s"));
                    update_user_meta($user_id , 'check_prenium_user_annual' , true);
                    update_user_meta($user_id , 'check_prenium_user_monthly' , false);
                    update_user_meta($user_id , 'check_prenium_user_free' , false);
                }elseif($prenium_user_monthly === 'yes'){
                    update_user_meta($user_id , 'date_prenium_user_monthly' , date("Y-m-d H:i:s"));
                    update_user_meta($user_id , 'check_prenium_user_monthly' , true);
                }elseif($prenium_user_free === 'yes'){
                    update_user_meta($user_id , 'date_prenium_user_free' , date("Y-m-d H:i:s"));
                    update_user_meta($user_id , 'check_prenium_user_free' , true);
                    update_post_meta($user_id,'_free_user_30_days',true);
                }
            }
        }
    }
}
function check_is_prenium_user(){
    if(!is_user_logged_in()) return false;
    $pro = get_user_meta(get_current_user_id() , 'is_prenium',true);
    if($pro){
        return true;
    }
    else{
        return false;
    }
}
define('SOMCHAI_URL', plugin_dir_url( __FILE__ ));
define('SOMCHAI_DIR', plugin_dir_path( __FILE__ ) );


add_filter('nbd_add_checkbox_upload','add_nbd_add_checkbox_upload');
function add_nbd_add_checkbox_upload($html){
    ?>
        <div class="nbd-term" ng-if="settings['nbdesigner_upload_show_term'] == 'yes'">
            <div class="nbd-checkbox">
                <input id="accept-term" type="checkbox">
                <label for="accept-term">&nbsp;</label>
            </div>
            <span class="term-read"><?php esc_html_e('I accept the terms','web-to-print-online-designer'); ?></span>
        </div>
        <div class="nbd-term" >
            <div class="nbd-checkbox">
                <input id="accept-cartoon" type="checkbox">
                <label for="accept-cartoon">&nbsp;</label>
            </div>
            <span class="term-read"><?php esc_html_e('Convert to cartoon artwork','web-to-print-online-designer'); ?></span>
        </div>
        <style>
            .lable_cartoon{
                display : none;
            }
            .images-cartoon{
                display: flex;
                flex-wrap: wrap;
            }
            .images-cartoon .imae{
                width: 33%;
                padding: 2px;
            }
            .images-cartoon .imae img{ 
                position: relative;

            }
            .images-cartoon .imae input{ 
                position: absolute;
                z-index : 99;
                margin: 27% 0% 0% -5%;
            }
        </style>
        <div class="lable_cartoon">
            <label for="option-cartoon">Choose a cartoon :</label>
            <div class="images-cartoon" >
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/toonmojihd.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="toonmojihd">
                </div>
                <div  class = "imae">
                     <img src="<?php echo SOMCHAI_URL . 'images/caricaturehd.jpg'; ?>">
                     <input type="checkbox" class="vehicle2"  value="caricaturehd">
                    
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/comichd.jpg'; ?>">
                   <input type="checkbox" class="vehicle2"  value="comichd">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/zombify.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="zombify">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/toonifyplus.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="toonifyplus">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/emojify.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="emojify">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/toonify.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="toonify">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/halloweenify.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="halloweenify">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/caricature.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="caricature">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/toonifyhd.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="toonifyhd">
                </div>
                <div class = "imae">
                    <img src="<?php echo SOMCHAI_URL . 'images/comic.jpg'; ?>">
                    <input type="checkbox" class="vehicle2"  value="comic">
                </div>
            </div>
        
        </div>
    <?php

}
add_filter('nbod_nbdesigner_customer_upload_response','adds_nbod_nbdesigner_customer_upload_response',10,4);
function adds_nbod_nbdesigner_customer_upload_response($res,$path,$ext,$name){
      /*nbdesigner_advanced*/
        if(isset($_POST['checked']) && $_POST['checked']){
            $resizable_extensions = array( 'jpg', 'jpeg', 'png' );
            if( in_array( $ext, $resizable_extensions ) ){
                $url = 'https://toonify.p.rapidapi.com/v0/'.$_POST['url'];
                $ch = curl_init($url);
                $images = array(
                    'image' => new CURLFile($path['full_path'], 'image/'.$ext, 'image')
                );
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                   curl_setopt($ch, CURLOPT_POSTFIELDS, $images);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'x-rapidapi-key: edb2043d44mshf1aeae5ff30a6acp168a43jsne2f76b75a425',
                    'accept: image/jpeg',
                    'Content-Type: multipart/form-data',
                    'x-rapidapi-host: toonify.p.rapidapi.com'
                 ));
                $output   = curl_exec($ch);
                $decoded = json_decode($output, TRUE);
                if(!$decoded){
                    file_put_contents($path['full_path'],$output);
                }
                curl_close($ch);
            }
        }
        return $res;
}
