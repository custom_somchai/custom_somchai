<div class="toolbar-image" ng-show="stages[currentStage].states.isImage">
        <ul class="nbd-main-menu">
            <li class="menu-item menu-filter" ng-show="settings.enableImageFilter">
                <i class="icon-nbd icon-nbd-baseline-tune nbd-tooltip-hover" title="<?php esc_html_e('Filter','web-to-print-online-designer'); ?>"></i>
                <div class="sub-menu" data-pos="left">
                    <div class="image-filters" ng-if="settings.enableImageFilter && stages[currentStage].states.isImage">
                        <div class="image-filter ng-scope" ng-click="removeImageFilters()" title="<?php esc_html_e('Clear filters','web-to-print-online-designer'); ?>">
                            <img ng-src="{{settings.assets_url + 'images/filter/filter.png'}}" />
                        </div>
                        <div class="image-filter" ng-click="addImageFilter( filter )" ng-class="stages[currentStage].states.filters[filter] ? 'active' : ''" ng-repeat="filter in availableFilters" title="{{filter}}">
                            <img ng-src="{{settings.assets_url + 'images/filter/f_' + filter + '.png'}}"" />
                            <i class="icon-nbd icon-nbd-fomat-done" ></i>
                        </div>
                    </div>
                </div>
            </li>
            <li class="menu-item menu-crop" ng-click="initCrop()">
                <i class="icon-nbd icon-nbd-round-crop nbd-tooltip-hover" title="<?php esc_html_e('Crop','web-to-print-online-designer'); ?>"></i>
            </li>
            <li class="menu-item menu-crop" ng-show="!stages[currentStage].states.isMasked">
                <i class="nbd-tooltip-hover nbd-svg-icon" title="<?php esc_html_e('Create clipping mask','web-to-print-online-designer'); ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"/></svg>
                </i>
                <div class="sub-menu sub-menu-shape_mask" data-pos="left" >
                    <div class="shape_mask-wrapper">
                        <span class="shape_mask shape-type-{{n}}" ng-click="createClippingMask(n)" ng-repeat="n in [] | range:25"></span>
                    </div>
                    <div class="custom_shape_mask-wrapper">
                        <div><?php esc_html_e('Custom Mask','web-to-print-online-designer'); ?></div>
                        <textarea class="form-control hover-shadow nbdesigner_svg_code" rows="5" ng-change="getPathCommand()" ng-model="svgPath" placeholder="<?php esc_html_e('Enter svg code','web-to-print-online-designer'); ?>"/></textarea>
                        <button ng-class="pathCommand !='' ? '' : 'nbd-disabled'" class="nbd-button" ng-click="createClippingMask(-1)"><?php esc_html_e('Apply Mask','web-to-print-online-designer'); ?></button>
                    </div>
                </div>
            </li>
            <li class="menu-item menu-crop menu-crop-customize" style="width: 20px;margin: 0 7px;" ng-show="!stages[currentStage].states.isMasked" ng-click="updateScrollBarCustomize('#customize-filter-mask')">

            <img style="cursor: pointer;" src="<?php echo SOMCHAI_URL . 'assets/images/image-filter-h.png' ?>" alt="Image Filter" title="<?php esc_html_e('Image Filter','web-to-print-online-designer'); ?>">
                <div class="sub-menu sub-menu-shape_mask-customize" id="customize-filter-mask" data-pos="left" style="height: 500px;overflow: auto;">
                    <div class="customize-filter" style="padding: 4px;white-space: normal;">
                        <div class="button-radius" style="text-align: center;margin: 10px;">
                            <span ng-click="resetImage();"style="font-weight: bold;" style="cursor: pointer;">Click To Reset Image</span>
                        </div>
                        <ul id="image-fx-filter">
                            <li ng-repeat="(matrixIndex, matrix) in matrixFilter" ng-style="{'background-position' : matrix.position}" 
                            ng-click="matrixFilters($index,matrix.key,matrix.type)"
                            ng-class="matrix.check == 1 ? 'active': ''" title="{{matrix.name}}"><span>{{matrix.name}}</span>
                                <input id="{{matrix.name}}" type="checkbox" style = "display:none" class="{{matrix.type}} {{matrix.key}}" ng-checked="matrix.check == 1">
                            </li>
                        </ul>
                        <div class = "range-infor">
                            <table class = "table-customize-filter">
                                <tbody>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Brightness','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;" ng-change="setImageAttribute('brightness',attributeImage.brightness / 100)" ng-model="attributeImage.brightness" min="-100" max="100">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input  type="number" placeholder="--" class="range-track" ng-change="setImageAttribute('brightness',attributeImage.brightness / 100)" min="-100" max="100"  ng-model="attributeImage.brightness"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Saturation','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                        <div class="main-customize">
                                            <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageAttribute('saturation',attributeImage.saturation / 100)"  ng-model="attributeImage.saturation" min="0" max="100">
                                            <span class="range-track"></span>
                                        </div> 
                                        </td>
                                        <td>
                                        <div class="range-number">
                                            <input type="number" placeholder="--" class="range-track" ng-change="setImageAttribute('saturation',attributeImage.saturation / 100)" ng-model="attributeImage.saturation" min="0" max="100"></input>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Contrast','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;" ng-change="setImageAttribute('contrast',attributeImage.contrast / 100)" ng-model="attributeImage.contrast" min="-100" max="100">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                            <input  type="number" min="-100" max="100" placeholder="--" class="range-track" ng-change="setImageAttribute('contrast',attributeImage.contrast / 100)" ng-model="attributeImage.contrast"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Blur','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;" ng-change="setImageAttribute('blur',attributeImage.blur / 100)" ng-model="attributeImage.blur" min="0" max="100">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input type="number" min="0" max="100"  placeholder="--" class="range-track" ng-change="setImageAttribute('blur',attributeImage.blur / 100)" ng-model="attributeImage.blur"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Noise','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageAttribute('noise',attributeImage.noise)" ng-model="attributeImage.noise" min="0" max="1000" >
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input type="number" placeholder="--" class="range-track" ng-change="setImageAttribute('noise',attributeImage.noise)" ng-model="attributeImage.noise" min="0" max="1000"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Pixelate','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageAttribute('pixelate',attributeImage.pixelate)" ng-model="attributeImage.pixelate" min="2" max="20">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input type="number" placeholder="--" class="range-track" ng-change="setImageAttribute('pixelate',attributeImage.pixelate)" ng-model="attributeImage.pixelate" min="2" max="20"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Hue','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;" ng-change="setImageAttribute('hue',attributeImage.huerotation/100)" ng-model="attributeImage.huerotation" min="-2" max="200">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                            <input type="number" placeholder="--" class="range-track" ng-change="setImageAttribute('hue',attributeImage.huerotation/100)" ng-model="attributeImage.huerotation" min="-2" max="200"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Rotate','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setLayerAttribute('angle', stages[currentStage].states.angle)" ng-model="stages[currentStage].states.angle"  min="0" max="360" step="0.1">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input type="number" placeholder="--" class="range-track" ng-change="setLayerAttribute('angle', stages[currentStage].states.angle)" ng-model="stages[currentStage].states.angle"  min="0" max="360" step="0.1"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Opacity','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setTextAttribute('opacity', stages[currentStage].states.opacity / 100)" ng-model="stages[currentStage].states.opacity" step="1" min="0" max="100">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                            <input  type="number" placeholder="--" class="range-track" ng-change="setTextAttribute('opacity', stages[currentStage].states.opacity / 100)" ng-model="stages[currentStage].states.opacity" step="1" min="0" max="100"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize label-color-customzie">
                                                <label>
                                                    <?php esc_html_e('Remove Color','web-to-print-online-designer'); ?>
                                                        <!-- <div id="circle1" ng-style="{'background-color': currentColorCustomize}" class="color-palette-add-customize" ng-click="showFilterImageColorPicker()" title="<?php esc_html_e('Color','web-to-print-online-designer'); ?>"> -->
                                                        <input type="color" id="circle1" class="color-palette-add-customize" colorpick-eyedropper-active="true" title="<?php esc_html_e('Color','web-to-print-online-designer'); ?>" ng-model="currentColorCustomize">
                                                    </div>
                                                </label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <div class ="table-customize">
                                                    <div class = "table-row">
                                                        <div class ="color-picker-custom">
                                                            <div class = "table-cell">
                                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageAttribute('removecolor', attributeImage.removecolor)" ng-model="attributeImage.removecolor" step="0.01" min="0" max="1">
                                                                <span class="range-track"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input  type="number" placeholder="--" class="range-track" ng-change="setImageAttribute('removecolor', attributeImage.removecolor)" ng-model="attributeImage.removecolor" step="0.01" min="0" max="1"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Gamma','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="box-switch-customize">
                                                <div class="nbd-checkbox-group-customize">
                                                    <div class="mt-check-garden"> 
                                                        <input id="gamma-title" type="checkbox" ng-checked="checkGamma == 1" ng-click="setImageGammaClick('gamma', [attributeImage.r,attributeImage.g,attributeImage.b])" />
                                                        <label for="gamma-title"></label>  
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="colum-3-customize" ng-show ="checkGamma == 1">
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('R','web-to-print-online-designer'); ?></label>
                                            </div>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('G','web-to-print-online-designer'); ?></label>
                                            </div>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('B','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageGamma('r',[attributeImage.r,attributeImage.g,attributeImage.b])" ng-model="attributeImage.r" min="0.2" max="2.2" step="0.003921">
                                                <span class="range-track"></span>
                                            </div>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageGamma('g',[attributeImage.r,attributeImage.g,attributeImage.b])" ng-model="attributeImage.g" min="0.2" max="2.2" step="0.003921">
                                                <span class="range-track"></span>
                                            </div>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageGamma('b',[attributeImage.r,attributeImage.g,attributeImage.b])" ng-model="attributeImage.b" min="0.2" max="2.2" step="0.003921">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input  type="number" placeholder="--" class="range-track" ng-change="setImageGamma('r',[attributeImage.r,attributeImage.g,attributeImage.b])" ng-model="attributeImage.r" min="0.2" max="2.2" ></input>
                                            </div>
                                            <div class="range-number">
                                                <input  type="number" placeholder="--" class="range-track"  ng-change="setImageGamma('g',[attributeImage.r,attributeImage.g,attributeImage.b])" ng-model="attributeImage.g" min="0.2" max="2.2" ></input>
                                            </div>
                                            <div class="range-number">
                                                <input  type="number" placeholder="--" class="range-track" ng-change="setImageGamma('b',[attributeImage.r,attributeImage.g,attributeImage.b])" ng-model="attributeImage.b" min="0.2" max="2.2" ></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Blend Color','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="box-switch-customize">
                                                <div class="nbd-checkbox-group-customize">
                                                    <div class="mt-check-garden"> 
                                                        <input id="blend-color-title" type="checkbox" ng-checked="checkBlendColor == 1" ng-click="setImageBlendClick('blendcolor',attributeImage.color,attributeImage.mode,attributeImage.alpha)" />
                                                        <label for="blend-color-title"></label>  
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="colum-2-customize" ng-show ="checkBlendColor == 1">
                                        <th>
                                            <div class="label-customize label-color-customzie">
                                                <label>
                                                    <?php esc_html_e('Color','web-to-print-online-designer'); ?>
                                                    <input type="color" id="circle1" class="color-palette-add-customize" colorpick-eyedropper-active="true" title="<?php esc_html_e('Color','web-to-print-online-designer'); ?>" ng-model="currentColorCustomizeBlend">
                                                </label>
                                            </div>
                                        </th>
                                        <td colspan="2">
                                            <div class="main-customize">
                                                <div class="brush">
                                                    <button class="nbd-button nbd-dropdown nbd-dropdown-customize" ng-click="updateScrollBarCustomize('.tab-scroll-customize');updateScrollBar('#customize-filter-mask')">
                                                        {{nameMode ? nameMode : '<?php esc_html_e('Mode','web-to-print-online-designer'); ?>'}} 
                                                        <i class="icon-nbd icon-nbd-arrow-drop-down"></i>
                                                        <div class="nbd-sub-dropdown nbd-sub-dropdown-customize" data-pos="left">
                                                            <ul class="tab-scroll tab-scroll-customize">
                                                                <li ng-repeat="(modeImage, mode) in modeImage" ng-class="mode.name == nameMode ? 'active' : ''" 
                                                                ng-click="setBlendAttritube($index,mode.value,attributeImage.color,attributeImage.apha);updateScrollBar('#customize-filter-mask')"><span><?php esc_html_e('{{mode.name}}','web-to-print-online-designer'); ?></span></li>
                                                            </ul>
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr ng-show ="checkBlendColor == 1">
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Apha','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageBlendColor('apha',attributeImage.color,attributeImage.mode,attributeImage.apha)" ng-model="attributeImage.apha" step="0.01" min="0" max="1">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                            <input  type="number" placeholder="--" class="range-track"  ng-change="setImageBlendColor('apha',attributeImage.color,attributeImage.mode,attributeImage.apha)" ng-model="attributeImage.apha" min="0" max="1"></input>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="label-customize">
                                                <label><?php esc_html_e('Blend Image','web-to-print-online-designer'); ?></label>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="box-switch-customize">
                                                <div class="nbd-checkbox-group-customize">
                                                    <div class="mt-check-garden"> 
                                                        <input id="blend-image-title" type="checkbox" ng-checked="checkBlendImage == 1" ng-click="setImageBlendClick('blendimage',aphaImage,modeImageBlend)" />
                                                        <label for="blend-image-title"></label>  
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr ng-show ="checkBlendImage == 1">
                                        <td>
                                            <div class="main-customize">
                                                <div class="brush" >
                                                    <button class="nbd-button nbd-dropdown nbd-dropdown-customize" ng-click="updateScrollBarCustomize('.tab-scroll-customize');updateScrollBar('#customize-filter-mask')">
                                                        {{nameModeImage ? nameModeImage : '<?php esc_html_e('Mode','web-to-print-online-designer'); ?>'}}  <i class="icon-nbd icon-nbd-arrow-drop-down"></i>
                                                        <div class="nbd-sub-dropdown nbd-sub-dropdown-customize" data-pos="left">
                                                            <ul class="tab-scroll tab-scroll-customize">
                                                                <li ng-repeat="(blendImage, blend) in blendImage" ng-class="blend.name == nameModeImage  ? 'active' : ''"
                                                                ng-click="setBlendAttritubeImage($index,blend.value,aphaImage);updateScrollBar('#customize-filter-mask')"
                                                                ><span><?php esc_html_e('{{blend.name}}','web-to-print-online-designer'); ?></span></li>
                                                            </ul>
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="main-customize">
                                                <input type="range" class="slide-input" style="cursor: pointer;"  ng-change="setImageBlend('blendimage',aphaImage,modeImageBlend)" ng-model="aphaImage" min="0" max="1" step="0.01">
                                                <span class="range-track"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="range-number">
                                                <input  type="number" placeholder="--" class="range-track" ng-change="setImageBlend('blendimage',aphaImage,modeImageBlend)" ng-model="aphaImage" min="0" max="1" ></input>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="customize-color-picker" id="nbd-text-color-picker-customize" ng-class="showFilterColorPicker ? 'active' : ''">
                    <spectrum-colorpicker
                        ng-model="currentColorCustomize"
                        options="{
                            preferredFormat: 'hex',
                            flat: true,
                            showButtons: false,
                            showInput: true,
                            containerClassName: 'nbd-sp'
                    }">
                    </spectrum-colorpicker>
                        <div>
                            <button class="nbd-button" ng-click="addColor();changeFill(currentColor);">Choose</button>
                        </div>
                    <span class="eyedropper-loading"></span>
                </div>
                <div class="customize-color-picker-blend" id="nbd-text-color-picker-customize-blend" ng-class="showFilterColorPickerBlend ? 'active' : ''">
                    <spectrum-colorpicker
                        ng-model="currentColorCustomizeBlend"
                        options="{
                            preferredFormat: 'hex',
                            flat: true,
                            showButtons: false,
                            showInput: true,
                            containerClassName: 'nbd-sp'
                    }">
                    </spectrum-colorpicker>
                </div>
        </li>
        <li class="menu-item menu-crop" ng-click="removeClippingMask()" ng-show="stages[currentStage].states.isMasked && !stages[currentStage].states.lockMask">
            <i class="nbd-tooltip-hover nbd-svg-icon" title="<?php esc_html_e('Remove clipping mask','web-to-print-online-designer'); ?>">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3.25 2.75l17 17L19 21l-2-2H5c-1.1 0-2-.9-2-2V7c0-.55.23-1.05.59-1.41L2 4l1.25-1.25zM22 12l-4.37-6.16C17.27 5.33 16.67 5 16 5H8l11 11 3-4z"/></svg>
            </i>
        </li>
        <li class="menu-item menu-crop" ng-click="editMask()" ng-show="stages[currentStage].states.isMasked && !stages[currentStage].states.lockMask">
            <i class="nbd-tooltip-hover nbd-svg-icon" title="<?php esc_html_e('Edit mask','web-to-print-online-designer'); ?>">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24" xml:space="preserve">
                    <path d="M23,7V1h-6v2H7V1H1v6h2v10H1v6h6v-2h10v2h6v-6h-2V7H23z M3,3h2v2H3V3z M5,21H3v-2h2V21z M17,19H7v-2H5V7h2V5h10v2h2v10h-2 V19z M21,21h-2v-2h2V21z M19,5V3h2v2H19z M13.7,14h-3.5l-0.7,2H7.9l3.4-9h1.4l3.4,9h-1.6C14.5,16,13.7,14,13.7,14z M10.7,12.7h2.6 L12,8.9C12,8.9,10.7,12.7,10.7,12.7z"/>
                    <path d="M12,5.4c-3.7,0-6.6,3-6.6,6.6s3,6.6,6.6,6.6s6.6-3,6.6-6.6S15.7,5.4,12,5.4z"/>
                </svg>
            </i>
        </li>
        <li class="menu-item menu-crop" ng-click="replaceMaskedImage()" ng-if="settings['nbdesigner_enable_upload_image'] == 'yes'" ng-show="stages[currentStage].states.isMasked">
            <i class="nbd-tooltip-hover nbd-svg-icon" title="<?php esc_html_e('Replace image','web-to-print-online-designer'); ?>">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 6v3l4-4-4-4v3c-4.42 0-8 3.58-8 8 0 1.57.46 3.03 1.24 4.26L6.7 14.8c-.45-.83-.7-1.79-.7-2.8 0-3.31 2.69-6 6-6zm6.76 1.74L17.3 9.2c.44.84.7 1.79.7 2.8 0 3.31-2.69 6-6 6v-3l-4 4 4 4v-3c4.42 0 8-3.58 8-8 0-1.57-.46-3.03-1.24-4.26z"/></svg>
            </i>
        </li>
        <li class="menu-item menu-crop" ng-click="detachImage()" ng-show="stages[currentStage].states.isMasked && !stages[currentStage].states.isEmptyMask && !stages[currentStage].states.lockMask">
            <i class="nbd-tooltip-hover nbd-svg-icon" title="<?php esc_html_e('Detach Image','web-to-print-online-designer'); ?>">
                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M15.96 10.29l-2.75 3.54-1.96-2.36L8.5 15h11l-3.54-4.71zM3 5H1v16c0 1.1.9 2 2 2h16v-2H3V5zm18-4H7c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V3c0-1.1-.9-2-2-2zm0 16H7V3h14v14z"/></svg>
            </i>
        </li>
        <li class="menu-item menu-removeBg" style="width: auto;margin-left: 0px;" ng-show="settings.is_prenium" ng-click="removeBg(stages[currentStage].states.src)">
            <button class="nbd-button try_pro">
            <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
            <span>Background removal</span>
        </button>
        </li>
        </ul>
    </div>