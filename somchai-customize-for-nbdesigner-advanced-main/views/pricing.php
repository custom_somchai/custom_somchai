<div class="wrap-pricing">
	<div class="title">
		<h5><?php esc_html_e('Thirdsdesign Pricing' , 'web-to-print-online-designer') ?></h5>
	</div>
	<div class="note">
		<h3><?php esc_html_e('Perfect fit for every everyone', 'web-to-print-online-designer') ?></h3>
	</div>
</div>