<?php if (!defined('ABSPATH')) exit; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">

    <meta name="Description" content="e-book">

    <title>e-book</title>
    <style type="text/css">
        html {
            height: 100%;
            width: 100%;
        }
    </style>
    <link rel='stylesheet' href='https://static.pubhtml5.com/book/template/Handy/style/phoneTemplate.css' />
    <link rel='stylesheet' href='https://static.pubhtml5.com/book/template/Handy/style/style.css' />
    <link rel='stylesheet' href='https://static.pubhtml5.com/book/template/Handy/style/player.css' />
    <script src="https://static.pubhtml5.com/book/js/jquery-1.9.1.min.js"></script>
</head>
<body>
<script src="<?php echo SOMCHAI_URL . 'assets/javascript/config.js' ?>"></script>
<script src="<?php echo SOMCHAI_URL . 'assets/javascript/LoadingJS.js' ?>"></script>
<script src="<?php echo SOMCHAI_URL . 'assets/javascript/main.js' ?>"></script>

<script src="<?php echo SOMCHAI_URL . 'assets/files/search/book_config.js' ?>"></script>
<script type="text/javascript">
    var textForPages = [];
    var positionForPages = [];
</script>
<link rel='stylesheet' href='https://static.pubhtml5.com/book/template/Handy/style/template.css' />
<script src="https://static.pubhtml5.com/book/js/flipHtml5.hiSlider2.min.js"></script>
<link rel="stylesheet" href="https://static.pubhtml5.com/book/css/hiSlider2.min.css"/>
<script src="<?php echo SOMCHAI_URL . 'assets/slide_javascript/slideJS.js' ?>"></script>
</body>
</html>
