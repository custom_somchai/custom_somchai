<?php if (!defined('ABSPATH')) exit; // Exit if accessed directly  ?>
<?php if( count($tags) > 0 ): ?>
<div class="nbd-category nbd-sidebar-con">
     <!-- NBD advanced - Custom Drop Down -->
    <?php ob_start() ?>
    <?php do_action('nbd_filter_template'); ?>
    <p class="nbd-sidebar-h3"><?php esc_html_e('Tags', 'web-to-print-online-designer'); ?></p>
     <?=  apply_filters('nbd_drop_down',ob_get_clean(),esc_html('Tags', 'web-to-print-online-designer')); ?>
    <div class="nbd-sidebar-con-inner">
        <ul>
            <?php foreach( $tags as $tag ): ?>
            <li class="nbd-gallery-filter-item">
                <a data-type="tag" data-value="<?php esc_attr_e( $tag['term_id'] ); ?>" href="#" class="nbd-tag-list-item <?php if( in_array( $tag['term_id'], $filter_tags ) ) echo 'active'; ?>">
                    <svg class="before" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="none" d="M0 0h24v24H0z"/>
                        <path d="M16.01 11H4v2h12.01v3L20 12l-3.99-4z"/>
                    </svg>
                    <span><?php esc_html_e( $tag['name'] ); ?></span>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endif; ?>
<?php if( count($colors) > 0 ): ?>
<?php /*nbdesigner*/ ob_start() ?>
<div class="nbd-category nbd-sidebar-con">
    <!-- NBD advanced - Custom Drop Down -->
    <?php ob_start() ?>
    <p class="nbd-sidebar-h3"><?php esc_html_e('Colors', 'web-to-print-online-designer'); ?></p>
    <?=  apply_filters('nbd_drop_down',ob_get_clean(),esc_html('Colors', 'web-to-print-online-designer')); ?>
    <div class="nbd-sidebar-con-inner">
        <ul>
            <?php foreach( $colors as $c ): ?>
            <li class="nbd-color-list-item">
                <span data-type="color" style="background: #<?php esc_attr_e( $c ); ?>" data-value="<?php esc_attr_e( $c ); ?>" class="nbd-color-list-item-inner <?php if( in_array( $c, $filter_colors ) ) echo 'active'; ?>"></span>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?=  apply_filters('nbod_hidden_color_sidebar',ob_get_clean()); ?>
<?php endif;