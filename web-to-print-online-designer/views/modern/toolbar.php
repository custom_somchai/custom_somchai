<?php ob_start();?>
<div class="nbd-toolbar animated" ng-class="stages[currentStage].states.isActiveLayer ? 'fadeInDown' : 'fadeOutUp'">
<?= /*nbdesigner advanced*/  apply_filters("ab_nbd_hidden_toolbar",ob_get_clean()); ?>
    <div class="main-toolbar">
        <?php include 'toolbars/toolbar-text.php'; ?>
        <?php include 'toolbars/toolbar-image.php'; ?>
        <?php include 'toolbars/toolbar-group.php'; ?>
        <?php include 'toolbars/toolbar-path.php';?>
        <?php include 'toolbars/toolbar-common.php'; ?>
        <?php include 'toolbars/color-palette.php';?>
    </div>
</div>