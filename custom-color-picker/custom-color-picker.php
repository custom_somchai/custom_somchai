<?php
/**
 * Plugin Name:       Custom color picker for Nbdesigner Advanced
 * Plugin URI:        https://cmsmart.net
 * Description:       Add gradient color picker for text and background
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            Hoang
 * Author URI:        https://cmsmart.net
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       custom-color-picker
 * Domain Path:       /languages
 */

add_action('wp_enqueue_scripts','nbod_gradient_pick_nbdesigner_js',31);
function nbod_gradient_pick_nbdesigner_js(){
    wp_localize_script('nbdesigner','nbd_gradient_pick',true); 
}
add_action('admin_enqueue_scripts','nbod_admin_gradient_pick_nbdesigner_js',31);
function nbod_admin_gradient_pick_nbdesigner_js(){
    wp_localize_script('admin_nbdesigner','nbd_gradient_pick',true);  
}
add_action('nbd_extra_css','nbsd_nbd_extra_js');
function nbsd_nbd_extra_js($ui_mode){
   ?>
    <script src="<?php echo plugin_dir_url( __FILE__ ) . './xncolorpicker.min.js'; ?>"></script>
   <?php
}

add_action('nbd_js_config','nbod_gradient_pick_frontend_js');
function nbod_gradient_pick_frontend_js(){
    echo 'var nbd_gradient_pick = true;';
}

add_filter('nbdesigner_colors_settings','enable_new_color_picker');
function enable_new_color_picker($array){
    $new_pick_arr =  array(
        'title'         => esc_html__( 'Use gradient color picker', 'web-to-print-online-designer'),
        'id'            => 'nbdesigner_use_custom_picker',
        'description'   => esc_html__('Choose "Yes" that will override current color picker with new color picker.', 'web-to-print-online-designer'),
        'default'       => 'no',
        'type'          => 'radio',
        'options'       => array(
            'yes'   => esc_html__('Yes', 'web-to-print-online-designer'),
            'no'    => esc_html__('No', 'web-to-print-online-designer')
        )
    );
    array_push($array['color-setting'],$new_pick_arr);
    return $array;
}

add_filter('apply_new_color_picker','apply_new_color_picker',10,1);
function apply_new_color_picker($html){
    if (nbdesigner_get_option('nbdesigner_use_custom_picker') == no){
        return $html;
    }
    ob_start();
    ?>
    <style type="text/css">
        .doc-container{
            display: inline-flex;
        }
        .fcolorpicker-curbox {
            width: 40px !important; 
            height: 40px !important;
        }
        .gradient-angle,.color-preview{
            display: none !important;
        }
        
        .to-right{
            background: linear-gradient(to right, red , yellow);
        }
        .to-left{
            background: linear-gradient(to left, red , yellow);
        }
        .to-radian{
            background: radial-gradient( red , yellow);
        }
    </style>
    <div class="doc-container">
        <div >
        </div>
        <ul class="main-color-palette nbd-perfect-scroll">
            <li class="color-palette-item to-right" id="colorpickerleft" ></li>
            <li class="color-palette-item to-left" id="colorpickerright" ></li>
            <li class="color-palette-add" ng-click="showTextColorPalette()" ng-if="settings['nbdesigner_show_all_color'] == 'yes'"></li>
            <li class="color-eyedropper" ng-click="initEyeDropper2($event)" title="<?php esc_html_e('Eyedropper','web-to-print-online-designer'); ?>" ng-if="settings['nbdesigner_enable_eyedropper'] == 'yes' && !settings.is_mobile">
                <span class="eyedropper-loading"></span>
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 508.8 508.8" xml:space="preserve"><g><path d="M508.324,61.2c0-16.4-6.4-31.6-18-43.2c-11.6-11.6-26.8-18-43.2-18c-16.4,0-31.6,6.4-43.2,18l-96.8,97.6 c-4.8-2.8-10-4-15.6-4c-8.4,0-16.8,3.2-22.8,9.2l-4,4c-12,12-12.4,30.8-1.6,43.6l-242,242c-13.2,13.2-16,33.6-6.8,49.6l-10.4,14 c-5.2,6.8-4.4,17.6,1.6,24l5.6,5.6c3.2,3.2,8,5.2,13.2,5.2c4,0,8-1.2,10.8-3.6l14-10.8c6,3.2,12.8,5.2,20,5.2 c11.2,0,21.6-4.4,29.2-12l241.6-241.6c6,5.2,13.2,8,21.2,8c8.4,0,16.8-3.2,22.8-9.2l4-4c10.4-10.4,12.4-26,5.2-38.4l97.2-98 C501.924,92.8,508.324,77.6,508.324,61.2z M87.124,476c-4.8,4.8-11.2,7.6-18,7.6c-4.4,0-8.8-1.2-12.4-3.2c-2.4-1.2-5.2-2-7.6-2 c-3.6,0-6.8,1.2-9.6,3.2l-14,10.8c0,0-0.8,0.4-1.2,0.4c-0.8,0-1.6-0.4-1.6-0.4l-6-6c-0.8-0.8-0.8-2.4-0.4-3.2l10.4-14 c4-5.2,4.4-12,1.2-17.6c-5.6-10-4-22.4,4-30.4l242.4-241.6l54.4,54.8L87.124,476z"/></g></svg>
            </li>
            <li ng-repeat="color in listAddedColor track by $index" ng-click="changeFill(color)" class="color-palette-item" data-color="{{color}}" title="{{color}}" ng-style="{'background-color': color}"></li>
        </ul>
    </div>
    <script src="<?php echo plugin_dir_url( __FILE__ ) . './xncolorpicker.min.js'; ?>"></script>
    <script>
        var xncolorpickerleft = new XNColorPicker({
            color: "linear-gradient(90deg,rgba(168, 80, 215, 1) 0.0,rgba(35, 174, 234, 1) 100.0%)",
            selector: "#colorpickerleft",
            showprecolor: true,
            prevcolors: null,
            showhistorycolor: false,
            historycolornum: 16,
            format: 'hex',
            showPalette:true,
            show:false, 
            lang:'en',
            // colorTypeOption:'single,linear-gradient,radial-gradient',
            colorTypeOption:'linear-gradient',
            canMove:false,
            alwaysShow:false,
            autoConfirm:true,
            onError: function (e) {

            },
            onCancel:function(color){
                console.log("cancel",color)
            },
            onChange:function(color){
                var scope = angular.element(document.getElementById("designer-controller")).scope();
                scope.setTextGradientone(color,'left');

            },
            onConfirm:function(color){
                console.log("confirm",color)
            }
        });
        var xncolorpickerright = new XNColorPicker({
            color: "linear-gradient(270.0deg,rgba(168, 80, 215, 1) 0.0,rgba(35, 174, 234, 1) 100.0%)",
            selector: "#colorpickerright",
            showprecolor: true,
            prevcolors: null,
            showhistorycolor: false,
            historycolornum: 16,
            format: 'hex',
            showPalette:true,
            show:false, 
            lang:'en',
            // colorTypeOption:'single,linear-gradient,radial-gradient',
            colorTypeOption:'linear-gradient',
            canMove:false,
            alwaysShow:false,
            autoConfirm:true,
            onError: function (e) {

            },
            onCancel:function(color){
                console.log("cancel",color)
            },
            onChange:function(color){
                var scope = angular.element(document.getElementById("designer-controller")).scope();
                scope.setTextGradientone(color,'right');

            },
            onConfirm:function(color){
                console.log("confirm",color)
            }
        })
    </script>
    <?php
    $html = ob_get_clean();
    return $html;
}
