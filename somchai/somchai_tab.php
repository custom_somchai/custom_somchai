<?php
/*
Plugin Name: NBD advan
Plugin URI: https://akismet.com/
Description: Used by millions, Akismet is quite possibly the best way in the world to <strong>protect your blog from spam</strong>. It keeps your site protected even while you sleep. To get started: activate the Akismet plugin and then go to your Akismet Settings page to set up your API key.
Version: 4.1.10
Author: Automattic
Author URI: https://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: akismet
*/

add_action('nbd_js_config', 'nb_extra_js_x');
function nb_extra_js_x()
{
?>
    var somchai_advan = true;
<?php
}
add_action('back_roll_tab', 'back_tab');
function back_tab()
{
?>
    <div style="position: relative;
        margin-bottom: 20px;
        top: 3px;
        z-index: 9999;">
        <span class="see-all-cutom" data-api="false" id="changetab-elem" ng-click="showHideIcon('back')">
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"></path>
            </svg>
        </span>
    </div>
    <script>
    </script>
<?php
}
add_filter('tab_element', 'abc', 10, 3);
function abc($html, $task, $design_type)
{
?>
    <style>
        .items-custorm {
            position: relative;
        }

        .conten-icon-custom {
            display: flex;
            flex-wrap: wrap;
            background: #374957;
            padding: 14px;
            cursor: pointer;
            margin: 14px 3px;
        }

        .show-icon-custom {
            width: 56px;
            height: auto;
            margin: 14px 3px;
        }

        .conten-icon-custom:hover,
        .custom-draw:hover {
            background: #485d6d;
        }

        .mansory-item__inner>img {
            width: 50px;
        }

        .hiden-icon {
            display: none;
        }

        .hiden-icon2 {
            display: none !important;
        }

        .show-icon {
            display: flex;
        }

        .clipart-wrap {
            display: flex;
        }

        .clipart-wrap .clipart-item {
            opacity: 1 !important;
        }

        .custorm-tab {
            display: flex;
            justify-content: space-between;
        }

        .name-cutom {
            font-size: 24px;
            display: flex;
            padding: 0 20px;
            font-weight: bold;
            color: #fff !important;
        }

        .see-all-cutom {
            padding-right: 20px;
            font-size: 24px;
            cursor: pointer;
            font-weight: bold;
            cursor: pointer;
            color: #fff !important;
        }

        .custom-draw {
            margin-top: 20px;
            background: #374957;
            height: 55px;
            padding: 20px;
        }

        .nbd-sidebar .nbd-items-dropdown .result-loaded .content-items .content-item {
            height: auto !important;
        }
    </style>
    <div class="main-items">
        <div class="items-custorm">
            <div class="wapper-icon-custorm">
                <div>
                    <div class="custorm-tab">
                        <span class="name-cutom" style="opacity: 1;">Icon</span>
                        <span class="see-all-cutom" data-type="icons" data-api="false" ng-click="showHideIcon('icon');">
                            See all
                        </span>
                    </div>
                    <div class="conten-icon-custom">
                        <div nbd-drag="art.url" extenal="true" type="svg" class="show-icon-custom" ng-if="key < 10" ng-repeat="(key, art) in resource.icon.data" ng-click="addArt(art, true, true)" draggable="true">
                            <div class="mansory-item__inner">
                                <img ng-src="{{art.url}}">
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="custorm-tab">
                        <span class="name-cutom" style="opacity: 1;">Flaticon</span>
                        <span class=" see-all-cutom" data-type="flaticon" data-api="false" ng-click="onClickTab('flaticon', 'element');showHideIcon('flaticon');">
                            See all
                        </span>
                    </div>
                    <div class="conten-icon-custom">
                        <div nbd-drag="art.url" extenal="true" type="svg" class="show-icon-custom" ng-if="key < 10" ng-repeat="(key, art) in resource.flaticon.data" ng-click="addSvgFromMedia(art, true, true)" draggable="true">
                            <div class="mansory-item__inner">
                                <img ng-src="{{art.url}}">
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="custorm-tab">
                        <span class="name-cutom" style="opacity: 1;">Storyset</span>
                        <span class="see-all-cutom" data-type="storyset" data-api="false" ng-click="showHideIcon('storyset');">
                            See all
                        </span>
                    </div>
                    <!-- All Storyset -->
                    <div class=" conten-icon-custom">
                        <div nbd-drag="art.url" extenal="true" type="svg" class="show-icon-custom" ng-if="key < 10" ng-repeat="(key, art) in resource.storyset.data" ng-click="addArt(art, true, true)" draggable="true">
                            <div class="mansory-item__inner">
                                <img ng-src="{{art.url}}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="custorm-tab" style="margin-bottom: 10px;">
                    <span class="name-cutom"> Grid frame</span>
                    <span class="see-all-cutom" data-type="frame" data-api="false" ng-click="showHideIcon('frame');">
                        See all
                    </span>
                </div>
                <!-- 10 Frame -->
                <div class="content-item" data-type="frame">
                    <div class="frames-wrapper">
                        <div class="frame-wrap" ng-if="$index < 3" ng-repeat="frame in resource.frames track by $index" ng-click="addFrame(frame)">
                            <photo-frame data-frame="frame" class="ng-isolate-scope">
                                <div class="frame-wrap-inner" ng-style="getFrameStyle()">
                                    <div class="frame-panel" ng-repeat="panel in framePanel" ng-style="{{frame}}"></div>
                                </div>
                            </photo-frame>
                        </div>
                    </div>
                </div>
                <!-- All Frame -->
                <div class="content-item" data-type="frame">
                    <div class="frames-wrapper">
                        <div class="frame-wrap" ng-if="$index < 3" ng-repeat="frame in resource.frames track by $index" ng-click="addFrame(frame)">
                            <photo-frame data-frame="frame" class="ng-isolate-scope">
                                <div class="frame-wrap-inner" ng-style="getFrameStyle()">
                                    <div class="frame-panel" ng-repeat="panel in framePanel" ng-style="{{frame}}"></div>
                                </div>
                            </photo-frame>
                        </div>
                    </div>
                </div>

                <!-- ClipArt -->

                <!-- <div class="custom-draw">
                <div>
                    <span class="name-cutom">Cliparts</span>
                    <span style="transform: translateY(1122px); left: 115px; top: 0px;" class="item see-all-cutom" data-type="cliparts" data-api="false" ng-click="onClickTab('clipart', 'element');showHideIcon()">
                        See all
                    </span>
                </div>
            </div>

            <div class="tab-main tab-scroll">
                <div class="nbd-items-dropdown">
                    <div>
                        <div class="clipart-wrap">
                            <div class="clipart-item" nbd-drag="art.url" extenal="false" type="svg" ng-repeat="art in resource.clipart.filteredArts | limitTo: resource.clipart.filter.currentPage * resource.clipart.filter.perPage">
                                <img ng-src="{{art.url}}" ng-click="addArt(art, true, true)" alt="{{art.name}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
                <!-- All cliparts -->
                <!-- <div class="tabs" ng-if="settings['nbdesigner_enable_clipart'] == 'yes'" id="tab-svg" nbd-scroll="scrollLoadMore(container, type)" data-container="#tab-svg" data-type="clipart" data-offset="20">
                <div class="cliparts-category" ng-class="resource.clipart.data.cat.length > 0 ? '' : 'nbd-hiden'">
                    <div class="nbd-button nbd-dropdown">
                        <span>{{resource.clipart.filter.currentCat.name}}</span>
                        <i class="icon-nbd icon-nbd-chevron-right rotate90"></i>
                        <div class="nbd-sub-dropdown" data-pos="center">
                            <ul class="nbd-perfect-scroll ps ps--theme_default" data-ps-id="2c7fbdac-df6e-922e-3bc6-1b1ca4f89810">

                                <li ng-click="changeCat('clipart', cat)" ng-repeat="cat in resource.clipart.data.cat" class="ng-scope"><span class="ng-binding">trunglol</span><span class="ng-binding">2</span></li>
                                <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                                    <div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                </div>
                                <div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;">
                                    <div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-main tab-scroll ps ps--theme_default" data-ps-id="09b970d9-23c6-d7eb-7403-e4fbc306afb1">
                    <div class="nbd-items-dropdown">
                        <div>
                            <div class="clipart-wrap" style="position: relative; height: 80.0156px;">
                                <div class="clipart-item ng-scope ng-isolate-scope in-view slideInDown animated animate700 animate300 animate800" nbd-drag="art.url" extenal="false" type="svg" ng-repeat="art in resource.clipart.filteredArts | limitTo: resource.clipart.filter.currentPage * resource.clipart.filter.perPage" draggable="true" style="position: absolute; left: 0px; top: 0px;">
                                    <img ng-src="{{art.url}}" ng-click="addArt(art, true, true)" alt="{{art.name}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->



                <!-- Photo Frame -->
                <div>
                    <div class="custorm-tab" style="margin-bottom: 10px;margin-top: 10px;">
                        <span class="name-cutom"> Photo frame</span>
                        <span id="frame_photo" class="see-all-cutom" data-type="photoFrame" data-api="false" ng-click="showHideIcon('photoFrame');">
                            See all
                        </span>
                    </div>
                    <div class="conten-icon-custom" style="height: 140px;">
                        <div class="show-icon-custom" ng-click="addPhotoFrame(frame)" ng-if="frame < 5" ng-repeat="frame in [] | range: ( resource.photoFrame.filter.currentPage * resource.photoFrame.filter.perPage > resource.photoFrame.filter.total ? resource.photoFrame.filter.total : ( resource.photoFrame.filter.currentPage * resource.photoFrame.filter.perPage ) )">
                            <img ng-src="{{'//dpeuzbvf3y4lr.cloudfront.net/frames/preview/f' + ( frame + 1 ) + '.png'}}" alt="Photo Frame" />
                        </div>
                    </div>
                </div>

                <!-- Draw -->
                <div class="custom-draw">
                    <div class="custorm-tab">
                        <span class="name-cutom">Draw</span>
                        <span class="see-all-cutom" data-type="draw" data-api="false" ng-click="showHideIcon('draw');">
                            See all
                        </span>
                    </div>
                </div>

                <!-- V Card -->
                <div class="custom-draw">
                    <div class="custorm-tab">
                        <span class="name-cutom">vCard</span>
                        <span class="see-all-cutom" data-type="vcard" data-api="false" ng-click="showHideIcon('vcard');">
                            See all
                        </span>
                    </div>
                </div>

                <!-- QR -->
                <div class="custom-draw">
                    <div class="custorm-tab">
                        <span class="name-cutom">Bar/QR-Code</span>
                        <span class="see-all-cutom" data-type="qr-code" data-api="false" ng-click="showHideIcon('qrcode');">
                            See all
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}

add_filter('after_render_data', 'after_data', 10, 4);
function after_data($html, $task, $design_type, $valid_license)
{
?>

    <div class="result-loaded">
        <div class="content-items">
            <div class="content-item type-color" data-type="color">
                <div class="main-type">
                    <div ng-if="!!settings.nbes_enable_settings && settings.nbes_enable_settings.combination == 1">
                        <span class="heading-title"><?php esc_html_e('Combination colors', 'web-to-print-online-designer'); ?></span>
                        <div class="nbes-colors">
                            <div class="nbes-color" ng-repeat="cbg_code in settings.nbes_settings.combination_colors.bg_codes track by $index">
                                <div ng-style="{'background-color': cbg_code}" class="bg_color" ng-click="selectCombinationColor( $index )" title="{{settings.nbes_settings.combination_colors.bg_names[$index] + ' - ' + settings.nbes_settings.combination_colors.fg_names[$index]}}">
                                    <span ng-style="{'color': settings.nbes_settings.combination_colors.fg_codes[$index]}">Aa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ng-if="!!settings.nbes_enable_settings && settings.nbes_enable_settings.background == 1 && settings.nbes_enable_settings.combination != 1">
                        <span class="heading-title"><?php esc_html_e('Background colors', 'web-to-print-online-designer'); ?></span>
                        <div class="nbes-colors">
                            <div class="nbes-color bg-color" ng-repeat="bg_code in settings.nbes_settings.background_colors.codes track by $index">
                                <div ng-style="{'background-color': bg_code}" class="bg_color" ng-click="_changeBackgroundCanvas($index)" title="{{settings.nbes_settings.background_colors.names[$index]}}">
                                    <span ng-style="{'color': bg_code}"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-item type-draw" data-type="draw" id="nbd-draw-wrap">
                <div class="main-type">
                    <div class="free-draw-settings" ng-show="resource.drawMode.status">
                        <span class="heading-title"><?php esc_html_e('Free Drawing', 'web-to-print-online-designer'); ?></span>
                        <div class="brush">
                            <h3 class="color-palette-label"><?php esc_html_e('Choose ', 'web-to-print-online-designer'); ?></h3>
                            <button class="nbd-button nbd-dropdown">
                                <?php esc_html_e('Brush', 'web-to-print-online-designer'); ?> <i class="icon-nbd icon-nbd-arrow-drop-down"></i>
                                <div class="nbd-sub-dropdown" data-pos="left">
                                    <ul class="tab-scroll">
                                        <li ng-click="resource.drawMode.brushType = 'Pencil';changeBush()" ng-class="resource.drawMode.brushType == 'Pencil' ? 'active' : ''"><span><?php esc_html_e('Pencil', 'web-to-print-online-designer'); ?></span></li>
                                        <li ng-click="resource.drawMode.brushType = 'Circle';changeBush()" ng-class="resource.drawMode.brushType == 'Circle' ? 'active' : ''"><span><?php esc_html_e('Circle', 'web-to-print-online-designer'); ?></span></li>
                                        <li ng-click="resource.drawMode.brushType = 'Spray';changeBush()" ng-class="resource.drawMode.brushType == 'Spray' ? 'active' : ''"><span><?php esc_html_e('Spray', 'web-to-print-online-designer'); ?></span></li>
                                    </ul>
                                </div>
                            </button>
                        </div>
                        <ul class="main-ranges">
                            <li class="range range-brightness">
                                <label><?php esc_html_e('Brush width ', 'web-to-print-online-designer'); ?></label>
                                <div class="main-track">
                                    <input class="slide-input" type="range" step="1" min="1" max="100" ng-change="changeBush()" ng-model="resource.drawMode.brushWidth">
                                    <span class="range-track"></span>
                                </div>
                                <span class="value-display">{{resource.drawMode.brushWidth}}</span>
                            </li>
                        </ul>
                        <div class="color">
                            <h3 class="color-palette-label"><?php esc_html_e('Brush color', 'web-to-print-online-designer'); ?></h3>
                            <ul class="main-color-palette nbd-perfect-scroll">
                                <li class="color-palette-add" ng-init="showBrushColorPicker = false" ng-click="showBrushColorPicker = !showBrushColorPicker;" ng-style="{'background-color': currentColor}"></li>
                                <li ng-repeat="color in listAddedColor track by $index" ng-click="resource.drawMode.brushColor=color; changeBush()" class="color-palette-item" data-color="{{color}}" title="{{color}}" ng-style="{'background-color': color}"></li>
                            </ul>
                            <div class="pinned-palette default-palette">
                                <h3 class="color-palette-label"><?php esc_html_e('Default palette', 'web-to-print-online-designer'); ?></h3>
                                <ul class="main-color-palette" ng-repeat="palette in resource.defaultPalette">
                                    <li ng-class="{'first-left': $first, 'last-right': $last}" ng-repeat="color in palette track by $index" ng-click="resource.drawMode.brushColor=color; changeBush()" class="color-palette-item" data-color="{{color}}" title="{{color}}" ng-style="{'background': color}"></li>
                                </ul>
                            </div>
                            <div class="nbd-text-color-picker" id="nbd-bg-color-picker" ng-class="showBrushColorPicker ? 'active' : ''">
                                <spectrum-colorpicker ng-model="currentColor" options="{
                                                    preferredFormat: 'hex',
                                                    color: '#fff',
                                                    flat: true,
                                                    showButtons: false,
                                                    showInput: true,
                                                    containerClassName: 'nbd-sp'
                                            }">
                                </spectrum-colorpicker>
                                <div style="text-align: <?php echo (is_rtl()) ? 'right' : 'left' ?>">
                                    <button class="nbd-button" ng-click="addColor();changeBush(currentColor);showBrushColorPicker = false;"><?php esc_html_e('Choose', 'web-to-print-online-designer'); ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="nbd-color-palette-inner">
                            <div class="working-palette" ng-if="settings['nbdesigner_show_all_color'] == 'yes'">
                                <ul class="main-color-palette tab-scroll">
                                    <li class="color-palette-item color-palette-add" ng-click="stageBgColorPicker.status = !stageBgColorPicker.status;"></li>
                                    <li ng-repeat="color in listAddedColor track by $index" ng-click="changeBackgroundCanvas(color)" class="color-palette-item" data-color="{{color}}" title="{{color}}" ng-style="{'background-color': color}">
                                    </li>
                                </ul>
                                <div class="nbd-text-color-picker" id="nbd-stage-bg-color-picker" ng-class="stageBgColorPicker.status ? 'active' : ''">
                                    <spectrum-colorpicker ng-model="stageBgColorPicker.currentColor" options="{
                                                preferredFormat: 'hex',
                                                color: '#fff',
                                                flat: true,
                                                showButtons: false,
                                                showInput: true,
                                                containerClassName: 'nbd-sp'
                                                }">
                                    </spectrum-colorpicker>
                                    <div>
                                        <button class="nbd-button" ng-click="changeBackgroundCanvas(stageBgColorPicker.currentColor);">
                                            <?php esc_html_e('Choose', 'web-to-print-online-designer'); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="pinned-palette default-palette" ng-if="settings['nbdesigner_show_all_color'] == 'yes'">
                                <h3 class="color-palette-label default"><?php esc_html_e('Default palette', 'web-to-print-online-designer'); ?></h3>
                                <ul class="main-color-palette tab-scroll" ng-repeat="palette in resource.defaultPalette">
                                    <li ng-class="{'first-left': $first, 'last-right': $last}" ng-repeat="color in palette track by $index" ng-click="changeBackgroundCanvas(color)" class="color-palette-item" data-color="{{color}}" title="{{color}}" ng-style="{'background': color}">
                                    </li>
                                </ul>
                            </div>
                            <div class="pinned-palette default-palette" ng-if="settings['nbdesigner_show_all_color'] == 'no'">
                                <h3 class="color-palette-label"><?php esc_html_e('Color palette', 'web-to-print-online-designer'); ?></h3>
                                <ul class="main-color-palette settings">
                                    <li ng-repeat="color in __colorPalette track by $index" ng-class="{'first-left': $first, 'last-right': $last}" ng-click="changeBackgroundCanvas(color)" class="color-palette-item" data-color="{{color}}" title="{{color}}" ng-style="{'background': color}"></li>
                                </ul>
                            </div>
                            <div><button class="nbd-button" ng-click="removeBackgroundCanvas()"><?php esc_html_e('Remove background', 'web-to-print-online-designer'); ?></button></div>
                        </div>
                    </div>
                    <div class="free-draw-options">
                        <div class="draw-item" ng-class="resource.drawMode.status ? 'active' : ''" ng-click="enableDrawMode()" title="<?php esc_html_e('Free Draw', 'web-to-print-online-designer'); ?>">
                            <i class="icon-nbd icon-nbd-drawing"></i>
                        </div>
                        <div class="draw-item" ng-click="addGeometricalObject( 'circle' )" title="<?php esc_html_e('Circle', 'web-to-print-online-designer'); ?>">
                            <i class="icon-nbd icon-nbd-layer-circle"></i>
                        </div>
                        <div class="draw-item" ng-click="addGeometricalObject( 'triangle' )" title="<?php esc_html_e('Triangle', 'web-to-print-online-designer'); ?>">
                            <i class="icon-nbd icon-nbd-layer-triangle"></i>
                        </div>
                        <div class="draw-item" ng-click="addGeometricalObject( 'rect' )" title="<?php esc_html_e('Rectangle', 'web-to-print-online-designer'); ?>">
                            <i class="icon-nbd icon-nbd-layer-rect"></i>
                        </div>
                        <div class="draw-item" ng-click="addGeometricalObject( 'hexagon' )" title="<?php esc_html_e('Hexagon', 'web-to-print-online-designer'); ?>">
                            <i class="icon-nbd icon-nbd-layer-polygon"></i>
                        </div>
                        <div class="draw-item" ng-click="addGeometricalObject( 'line' )" title="<?php esc_html_e('Line', 'web-to-print-online-designer'); ?>">
                            <i class="icon-nbd icon-nbd-line"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-item type-shapes" data-type="shapes" id="nbd-shape-wrap">
                <div class="mansory-wrap">
                    <div nbd-drag="art.url" extenal="true" type="svg" class="mansory-item" ng-click="addSvgFromMedia(art)" ng-repeat="art in resource.shape.data"><img ng-src="{{art.url}}"><span class="photo-desc">{{art.name}}</span></div>
                </div>
            </div>
            <div class="content-item type-icons" data-type="icons" id="nbd-icon-wrap">
                <div class="cliparts-category" ng-class="resource.icon.cat.length > 0 ? '' : 'nbd-hiden'">
                    <div class="nbd-button nbd-dropdown nbd-cat-dropdown">
                        <span>{{resource.icon.filter.currentCat.name}}</span>
                        <i class="icon-nbd icon-nbd-chevron-right rotate90"></i>
                        <div class="nbd-sub-dropdown" data-pos="center">
                            <ul class="nbd-perfect-scroll">
                                <li ng-click="changeCat('icon', cat)" ng-repeat="cat in resource.icon.cat"><span>{{cat.name}}</span><span>{{cat.total}}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mansory-wrap">
                    <div nbd-drag="art.url" extenal="true" type="svg" class="mansory-item" ng-click="addSvgFromMedia(art, $index)" ng-repeat="art in resource.icon.data" >
                        <div class="mansory-item__inner">
                            <img ng-src="{{art.url}}" /><span class="photo-desc">{{art.name}}</span>
                            <?php if (!$valid_license) : ?>
                                <span class="nbd-pro-mark-wrap" ng-if="$index > 20">
                                    <svg class="nbd-pro-mark" fill="#F3B600" xmlns="http://www.w3.org/2000/svg" viewBox="-505 380 12 10">
                                        <path d="M-503 388h8v1h-8zM-494 382.2c-.4 0-.8.3-.8.8 0 .1 0 .2.1.3l-2.3.7-1.5-2.2c.3-.2.5-.5.5-.8 0-.6-.4-1-1-1s-1 .4-1 1c0 .3.2.6.5.8l-1.5 2.2-2.3-.8c0-.1.1-.2.1-.3 0-.4-.3-.8-.8-.8s-.8.4-.8.8.3.8.8.8h.2l.8 3.3h8l.8-3.3h.2c.4 0 .8-.3.8-.8 0-.4-.4-.7-.8-.7z"></path>
                                    </svg>
                                    <?php esc_html_e('Pro', 'web-to-print-online-designer'); ?>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-item type-flaticon" data-type="flaticon" id="nbd-flaticon-wrap">
                <div class="mansory-wrap">
                    <div nbd-drag="art.url" extenal="true" type="svg" class="mansory-item" ng-click="addArt(art, true, true)" ng-repeat="art in resource.flaticon.data" ><img ng-src="{{art.url}}"><span class="photo-desc">{{art.name}}</span></div>
                </div>
            </div>
            <div class="content-item type-storyset" data-type="storyset" id="nbd-storyset-wrap">
                <div class="mansory-wrap">
                    <div nbd-drag="art.url" extenal="true" type="svg" class="mansory-item" ng-click="addArt(art, true, true)" ng-repeat="art in resource.storyset.data" ><img ng-src="{{art.url}}"><span class="photo-desc">{{art.name}}</span></div>
                </div>
            </div>
            <div class="content-item type-lines" data-type="lines" id="nbd-line-wrap">
                <div class="mansory-wrap">
                    <div nbd-drag="art.url" extenal="true" type="svg" class="mansory-item" ng-click="addSvgFromMedia(art)" ng-repeat="art in resource.line.data" ><img ng-src="{{art.url}}"><span class="photo-desc">{{art.name}}</span></div>
                </div>
            </div>
            <div class="content-item type-qrcode" data-type="qr-code" id="nbd-qrcode-wrap">
                <div class="main-type">
                    <div class="main-input">
                        <input ng-model="resource.qrText" type="text" class="nbd-input input-qrcode" name="qr-code" placeholder="https://yourcompany.com">
                    </div>
                    <button ng-class="resource.qrText != '' ? '' : 'nbd-disabled'" class="nbd-button" ng-click="addQrCode()"><?php esc_html_e('Create QRCode', 'web-to-print-online-designer'); ?></button>
                    <button ng-class="resource.qrText != '' ? '' : 'nbd-disabled'" class="nbd-button" ng-click="addBarCode()"><?php esc_html_e('Create BarCode', 'web-to-print-online-designer'); ?></button>
                    <div class="main-qrcode">

                    </div>
                    <svg id="barcode"></svg>
                </div>
            </div>
            <div ng-if="settings['nbdesigner_enable_vcard'] == 'yes'" class="content-item type-vcard" data-type="vcard" id="nbd-vcard-wrap">
                <p><?php esc_html_e('Your information', 'web-to-print-online-designer'); ?></p>
                <div ng-repeat="field in settings.vcard_fields" class="md-input-wrap">
                    <input id="vf-{{field.key}}" ng-model="field.value" ng-class="field.value.length > 0 ? 'holder' : ''" />
                    <label for="vf-{{field.key}}">{{field.name}}<label />
                </div>
                <p>
                    <span class="nbd-button" ng-click="generateVcard()"><?php esc_html_e('Generate', 'web-to-print-online-designer'); ?></span>
                </p>
            </div>
            <div class="content-item type-frame" data-type="frame" id="nbd-frame-wrap">
                <div class="frames-wrapper" id="somchai-frame-wrap">
                    <div class="frame-wrap" ng-repeat="frame in resource.frames track by $index" ng-click="addFrame(frame)">
                        <photo-frame data-frame="frame"></photo-frame>
                    </div>
                </div>
            </div>
            <div class="content-item type-photoFrame" data-type="photoFrame" id="nbd-photoFrame-wrap">
                <div class="mansory-wrap">
                    <div class="mansory-item" ng-click="addPhotoFrame(frame)" ng-repeat="frame in [] | range: ( resource.photoFrame.filter.currentPage * resource.photoFrame.filter.perPage > resource.photoFrame.filter.total ? resource.photoFrame.filter.total : ( resource.photoFrame.filter.currentPage * resource.photoFrame.filter.perPage ) )" >
                        <img ng-src="{{'//dpeuzbvf3y4lr.cloudfront.net/frames/preview/f' + ( frame + 1 ) + '.png'}}" alt="Photo Frame" />
                    </div>
                </div>
            </div>
            <?php if ($task == 'create' || ($task == 'edit' && $design_type == 'template')) : ?>
                <div class="content-item type-image-shape" data-type="image-shape">
                    <div class="image-shape-wrapper">
                        <div>
                            <span class="shape_mask shape-type-{{n}}" ng-click="addMask(n)" ng-repeat="n in [] | range:25"></span>
                        </div>
                        <div class="custom_image_shape-wrapper">
                            <div><?php esc_html_e('Custom Shape', 'web-to-print-online-designer'); ?></div>
                            <textarea class="form-control hover-shadow nbdesigner_svg_code" rows="5" ng-change="getPathCommand()" ng-model="svgPath" placeholder="<?php esc_html_e('Enter svg code', 'web-to-print-online-designer'); ?>" /></textarea>
                            <button ng-class="pathCommand !='' ? '' : 'nbd-disabled'" class="nbd-button" ng-click="addMask(-1)"><?php esc_html_e('Aadd Shape', 'web-to-print-online-designer'); ?></button>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div ng-if="settings['nbdesigner_enable_google_maps'] == 'yes' && settings['nbdesigner_static_map_api_key'] != ''" class="content-item type-maps" data-type="maps">
                <div class="google-maps-options">
                    <div class="google-maps-search">
                        <input ng-keyup="$event.keyCode == 13 && updateMapUrl()" ng-model="resource.maps.address" type="text" name="search" placeholder="<?php esc_attr_e('Your address', 'web-to-print-online-designer'); ?>">
                        <i class="icon-nbd icon-nbd-fomat-search" ng-click="updateMapUrl()"></i>
                    </div>
                    <div class="google-maps-option">
                        <select id="google-maps-maptype" ng-change="updateMapUrl()" ng-model="resource.maps.maptype">
                            <option value="roadmap"><?php esc_html_e('Roadmap', 'web-to-print-online-designer'); ?></option>
                            <option value="satellite"><?php esc_html_e('Satellite', 'web-to-print-online-designer'); ?></option>
                            <option value="terrain"><?php esc_html_e('Terrain', 'web-to-print-online-designer'); ?></option>
                            <option value="hybrid"><?php esc_html_e('Hybrid', 'web-to-print-online-designer'); ?></option>
                        </select>
                        <label for="google-maps-maptype"><?php esc_html_e('Map type', 'web-to-print-online-designer'); ?><label />
                    </div>
                    <div class="google-maps-option">
                        <select id="google-maps-maptype" ng-change="updateMapUrl()" ng-model="resource.maps.zoom">
                            <?php foreach (range(1, 20) as $range) : ?>
                                <option value="<?php echo $range; ?>"><?php echo $range; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <label for="google-maps-maptype"><?php esc_html_e('Map zoom', 'web-to-print-online-designer'); ?><label />
                    </div>
                    <div class="google-maps-option">
                        <input type="number" ng-min="100" ng-max="640" ng-step="1" ng-model="resource.maps.width" id="google-maps-width" />
                        <label for="google-maps-width"><?php esc_html_e('Map width', 'web-to-print-online-designer'); ?><label />
                    </div>
                    <div class="google-maps-option">
                        <input type="number" ng-min="100" ng-max="640" ng-step="1" ng-model="resource.maps.height" id="google-maps-height" />
                        <label for="google-maps-height"><?php esc_html_e('Map height', 'web-to-print-online-designer'); ?><label />
                    </div>
                    <div class="google-maps-option">
                        <select id="google-maps-markers-label" ng-change="updateMapUrl()" ng-model="resource.maps.markers.label">
                            <?php
                            $marker_labels = array_merge(array(''), range(0, 9), range('A', 'Z'));
                            foreach ($marker_labels as $marker_label) :
                            ?>
                                <option value="<?php echo $marker_label; ?>"><?php echo $marker_label === '' ? __('None', 'web-to-print-online-designer') : $marker_label; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <label for="google-maps-markers-label"><?php esc_html_e('Marker label', 'web-to-print-online-designer'); ?><label />
                    </div>
                    <div class="google-maps-option">
                        <select id="google-maps-markers-size" ng-change="updateMapUrl()" ng-model="resource.maps.markers.size">
                            <option value="normal"><?php esc_html_e('Normal', 'web-to-print-online-designer'); ?></option>
                            <option value="mid"><?php esc_html_e('Mid', 'web-to-print-online-designer'); ?></option>
                            <option value="small"><?php esc_html_e('Small', 'web-to-print-online-designer'); ?></option>
                        </select>
                        <label for="google-maps-markers-size"><?php esc_html_e('Marker size', 'web-to-print-online-designer'); ?><label />
                    </div>
                    <div class="google-maps-option">
                        <select id="google-maps-markers-color" ng-change="updateMapUrl()" ng-model="resource.maps.markers.color">
                            <option ng-style="{'background-color': color, color: '#fff'}" ng-repeat="color in resource.defaultPalette[0]" value="{{color}}">{{color}}</option>
                        </select>
                        <label for="google-maps-markers-color"><?php esc_html_e('Marker color', 'web-to-print-online-designer'); ?><label />
                    </div>
                    <div class="google-maps-preview google-maps-option" ng-class="resource.maps.loading ? 'loading' : ''" ng-if="resource.maps.url !=''">
                        <span class="nbd-button" ng-click="addImageFromUrl( resource.maps.url )"><?php esc_html_e('Insert map', 'web-to-print-online-designer'); ?></span>
                        <img ng-click="addImageFromUrl( resource.maps.url )" ng-src="{{resource.maps.url}}" title="<?php esc_html_e('Click to insert this map', 'web-to-print-online-designer'); ?>" />
                        <div class="loading-maps">
                            <svg class="circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nbdesigner-gallery" id="nbdesigner-gallery">
        </div>
        <div class="loading-photo">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
<?php
}

//dd_filter('nbd_after_cliparts', 'change_cliparts', 10, 3);
function change_cliparts($e, $active_cliparts, $animation_dir)
{
?>
    <li id="nav-cliparts" data-tour="cliparts" data-tour-priority="3" ng-click="disableDrawMode();disablePreventClickMode();show_all('cliparts');getResource('clipart', '#tab-svg', true)" class="<?php if ($active_cliparts) echo 'active'; ?> tab animated <?php echo ($animation_dir); ?> animate600 tab-clipart" ng-if="settings['nbdesigner_enable_clipart'] == 'yes'"><i class="icon-nbd icon-nbd-sharp-star"></i><span><?php esc_html_e('Cliparts', 'web-to-print-online-designer'); ?></span></li>
<?php
}

//add_filter('nbd_after_element', 'nbd_after_element', 10, 3);
function nbd_after_element($a, $active_elements, $animation_dir)
{
?>
    <li id="nav-elements" data-tour="elements" data-tour-priority="5" class="<?php if ($active_elements) echo 'active'; ?> tab animated <?php echo ($animation_dir); ?> animate800" ng-click="disableDrawMode();disablePreventClickMode();show_all('element');onClickTab('photoFrame', 'element');getResource('clipart', '#tab-svg', true)"><i class="icon-nbd icon-nbd-geometrical-shapes-group"></i><span><?php esc_html_e('Elements', 'web-to-print-online-designer'); ?></span></li>
<?php
}

add_filter('nbd_somchai_cliparts', 'somchai_cliparts', 10, 2);
function somchai_cliparts($a, $active_cliparts)
{
?>
    <div class="<?php if ($active_cliparts) echo 'active'; ?> tab nbd-onload" ng-if="settings['nbdesigner_enable_clipart'] == 'yes'" id="tab-svg" nbd-scroll="scrollLoadMore(container, type)" data-container="#tab-svg" data-type="clipart" data-offset="20">
        <div class="nbd-search">
            <input type="text" name="search" placeholder="<?php esc_html_e('Search clipart', 'web-to-print-online-designer'); ?>" ng-model="resource.clipart.filter.search" />
            <i class="icon-nbd icon-nbd-fomat-search"></i>
        </div>
        <div class="cliparts-category" ng-class="resource.clipart.data.cat.length > 0 ? '' : 'nbd-hiden'">
            <div class="nbd-sub-dropdown" data-pos="center">
                <div class="nbd-perfect-scroll">
                    <div ng-click="switchCat(cat)" ng-repeat="cat in resource.clipart.data.cat">
                        <div class="custorm-tab">
                            <span class="name-cutom" style="opacity: 1;">{{cat.name}}</span>
                            <span data-type="cliparts" data-api="false" ng-click="seeAll(cat)">See all</span>
                        </div>
                        <div class=" tab-main tab-scroll">
                            <div class="nbd-items-dropdown">
                                <div>
                                    <div class="clipart-wrap_somchai">
                                        <div class="clipart-item" nbd-drag="art.url" extenal="false" type="svg">
                                            <ul  a="{{cat.id}}">
                                                <li ng-repeat="(key,art) in resource.clipart.dataCustom['cat_' + cat.id]" ng-if="key < 10"><img ng-src="{{art.url}}" ng-click="addArt(art, true, true)" alt="{{art.name}}"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- All cliparts -->
        <div class="tab-main tab-scroll ps ps--theme_default show-cat-clipart" data-ps-id="77fd410f-3128-fd6a-6641-9193fe848856">
            <div style="position: relative;
                margin-bottom: 20px;
                top: 3px;
                z-index: 9999;">
                <span class="see-all-cutom" data-api="false" id="changetab-elem" ng-show="cat.id==0" ng-click="showAllCat(cat);" ng-repeat="cat in resource.clipart.data.cat">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"></path>
                    </svg>
                </span>
            </div>
            <div class="nbd-items-dropdown">
                <div>
                    <div class="clipart-wrap" style="position: relative; height: 698.141px;">
                        <div class="clipart-item sss ng-scope ng-isolate-scope in-view slideInDown animated animate100 animate800" nbd-drag="art.url" extenal="false" type="svg" ng-repeat="art in resource.clipart.filteredArts | limitTo: resource.clipart.filter.currentPage * resource.clipart.filter.perPage"  draggable="true" style="width:50%">
                            <img ng-src="{{art.url}}" ng-click="addArt(art, true, true)" alt="{{art.name}}">
                        </div>
                    </div>
                    <div class="loading-photo" style="display: none;">
                        <svg class="circular" viewBox="25 25 50 50">
                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                        </svg>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <style>
        .clipart-wrap .clipart-item {
            width: 100%;
        }

        .clipart-item ul {
            display: flex;

            justify-content: space-between;

            flex-wrap: wrap;
        }

        .clipart-item ul li {
            width: 20%;
            list-style: none;
        }
        div.nbd-sidebar .tabs-content .tab{
            overflow: auto;
        }
        div#tab-svg::-webkit-scrollbar {
            display: none;
        }
    </style>
<?php

}