<?php
/**
 * Plugin Name:       Custom edit text
 * Plugin URI:        https://cmsmart.net
 * Description:       Custom edit text
 * Version:           1.0.1
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            Huy
 * Author URI:        https://cmsmart.net
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       Custom edit text
 * Domain Path:       /languages
 */

add_action('nbd_js_config','tppl_add_jss');
function tppl_add_jss(){
  ?>
        var add_somchai_js = 1;
  <?php 
}

add_action('nbd_extra_css','templates_styleee');
function templates_styleee($path){
  ?>
        <link rel="stylesheet" href="<?= plugin_dir_url(__FILE__) . 'assets/style.css'; ?>">
  <?php
}

add_action('nbd_somchai_edit_Stage','menuStage');
function menuStage() {
    ?>
        <li ng-click="_addStage()">
          <i class="icon-nbd icon-nbd-24">
              <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path fill="#888" d="M4 6H2v14c0 1.1.9 2 2 2h14v-2H4V6zm16-4H8c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-1 9h-4v4h-2v-4H9V9h4V5h2v4h4v2z"></path></svg>
          </i>
        </li>
        <li class="context-item" ng-click="duplicateStage()">
            <i class="icon-nbd icon-nbd-24">
                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path fill="#888" d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm-1 4l6 6v10c0 1.1-.9 2-2 2H7.99C6.89 23 6 22.1 6 21l.01-14c0-1.1.89-2 1.99-2h7zm-1 7h5.5L14 6.5V12z"></path></svg>
            </i>
        </li>
        <li class="context-item" ng-click="_deleteStage()"><i class="icon-nbd icon-nbd-delete"></i></li>
    <?php
}

add_action('showCustomWarning','pageDesignScroll');
function pageDesignScroll() {
    ?>
        <div class="porto-thumnail_button" ng-click="hiddenThumnail()"> 
            <span> > </span>
        </div>
        <div class="porto-thumnail">
            <ul class="navbuttons" style="align-items: center;display: block !important;margin-left: 6px;">
                <li ng-repeat="temp in stages" ng-class="currentStage == $index ? 'active' : ''" class="img_thumbnail_css ng-scope active" style="margin-bottom: 10px; margin-top: 10px; cursor: pointer;" ng-click="switchStage($index, 'index')">
                    <div class="img-info">
                        <img class="side_thumbnail" title="{{temp.config.name}}" ng-src="{{temp.stage_thumbnail}}" style="display:block;">
                        <span>{{$index + 1}}</span>
                    </div>
                    <span>Print page: </span>
                    <input class="check-print_page" type="checkbox" value="{{$index + 1}}">
                </li>
            </ul>
            <div ng-click="_addStage()" style="padding: 15px;">
                <span>+ New Page</span>
            </div>
        </div>
    <?php
}

add_action('somchai_add_to_cartbtn','addBtnAddtoCart');
function addBtnAddtoCart() {
    ?>
        <br>
        <button class="nbd-button" ng-click="saveData('check');">Add to Card</button>
    <?php
}

add_action( 'woocommerce_after_order_itemmeta', 'custom_checkout_create_order_line_item',10,3);
function custom_checkout_create_order_line_item( $item_id, $item, $order ) {
    $custom_field = wc_get_order_item_meta( $item_id, '_nbd', true ); 
    $path = NBDESIGNER_CUSTOMER_DIR . '/' . $custom_field . '/config.json';
    $config = nbd_get_data_from_json($path);
    $page_print = $config->print_page;
    $page_print = implode(",",$page_print); 
    echo "Pages to print: " .$page_print;
}

add_filter('nbod_somchai_btn_ct','customBtnSave');
function customBtnSave() {
    ?>
        <button class="nbd-button try_pro" ng-click="prepareBeforeSaveForLater();">
            <span>Save</span>
        </button>
        <button class="nbd-button try_pro" ng-click="showProSomChai();">
            <svg style="color: #FBBE28;margin-right: 6px;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 13 13"><path fill="currentColor" d="M7.51 4.87C7.01 6.27 6.45 6.95 5.94 7c-.57.07-1.07-.18-1.54-.8a.54.54 0 0 0-.1-.1 1 1 0 1 0-.8.4l.01.12.82 3.24A1.5 1.5 0 0 0 5.78 11h4.44a1.5 1.5 0 0 0 1.45-1.14l.82-3.24a.54.54 0 0 0 .01-.12 1 1 0 1 0-.8-.4.54.54 0 0 0-.1.09c-.49.62-1 .87-1.54.81-.5-.05-1.04-.74-1.57-2.13a1 1 0 1 0-.98 0zM11 11.75a.5.5 0 1 1 0 1H5a.5.5 0 1 1 0-1h6z"></path></svg>
            <span>Try Thirdsdesign Pro</span>
        </button>
    <?php
}

add_action('ndb_modal_preview','modalPreview');
function modalPreview(){
    ?>
       <div id="myModal" class="modal fade nbdesigner_modal">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom: 15px;">
                    <b>Try Thirdsdesign Pro</b>
                    <button style="margin-top: 0;" type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                </div>
             
                <div class="modal-body somchai-popup_body">
                    <div class="somchai-popup_left">
                        <div class="popup_left-top">
                            <div class="sc-warrnig">
                                <div class="sc-warrnig_icon">
                                    <span style="margin-right: 10px;">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="m13.8 4.15 6.86 13.97a2 2 0 0 1-1.8 2.88H5.14a2 2 0 0 1-1.8-2.88L10.2 4.15a2 2 0 0 1 3.6 0zm-1.35.66a.5.5 0 0 0-.9 0L4.7 18.78a.5.5 0 0 0 .45.72h13.72a.5.5 0 0 0 .45-.72L12.45 4.81zM12 18.25a1.25 1.25 0 1 1 0-2.5 1.25 1.25 0 0 1 0 2.5zM12 9a1 1 0 0 1 1 1v3.5a1 1 0 0 1-2 0V10a1 1 0 0 1 1-1z"></path></svg>
                                    </span>
                                </div>
                                <div class="sc-warrnig_info">
                                    <p style="margin-bottom: 0">Talk to <a href="">the team owner</a> about the upgrade.</p>
                                </div>
                            </div>
                            <h3>Try Thirdsdesign Pro for Free</h3>
                            <font>
                               More productivity.Stronger.Enhance the productivity of your entire team.Try our favorite features, free for 30 days.
                            </font>
                            <p class="sc-warrnig_notification">
                                Here are the features you get when you choose Canva Pro.
                            </p>
                            <ul class="featuree">
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Unlimited designs.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Free to use all the pro templates, images.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Background removal.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Unlimited design sizes "Instant resizing".</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Over 100million premium stock photos,other.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Unlimited cliparts.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Premium Icons.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Using the gradation function.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Making an image into a cartoon.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Download in Transparent PNG.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Download in SVG file.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Can to upload Ai, EPS file.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Cloud Storage 100GB.</span></div>
                                </li>
                                <li class="item-feature">
                                    <span class="featue-check">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="m5.72 12.53-3.26-3.3c-.7-.72.36-1.77 1.06-1.06l2.73 2.77 6.35-6.35a.75.75 0 0 1 1.06 1.06l-6.88 6.88a.78.78 0 0 1-.5.23.83.83 0 0 1-.56-.23z"></path></svg>
                                        </span>
                                    </span>
                                    <div><span class="feature-text">Image Upload Space 1GB.</span></div>
                                </li>
                            </ul>
                            <p>
                                <b>Cancel any time.</b>We'll remind you before the 3-day trial expires.
                            </p>
                        </div>
                        <div class="popup_left-bottom">
                            <button><a href="https://www.thirdsdesign.com/pricing/">Start my free trial</a></button>
                        </div>
                    </div>
                    <div class="somchai-popup_right">
                        <img src="<?= plugin_dir_url(__FILE__) . 'assets/popup.png'; ?>" alt="">
                    </div>
                </div>
                </div>
            </div>
        </div>
    <?php
}