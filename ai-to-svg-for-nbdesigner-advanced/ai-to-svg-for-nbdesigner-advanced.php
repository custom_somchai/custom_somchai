<?php
/**
 * Plugin Name:       Ai to SVG for NBDesigner Advanced
 * Plugin URI:        https://cmsmart.net
 * Description:       Upload Ai and convert to SVG via Inkscape
 * Version:           1.0.1
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            Hieu
 * Author URI:        https://cmsmart.net
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       nbdesign_ai_to_svg
 * Domain Path:       /languages
 */
function check_is_prenium_user_ai(){
    if(!is_user_logged_in()) return false;
    $pro = get_user_meta(get_current_user_id() , 'is_prenium',true);
    if($pro){
        return true;
    }
    else{
        return false;
    }
} 
add_filter('nbod_input_html_tab_photo','nbdai_input_appliction_ai');
function nbdai_input_appliction_ai($html){
	if ( !strpos($html,"application/postscript")){
		$html = '<input type="file" accept="image/*,application/postscript" style="display: none;">';
	}
	return $html;
}

add_filter('nbod_span_html_tab_photo','nbdai_span_html_tab_photo_ai');
function nbdai_span_html_tab_photo_ai($html) {
	$html = '<span>Accept file types: <strong>png, jpg, svg , ai , eps </strong></span>';
	if(!check_is_prenium_user_ai()){
	    $html .= "<p style='color: red;font-size: 13px;'>Your account can't upload AI, EPS images, please upgrade your account</p>";
	}
	return $html;
}

add_action('nbd_js_config','nbdai_ai_to_svg');
function nbdai_ai_to_svg (){
	echo "var nbpdf_ai_to_svg = true;";
}

add_filter('nbod_config_allow_extension_var' , 'nbdai_nbdesigner_config');
function nbdai_nbdesigner_config($allow_extension){
    if(check_is_prenium_user_ai()){
	    array_push($allow_extension, 'ai','eps');
    }
	return $allow_extension;
}

add_filter('nbod_nbdesigner_customer_upload_response','nbdai_customer_upload_response',10,3);
function nbdai_customer_upload_response($res,$path,$ext){
	if($ext == 'ai' || $ext == 'eps'){
		$inp = realpath ($path['full_path']);
		if(PHP_OS == "WINNT"){
			if(nbdesigner_get_option('nbdesigner_path_inkscape') == ''){
				$res['mes'] = esc_html__( 'You have not entered the inkscape path!!', 'nbdesign_ai_to_svg' );
				$res['flag'] = 2;
				return $res;
			}
			$dir = nbdesigner_get_option('nbdesigner_path_inkscape');
			if(file_exists($dir) == false){
				$res['mes'] = esc_html__( 'The system cannot find the path specified !!', 'nbdesign_ai_to_svg' );
				$res['flag'] = 2;
				return $res;
			}
		}else{
			$dir = nbdesigner_get_option('nbdesigner_path_inkscape');
		}

		if ($ext == 'ai') {
		    $name = str_replace(".ai",".svg", $inp);
		    exec('"'.$dir.'" -f "'.$inp.'" --export-plain-svg="'.$name.'" 2>&1', $output);
		    $name = str_replace(".ai",".svg", $path['date_path']);
		} elseif($ext == 'eps'){
		    $name = str_replace(".eps",".eps.svg", $path['date_path']);
		    putenv('PATH=/usr/bin');
	    	exec('"'.$dir.'" -f "'.$inp.'" --export-plain-svg="'.$inp.'.svg" 2>&1', $output);
		}
		
		if(file_exists(NBDESIGNER_TEMP_DIR. $name) == true){
			$res['src'] = NBDESIGNER_TEMP_URL. $name;
			$res['dir'] = NBDESIGNER_TEMP_DIR. $name;
			$svg_content = file_get_contents($res['dir'] );
			$base_url = NBDESIGNER_TEMP_URL.'/';
    		$new_name   = strtotime( "now" ) . substr( md5( rand( 1111, 9999 ) ), 0, 8 );
			preg_match_all( '/xlink:href="(data:image\/[^;]+;base64[^"]+)"/i', $svg_content, $matches );
			if ( is_array( $matches ) && isset( $matches[1] ) && is_array( $matches[1] ) ) {
			foreach ( $matches[1] as $matchkey => $match ) {
				$data = explode( ',', $match );
				file_put_contents( NBDESIGNER_TEMP_DIR . "/".$new_name."_{$matchkey}.png", base64_decode( $data[ 1 ] ) );
				$svg_content = str_replace( $match, $base_url . "/".$new_name."_{$matchkey}.png", $svg_content );
				}
			}
			if ($ext == 'ai') {
				$name = str_replace(".ai",".svg", $path['date_path']);
			} elseif($ext == 'eps'){
				$name = str_replace(".eps",".svg", $path['date_path']);
			}
			
			file_put_contents( NBDESIGNER_TEMP_DIR. $name , $svg_content );
		}
		else{
			$res['mes'] = esc_html__( "error convert!!\r\n".implode("\r\n",$output), 'nbdesign_ai_to_svg' );
			$res['flag'] = 2;
		}
	}
	return $res;

}
add_filter('nbdesigner_general_settings','aitosvg_general_settings_path');
if(!function_exists('aitosvg_general_settings_path')){
	function aitosvg_general_settings_path($array){
		$default = 'inkscape';
		if (PHP_OS == "Linux"){
			exec('which inkscape 2>&1', $output);
			if (!empty($output) && strpos($output[0],'sh:')===false){
				$default = $output[0];
			}
			$AppImage = __DIR__.'/Inkscape.AppImage';
			if (!file_exists($AppImage)){
				$result = file_put_contents($AppImage, file_get_contents('https://inkscape.org/gallery/item/23849/Inkscape-e86c870-x86_64.AppImage'));
				chmod($AppImage,0744);
				if ($result !== false) $default = $AppImage;
			}else{
				$default = $AppImage;
				if (!is_executable($AppImage)){
					$default.= "<strong>Please change permission to executable: Chmod 744";
				}
			}
		}
		array_push($array['application'],array(
	                        'title'         => esc_html__( 'Inkscape Path', 'web-to-print-online-designer'),
	                        'description'   => esc_html__( 'This feature allows customers to enter the path of inscape.', 'web-to-print-online-designer')." Your OS: ".PHP_OS. ". Default value: ".$default,
	                        'id'            => 'nbdesigner_path_inkscape',
	                        'class'         => 'regular-text',
	                        'default'       => $default,
	                        'type'          => 'text',
	                  
	                   ));

		return $array;
	}
}

add_filter('nbod_svg_fitToStage_width','nbdsvg_fitToStage_width');
function nbdsvg_fitToStage_width($html){
		$html = '<li class="context-item" ng-click="fitToStage(\'width\')" ng-show="stages[currentStage].states.isLayer">';
	return $html;
}

add_filter('nbod_svg_fitToStage_height','nbdsvg_fitToStage_height');
function nbdsvg_fitToStage_height($html){
		$html = '<li class="context-item" ng-click="fitToStage(\'height\')" ng-show="stages[currentStage].states.isLayer">';
	return $html;
}

add_filter('nbod_svg_fitToStage_stretch','nbdsvg_fitToStage_stretch');
function nbdsvg_fitToStage_stretch($html){
		$html = '<li class="context-item" ng-click="fitToStage()" ng-show="stages[currentStage].states.isLayer">';
	return $html;
}

//add_filter('nbod_nbdesigner_customer_path','nbdsvg_nbdesigner_customer_path',10,2);
function nbdsvg_nbdesigner_customer_path($path,$ext){
	if($ext == 'ai'){
        $path = str_replace(".ai","-ai.ai", $path);
    }
	return $path;
}

add_filter('nbd_rest_part_svg','add_nbd_rest_part_svg',10,3);
function add_nbd_rest_part_svg($svg,$nbd_item_key,$key){
	$svg = NBDESIGNER_CUSTOMER_DIR . '/' . $nbd_item_key . '/frame_' . $key . '_svg.svg';
	return $svg;
}