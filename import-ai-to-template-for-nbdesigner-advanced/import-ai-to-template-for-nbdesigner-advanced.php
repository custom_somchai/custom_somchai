<?php
/**
* Plugin Name:       Import AI to Template for NBDesigner Advanced
* Plugin URI:        https://cmsmart.net
* Version:           1.0.0
* Description:       Import AI file to template
* Requires at least: 5.2
* Requires PHP:      7.1
* Author:            Hieu
* Author URI:        https://cmsmart.net
* License:           GPL v2 or later
* License URI:       https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain:       nbdesigner-advanced
* Domain Path:       /languages
*/




add_filter('nbdesigner_general_settings','aitosvg_general_settings_path');
if(!function_exists('aitosvg_general_settings_path')){
	function aitosvg_general_settings_path($array){
		$default = 'inkscape';
		if (PHP_OS == "Linux"){
			exec('which inkscape 2>&1', $output);
			if (!empty($output) && strpos($output[0],'sh:')===false){
				$default = $output[0];
			}
		}
		array_push($array['application'],array(
	                        'title'         => esc_html__( 'Inkscape Path', 'web-to-print-online-designer'),
	                        'description'   => esc_html__( 'This feature allows customers to enter the path of inscape.', 'web-to-print-online-designer')." Your OS: ".PHP_OS. ". Default value: ".$default,
	                        'id'            => 'nbdesigner_path_inkscape',
	                        'class'         => 'regular-text',
	                        'default'       => 'inkscape',
	                        'type'          => 'text',
	                  
	                   ));

		return $array;
	}
}

add_action('nbd_js_config','nbptemp_ai_to_svg');
function nbptemp_ai_to_svg (){
	echo "var nbptemp_ai_to_svg = true;";
}

add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );
function wpdocs_enqueue_custom_admin_style(){
 		wp_register_script( 'custom_wp_admin_js', "https://kit.fontawesome.com/f51a86faf4.js" );
        wp_enqueue_script( 'custom_wp_admin_js' );
}

add_filter('nbd_button_import_temp','nbdai_button_import_temp',10,2);
function nbdai_button_import_temp($html,$link_add_template) {
	$html = '<a class="button" href="'. $link_add_template .'">'. __('Add Template').'</a>
			<a class="button" id="button_imp"> '.__('Import Template').'</a>
			<form action="" method="post" enctype="multipart/form-data">
				<input type="file" id="input_imp" name = "fileUpload" onchange="importfunc()" accept="image/*,application/postscript" style="display: none;">
			</form>
			<div class= "load" id="load_impor">
				<span class="fas fa-spinner icon_xoay icon"></span>
			</div>
			<style>
				.load{
			        position: absolute;
				    z-index: 99999;
				    top: 0;
				    left: -20px;
				    width: 100%;
				    display: none;
				    height: 70%;
				    background: #BDBDBD;
				    overflow: hidden;
				    opacity: 0.9;
				    text-align: center;
				    font-size: 100px;
				    padding-top: 20%;
				}
				.icon_xoay{
					animation: icon_xoay 1.5s linear infinite;
					-moz-animation: icon_xoay 1.5s linear infinite;
					-ms-animation: icon_xoay 1.5s linear infinite;
					-o-animation: icon_xoay 1.5s linear infinite;
					-webkit-animation: icon_xoay 1.5s linear infinite;
				}
				@-webkit-keyframes icon_xoay{
					from{
						-ms-transform:rotate(0deg);
						-moz-transform: rotate(0deg);
						-o-transform: rotate(0deg);
						-webkit-transform: rotate(0deg);
						transform: rotate(0deg);
					}
					to{
						-ms-transform:rotate(360deg);
						-moz-transform: rotate(360deg);
						-o-transform: rotate(360deg);
						-webkit-transform: rotate(360deg);
						transform: rotate(360deg);
					}
				}
				/*@keyframes icon_xoay{
					from{
						-ms-transform:rotate(0deg);
						-moz-transform: rotate(0deg);
						-o-transform: rotate(0deg);
						-webkit-transform: rotate(0deg);
						transform: rotate(0deg);
					}
					to{
						-ms-transform:rotate(360deg);
						-moz-transform: rotate(360deg);
						-o-transform: rotate(360deg);
						-webkit-transform: rotate(360deg);
						transform: rotate(360deg);
					}
				}*/

			</style>
		<script>
			jQuery("#load_impor").hide();
			jQuery( "#button_imp" ).click(function() {
				jQuery( "#input_imp" ).click();
			});
			function importfunc() {
				var fileInputElement = document.getElementById("input_imp").files[0];
				var formData = new FormData();
				formData.append("fileInputElement", fileInputElement);
				formData.append("_ajax_nonce", "'.wp_create_nonce('import_temp').'");
				formData.append("action", "ajaxs_import_file_vector");
				jQuery("#load_impor").show();
				jQuery.ajax({
					method : "POST",
					data: formData,
					url : "'.admin_url('admin-ajax.php').'",
					contentType: false,
					processData: false,
					success: function(data){
						var data = JSON.parse(data);
						if(data.flag == 0 ){
							jQuery("#load_impor").hide();
							alert("error convert!!");
							return
						}
						else{
							location.href = "'.$link_add_template.'"+"&src="+data.src;
						}
					}
				});
			}
		</script>
	';
	return $html;
}

add_action( 'wp_ajax_ajaxs_import_file_vector', 'ajaxs_import_file_vector' );
function ajaxs_import_file_vector() {
	$res = [];
	$name = $_FILES["fileInputElement"]["name"];
	$target_dir = $_FILES["fileInputElement"]["tmp_name"];
	$ext =  explode('.', $name);
	$file_exten = $ext[count($ext) - 1];
    $new_name           = strtotime( "now" ) . substr( md5( rand( 1111, 9999 ) ), 0, 8 ) . '.' . $file_exten;
	$path = Nbdesigner_IO::create_file_path( NBDESIGNER_TEMP_DIR, $new_name);
	move_uploaded_file($target_dir, $path['full_path'] );
	// if($file_exten != 'ai'){
	// 	$res['mes'] = esc_html__( 'error convert!!', 'import-temp' );
	// 	$res['flag'] = 0;
	// 	echo json_encode($res);
	// 	wp_die();
	// }
	// if($file_exten == 'ai'){
	// 	$inp = realpath($path['full_path']);
	// 	$dir = nbdesigner_get_option('nbdesigner_path_inkscape');
	// 	if($dir == ''){
	// 			$dir = 'inskcape';
	// 	}
	// 	if(PHP_OS == "WINNT"){
	// 		if(file_exists($dir) == false){
	// 			$res['mes'] = esc_html__( 'The system cannot find the path specified !!', 'import-temp' );
	// 			$res['flag'] = 2;
	// 			return $res;
	// 		}
	// 	}
	// 	$name = str_replace(".ai",".svg", $inp);
	// 	exec('"'.$dir.'" -f "'.$inp.'" --export-plain-svg="'.$name.'" 2>&1', $output);
	// 	$name = str_replace(".ai",".svg", $path['date_path']);
	// 	if(file_exists(NBDESIGNER_TEMP_DIR. $name) == true){
	// 		$res['src'] = NBDESIGNER_TEMP_URL. $name;
	// 	}
	// 	else{
	// 		$res['mes'] = esc_html__( 'error convert!!', 'import-temp' );
	// 		$res['flag'] = 2;
	// 	}
	// 	$svg_content = file_get_contents($res['src'] );
	// 	$base_url = NBDESIGNER_TEMP_URL.'/';
	// 	$new_name = strtotime( "now" ) . substr( md5( rand( 1111, 9999 ) ), 0, 8 );
	// 	preg_match_all( '/xlink:href="(data:image\/[^;]+;base64[^"]+)"/i', $svg_content, $matches );
	// 	if ( is_array( $matches ) && isset( $matches[1] ) && is_array( $matches[1] ) ) {
	// 	foreach ( $matches[1] as $matchkey => $match ) {
	// 		$data = explode( ',', $match );
	// 		file_put_contents( NBDESIGNER_TEMP_DIR . "/".$new_name."{$matchkey}.png", base64_decode( $data[ 1 ] ) );
	// 		$svg_content = str_replace( $match, $base_url . "/".$new_name."{$matchkey}.png", $svg_content );
	// 		}
	// 	}
	// 	file_put_contents( NBDESIGNER_TEMP_DIR. $name , $svg_content );
	// 	if(file_exists(NBDESIGNER_TEMP_DIR. $name) == true){
	// 		$res['src'] = NBDESIGNER_TEMP_URL. $name;
	// 		$res['flag'] = 2;
	// 	}

	// 	echo json_encode($res);
	// 	wp_die();
	// }
	if($file_exten == 'ai' || $file_exten == 'eps'){
		$inp = realpath ($path['full_path']);
		if(PHP_OS == "WINNT"){
			if(nbdesigner_get_option('nbdesigner_path_inkscape') == ''){
				$res['mes'] = esc_html__( 'You have not entered the inkscape path!!', 'nbdesign_ai_to_svg' );
				$res['flag'] = 2;
				return $res;
			}
			$dir = nbdesigner_get_option('nbdesigner_path_inkscape');
			if(file_exists($dir) == false){
				$res['mes'] = esc_html__( 'The system cannot find the path specified !!', 'nbdesign_ai_to_svg' );
				$res['flag'] = 2;
				return $res;
			}
		}else{
			$dir = nbdesigner_get_option('nbdesigner_path_inkscape');
		}

		if ($file_exten == 'ai') {
		    $name = str_replace(".ai",".svg", $inp);
		    exec('"'.$dir.'" -f "'.$inp.'" --export-plain-svg="'.$name.'" 2>&1', $output);
		    $name = str_replace(".ai",".svg", $path['date_path']);
		} elseif($file_exten == 'eps'){
		    $name = str_replace(".eps",".eps.svg", $path['date_path']);
		    putenv('PATH=/usr/bin');
	    	exec('"'.$dir.'" -f "'.$inp.'" --export-plain-svg="'.$inp.'.svg" 2>&1', $output);
		}
		
		if(file_exists(NBDESIGNER_TEMP_DIR. $name) == true){
			$res['src'] = NBDESIGNER_TEMP_URL. $name;
			$res['dir'] = NBDESIGNER_TEMP_DIR. $name;
			$svg_content = file_get_contents($res['dir'] );
			$base_url = NBDESIGNER_TEMP_URL.'/';
    		$new_name   = strtotime( "now" ) . substr( md5( rand( 1111, 9999 ) ), 0, 8 );
			preg_match_all( '/xlink:href="(data:image\/[^;]+;base64[^"]+)"/i', $svg_content, $matches );
			if ( is_array( $matches ) && isset( $matches[1] ) && is_array( $matches[1] ) ) {
			foreach ( $matches[1] as $matchkey => $match ) {
				$data = explode( ',', $match );
				file_put_contents( NBDESIGNER_TEMP_DIR . "/".$new_name."_{$matchkey}.png", base64_decode( $data[ 1 ] ) );
				$svg_content = str_replace( $match, $base_url . "/".$new_name."_{$matchkey}.png", $svg_content );
				}
			}
			if ($file_exten == 'ai') {
				$name = str_replace(".ai",".svg", $path['date_path']);
			} elseif($file_exten == 'eps'){
				$name = str_replace(".eps",".svg", $path['date_path']);
			}
			
			file_put_contents( NBDESIGNER_TEMP_DIR. $name , $svg_content );
		}
		else{
			$res['mes'] = esc_html__( "error convert!!\r\n".implode("\r\n",$output), 'nbdesign_ai_to_svg' );
			$res['flag'] = 2;
		}
	}
	echo json_encode($res);
	wp_die();
}